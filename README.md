![](https://git.atd16.fr/cesig/gestion-de-git/-/raw/master/img/logo_geo16crea.png)

# Certification Univerttrail

![image_schema](ressources/Logo_Univert_Trail-300x176.jpg)

Ce projet recense l'ensemble des fichiers permettant la mise en place d'une base de données géographique aidant à l'obtention d'une certification Uni'vert trail.

Ce modèle est mis en oeuvre par l'ATD16 dans le cadre d'une utilisation sur le logiciel X'MAP développé par la société SIRAP. Il s'implémente dans un serveur de base de données PostgreSQL/PostGIS. 
Néanmoins, il peut être adapté pour usage sur un autre outil.

Il se base sur le cahier des charges Uni'vert trail de la fédération française d'athlétisme [[pdf](https://atd16.sharepoint.com/:b:/s/GO16/EataOXuhLd1Eu3pkBcAfNCgBfduaqTmCOGCTWXNg3k6mfg?e=73xcBB)].

# Principe de fonctionnement

Des tables géographiques permettent de répertorier les différents éléments nécessaires à la certification qui peuvent être cartographiés :
- site : élément surfacique représentant la zone de gestion cherchant ou ayant la certification Uni'vert trail
- zone de départ : élément surfacique représentant la ou les zones de départ du site
- parking : élément surfacique représentant les zones de parking
- circuit : élément linéaire représentant les circuits ou itinéraires de trail
- zone d'échauffement : élément linéaire représentant la zone d'échauffement
- passage goudronné : élément linéaire représentant les zones de passage goudronné empruntées par un circuit
- passage privé : élément linéaire représentant les zones de passage privé empruntées par un circuit
- signalétique : élément ponctuel représentant les panneaux et balises de signalétique associés au site et aux circuits
- équipement : élément ponctuel représentant les différents équipements (poubelle, vestiaire, points d'eau etc.) du site

La table des circuits est mis en valeur en fonction des caractéristiques de ses attributs. Une thématique s'applique en fonction de son niveau, lui-même calculé à partir de sa longueur, son dénivelé, sa difficulté etc.

Enfin, des tables d'alertes permettent de mettre en valeur des objets géographiques qui ne seraient pas en adéquation avec le cahier des charges Uni'vert trail.

# Génèse : quelques dates

* mai 2021-octobre 2021 : développement du modèle de donnée.
* novembre 2021 : mise en place et paramétrage de l'espace de travail cartographique dédié.
* décembre 2021 : mise en production de l'espace de travail cartographique.

# Ressources du projet

- MCD [[pdf](donnees/bdd/xmap/mcd/mcd_univert_trail.drawio.pdf)]  
- Schéma applicatif [[pdf](ressources/schema_applicatif_univert_trail.pdf)]  
<br/>

/!\ le schéma de bdd "atd16_univerttrail" est utilisé par l'ATD16 pour répondre aux exigences de son schéma applicatif. La modification de cette valeur nécessitera d'intervenir sur l'ensemble des fichiers de code.

- Script agrégé de l'ensemble de la structure [[sql](donnees/bdd/xmap/aggregat_atd16_univerttrail.sql)]  
- Schéma **atd16_univerttrail** [[sql](donnees/bdd/xmap/000_create_schema_atd16_univerttrail.sql)]  
    * Fonctions-triggers génériques [[sql](donnees/bdd/xmap/001_create_function_trigger_generique.sql)]  
    * Tables de listes déroulantes :  
        - Table **lst_labellisation** [[sql](donnees/bdd/xmap/010_lst_labellisation.sql)]  
        - Table **lst_categorie_longueur** [[sql](donnees/bdd/xmap/011_lst_categorie_longueur.sql)]  
        - Table **lst_categorie_denivele_positif** [[sql](donnees/bdd/xmap/012_lst_categorie_denivele_positif.sql)]  
        - Table **lst_categorie_denivele_continu_max** [[sql](donnees/bdd/xmap/013_lst_categorie_denivele_continu_max.sql)]  
        - Table **lst_technicite** [[sql](donnees/bdd/xmap/014_lst_technicite.sql)]  
        - Table **lst_niveau** [[sql](donnees/bdd/xmap/015_lst_niveau.sql)]  
        - Table **lst_risque** [[sql](donnees/bdd/xmap/016_lst_risque.sql)]  
        - Table **lst_passage_prive** [[sql](donnees/bdd/xmap/017_lst_passage_prive.sql)]  
        - Table **lst_autorisation_passage** [[sql](donnees/bdd/xmap/018_lst_autorisation_passage.sql)]  
        - Table **lst_zone_depart** [[sql](donnees/bdd/xmap/019_lst_zone_depart.sql)]  
        - Table **lst_type_signaletique** [[sql](donnees/bdd/xmap/020_lst_type_signaletique.sql)]  
        - Table **lst_type_equipement** [[sql](donnees/bdd/xmap/021_lst_type_equipement.sql)]  
    * Tables non géographiques :  
        - Table **ngeo_circuit_passage_prive** [[sql](donnees/bdd/xmap/025_ngeo_circuit_passage_prive.sql)]  
            * Fonction-trigger associée [[sql](donnees/bdd/xmap/250_trigger_ngeo_circuit_passage_prive.sql)]  
        - Table **ngeo_circuit_passage_goudronne** [[sql](donnees/bdd/xmap/026_ngeo_circuit_passage_goudronne.sql)]  
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/260_trigger_ngeo_circuit_passage_goudronne.sql)]  
    * Tables géographiques :  
        - Table **geo_site** [[sql](donnees/bdd/xmap/030_geo_site.sql)]  
            * Fonction-trigger associée [[sql](donnees/bdd/xmap/190_trigger_geo_site.sql)]  
            * Représentation graphique [[json](donnees/representation_graphique/json/ut_site.json)] - [[sld](donnees/representation_graphique/sld/utrail_thematique_site.sld)]  
            * Représentation textuelle [[sld](donnees/representation_graphique/sld/utrail_site_tpn.sld)]  
            * Représentation thématique des sites sans zone d'accueil et/ou de départ [[sld](donnees/representation_graphique/sld/utrail_site_alert_sans_zone.sld)]  
            * Représentation textuelle des sites sans zones d'accueil et/ou de départ [[sld](donnees/representation_graphique/sld/utrail_site_alert_sans_zone_tpn.sld)]  
            * Représentation thématique des sites ayant une longueur totale d'itinéraire inféreiure à 40 km [[sld](donnees/representation_graphique/sld/utrail_site_alert_longueur_iti_totale_40.sld)]  
            * Représentation textuelle des sites ayant une longueur totale d'itinéraire inféreiure à 40 km [[sld](donnees/representation_graphique/sld/utrail_site_alert_longueur_iti_totale_40_tpn.sld)]  
            * Représentation thématique des sites n'ayant pas trois circuits de niveaux différents [[sld](donnees/representation_graphique/sld/utrail_site_alert_circuits_non_heterogenes.sld)]  
            * Représentation textuelle des sites n'ayant pas trois circuits de niveaux différents [[sld](donnees/representation_graphique/sld/utrail_site_alert_circuits_non_heterogenes_tpn.sld)]  
        - Table **geo_zone_accueil** [[sql](donnees/bdd/xmap/040_geo_zone_accueil.sql)]  
            * Représentation graphique [[json](donnees/representation_graphique/json/ut_zone_accueil.json)]  
            * Représentation textuelle [[sld](donnees/representation_graphique/sld/utrail_zone_accueil_tpn.sld)]  
            * Représentation thématique des zones d'accueil sans site [[sld](donnees/representation_graphique/sld/utrail_alert_sans_site_surf.sld)]  
            * Représentation thématique des zones d'accueil incomplètes [[sld](donnees/representation_graphique/sld/utrail_zone_accueil_incomplete.sld)]  
            * Représentation textuelle des zones d'accueil incomplètes [[sld](donnees/representation_graphique/sld/utrail_zone_accueil_incomplete_tpn.sld)]  
        - Table **geo_zone_depart** [[sql](donnees/bdd/xmap/050_geo_zone_depart.sql)]  
            * Représentation graphique [[json](donnees/representation_graphique/json/ut_zone_depart.json)]  
            * Représentation textuelle [[sld](donnees/representation_graphique/sld/utrail_zone_depart_tpn.sld)]  
            * Représentation thématique des zones de départ sans site [[sld](donnees/representation_graphique/sld/utrail_alert_sans_site_surf.sld)]  
            * Représentation thématique des zones de départ comportant plusieurs portes principales sur le même site [[sld](donnees/representation_graphique/sld/utrail_zone_depart_portes_principales_multiples.sld)]  
            * Représentation thématique des zones de départ secondaire incomplètes [[sld](donnees/representation_graphique/sld/utrail_zone_depart_secondaire_incomplete.sld)]  
            * Représentation textuelle des zones de départ secondaire incomplètes [[sld](donnees/representation_graphique/sld/utrail_zone_depart_secondaire_incomplete_tpn.sld)]  
        - Table **geo_zone_echauffement** [[sql](donnees/bdd/xmap/060_geo_zone_echauffement.sql)]  
            * Fonction-trigger associée [[sql](donnees/bdd/xmap/360_trigger_geo_zone_echauffement.sql)]  
            * Représentation graphique [[json](donnees/representation_graphique/json/ut_zone_echauffement.json)]  
            * Représentation thématique des zones d'échauffement sans site [[sld](donnees/representation_graphique/sld/utrail_alert_sans_site_lin.sld)]  
            * Représentation thématique des zone d'échauffement ayant une longueur non conforme [[sld](donnees/representation_graphique/sld/utrail_alert_zone_echauffement.sld)]  
        - Table **geo_signaletique** [[sql](donnees/bdd/xmap/070_geo_signaletique.sql)]  
            * Représentation graphique [[json](donnees/representation_graphique/json/ut_signaletique.json)]  
            * Représentation thématique des objets de signalétique sans site [[sld](donnees/representation_graphique/sld/utrail_alert_sans_site_pct.sld)]  
        - Table **geo_equipement** [[sql](donnees/bdd/xmap/080_geo_equipement.sql)]  
            * Représentation graphique [[json](donnees/representation_graphique/json/ut_equipement.json)]  
            * Représentation thématique des objets d'équipement sans site [[sld](donnees/representation_graphique/sld/utrail_alert_sans_site_pct.sld)]  
        - Table **geo_parking** [[sql](donnees/bdd/xmap/090_geo_parking.sql)]  
            * Représentation graphique [[json](donnees/representation_graphique/json/ut_parking.json)]  
            * Représentation textuelle [[sld](donnees/representation_graphique/sld/utrail_parking_tpn.sld)]  
            * Représentation thématique des parkings sans site [[sld](donnees/representation_graphique/sld/utrail_alert_sans_site_surf.sld)]  
        - Table **geo_circuit** [[sql](donnees/bdd/xmap/100_geo_circuit.sql)]  
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/200_trigger_geo_circuit.sql)]  
            * Représentation graphique [[json](donnees/representation_graphique/json/ut_circuit.json)]  
            * Représentation thématique des circuits [[sld](donnees/representation_graphique/sld/utrail_thematique_circuit.sld)]  
            * Représentation thématique et textuelle des circuits [[sld](donnees/representation_graphique/sld/utrail_thematique_label_circuit.sld)]  
            * Représentation textuelle des circuits [[sld](donnees/representation_graphique/sld/utrail_circuit_tpn.sld)]  
            * Représentation thématique des circuits sans site [[sld](donnees/representation_graphique/sld/utrail_alert_sans_site_lin.sld)]  
            * Représentation thématique des circuits avec un niveau de difficulté incohérent [[sld](donnees/representation_graphique/sld/utrail_alert_circuit_niveau_incoherent.sld)]  
            * Représentation thématique des circuits sans niveau de difficulté [[sld](donnees/representation_graphique/sld/utrail_alert_circuit_sans_niveau.sld)]  
            * Représentation thématique des circuits ayant une proportion de passages goudronnés supérieure à 20% [[sld](donnees/representation_graphique/sld/utrail_circuit_passage_goudronne_sup_20.sld)]  
        - Table **geo_passage_goudronne** [[sql](donnees/bdd/xmap/110_geo_passage_goudronne.sql)]  
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/210_trigger_geo_passage_goudronne.sql)]  
            * Représentation graphique [[json](donnees/representation_graphique/json/ut_passage_goudronne.json)]  
            * Représentation thématique des passages goudronnés sans site [[sld](donnees/representation_graphique/sld/utrail_alert_sans_site_lin.sld)]  
        - Table **geo_passage_prive** [[sql](donnees/bdd/xmap/120_geo_passage_prive.sql)]  
            * Fonction-trigger associée [[sql](donnees/bdd/xmap/220_trigger_geo_passage_prive.sql)]  
            * Représentation graphique [[json](donnees/representation_graphique/json/ut_passage_prive.json)]  
            * Représentation thématique des passages privés sans site [[sld](donnees/representation_graphique/sld/utrail_alert_sans_site_lin.sld)]  
    * Vues géographiques interschémas [[sql](donnees/bdd/xmap/150_vues_interschemas.sql)]  
        - Représentation thématique des parcelles intersectant des circuits [[sld](donnees/representation_graphique/sld/utrail_parcelle_x_circuit.sld)]  

# Ressources annexes

Cahier des charges Uni'vert trail [[pdf](ressources/Cahier_des_charges_Univerttrail.pdf)]  
Notice d'utilisation dans X'MAP [[pdf](ressources/Géo16Créa_-_certification_Uni_Vert_trail.pdf)]

# Ressources générales
