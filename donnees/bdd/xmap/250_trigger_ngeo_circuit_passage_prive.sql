-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/05 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_presence_passage_prive() et du trigger associé t_after_iud_maj_presence_passage_prive
-- 2021/08/06 : SL / Modification du nom en 250_trigger_ngeo_circuit_passage_prive.sql et modification de la table sur laquelle cela s'applique

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                               Fonctions triggers et triggers spécifiques à la table ngeo_circuit_passage_prive                             ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                       Mise à jour de la présence de passage privé dans geo_circuit                                         ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_presence_passage_prive();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_presence_passage_prive()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        IF 
            COUNT(id_circuit) > 0
            FROM atd16_univerttrail.ngeo_circuit_passage_prive
            WHERE OLD.id_circuit = id_circuit -- Si la somme des passages privés ayant un id_circuit correspondant à celui qui est créé est supérieur à 0 
        THEN -- alors
            UPDATE atd16_univerttrail.geo_circuit -- On met à jour la table geo_circuit
	        SET passage_prive = '01' -- Le champ passage_prive prend la valeur 01 (Oui)
	        WHERE gid::text = OLD.id_circuit::text; -- Où le nouvel id_circuit est égal au gid de la table geo_circuit
	    ELSE -- sinon
	        UPDATE atd16_univerttrail.geo_circuit -- On met à jour la table geo_circuit
	        SET passage_prive = '02' -- Le champ passage_prive prend la valeur 02 (Non)
	        WHERE gid::text = OLD.id_circuit::text; -- Où le nouvel id_circuit est égal au gid de la table geo_circuit
	    END IF;
	ELSE
        IF 
            COUNT(id_circuit) > 0
            FROM atd16_univerttrail.ngeo_circuit_passage_prive
            WHERE NEW.id_circuit = id_circuit -- Si la somme des passages privés ayant un id_circuit correspondant à celui qui est créé est supérieur à 0 
        THEN -- alors
            UPDATE atd16_univerttrail.geo_circuit -- On met à jour la table geo_circuit
	        SET passage_prive = '01' -- Le champ passage_prive prend la valeur 01 (Oui)
	        WHERE gid::text = NEW.id_circuit::text; -- Où le nouvel id_circuit est égal au gid de la table geo_circuit
	    ELSE -- sinon
	        UPDATE atd16_univerttrail.geo_circuit -- On met à jour la table geo_circuit
	        SET passage_prive = '02' -- Le champ passage_prive prend la valeur 02 (Non)
	        WHERE gid::text = NEW.id_circuit::text; -- Où le nouvel id_circuit est égal au gid de la table geo_circuit
	    END IF;
	END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_presence_passage_prive() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_presence_passage_prive() IS '[ATD16] Mise à jour de la présence de passage privé dans geo_circuit';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_presence_passage_prive 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_univerttrail.ngeo_circuit_passage_prive 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_presence_passage_prive();
    
COMMENT ON TRIGGER t_after_iud_maj_presence_passage_prive ON atd16_univerttrail.ngeo_circuit_passage_prive IS 
    '[ATD16] Mise à jour de la présence de passage privé dans geo_circuit';

