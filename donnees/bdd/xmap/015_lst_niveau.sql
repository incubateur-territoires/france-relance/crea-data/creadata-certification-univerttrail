-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                              Table non géographique : Domaine de valeur du niveau de difficulté du circuit                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_niveau;

CREATE TABLE atd16_univerttrail.lst_niveau 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du niveau de difficulté
    libelle varchar(254), --[UVT] Libellé du niveau de difficulté
    cotation varchar(12), --[UVT] Cotation du niveau de difficulté
    CONSTRAINT pk_lst_niveau PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_niveau OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_niveau TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_niveau IS '[UVT] Domaine de valeur du niveau de difficulté du circuit';

COMMENT ON COLUMN atd16_univerttrail.lst_niveau.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_niveau.code IS '[ATD16] Code du niveau de difficulté';
COMMENT ON COLUMN atd16_univerttrail.lst_niveau.libelle IS '[UVT] Libellé du niveau de difficulté';
COMMENT ON COLUMN atd16_univerttrail.lst_niveau.cotation IS '[UVT] Cotation du niveau de difficulté';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_niveau 
    (code, libelle, cotation)
VALUES
    ('01', 'Niveau facile', '5 à 6'),
    ('02', 'Niveau moyen', '6 à 11'),
    ('03', 'Niveau difficile', '10 à 18'),
    ('04', 'Niveau très difficile', '17 à 26');

