-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Modification de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                        Table non géographique : Domaine de valeur de la catégorie du dénivelé positif du circuit                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_categorie_denivele_positif;

CREATE TABLE atd16_univerttrail.lst_categorie_denivele_positif 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de la catégorie
    libelle varchar(254), --[UVT] Libellé de la catégorie
    note integer, --[UVT] Note de la catégorie
    CONSTRAINT pk_lst_categorie_denivele_positif PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_categorie_denivele_positif OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_categorie_denivele_positif TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_categorie_denivele_positif IS '[UVT] Domaine de valeur de la catégorie du dénivelé positif du circuit ';

COMMENT ON COLUMN atd16_univerttrail.lst_categorie_denivele_positif.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_denivele_positif.code IS '[ATD16] Code de la catégorie';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_denivele_positif.libelle IS '[UVT] Libellé de la catégorie';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_denivele_positif.note IS '[UVT] Note de la longueur';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_categorie_denivele_positif 
    (code, libelle, note)
VALUES
    ('00', 'Pas de dénivelé positif renseigné', NULL),
    ('01', '< 500 m', '2'),
    ('02', '500-750 m', '3'),
    ('03', '750-1000 m', '4'),
    ('04', '1000-1500 m', '5'),
    ('05', '1500-2000 m', '6'),
    ('06', '2000-2500 m', '7'),
    ('07', '> 2500 m', '8');

