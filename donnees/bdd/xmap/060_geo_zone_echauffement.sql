-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/07 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/08/05 : SL / Ajout du trigger t_before_iu_maj_longueur_sig
-- 2021/10/26 : SL / Ajout de la vue v_geo_zone_echauffement_alert_longueur
-- 2021/10/28 : SL / Ajout de la vue v_geo_zone_echauffement_sans_site

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                   Table géographique : Zone d'échauffement                                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.geo_zone_echauffement;

CREATE TABLE atd16_univerttrail.geo_zone_echauffement
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    id_site integer, --[FK][UVT] Identifiant du site
    longueur_sig numeric(5,3), --[UVT] Longueur du tronçon (automatique)
    longueur numeric(5,3), --[UVT] Longueur du tronçon
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Nom de la zone
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_zone_echauffement PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.geo_zone_echauffement OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.geo_zone_echauffement TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.geo_zone_echauffement IS '[ATD16] Table géographique contenant les zones d''échauffement';

COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.id_site IS '[FK][UVT] Identifiant du site';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.longueur_sig IS '[UVT] Longueur du tronçon (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.longueur IS '[UVT] Longueur du tronçon';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.ident IS '[SIRAP] Identificaion de l''objet / Nom de la zone';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###   v_geo_zone_echauffement_alert_longueur : vue de la couche geo_zone_echauffement dont la longueur n'est pas comprise entre 400 et 600 m   ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_zone_echauffement_alert_longueur;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_zone_echauffement_alert_longueur 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.longueur,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_zone_echauffement a
    WHERE longueur > 0.6 OR longueur < 0.4;

ALTER TABLE atd16_univerttrail.v_geo_zone_echauffement_alert_longueur OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_zone_echauffement_alert_longueur IS 
    '[ATD16] Vue de la couche geo_zone_echauffement dont la longueur n''est pas comprise entre 400 et 600 m';


-- ##################################################################################################################################################
-- ###                v_geo_zone_echauffement_sans_site : vue de la couche geo_zone_echauffement sans lien avec la couche geo_site                ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_zone_echauffement_sans_site;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_zone_echauffement_sans_site 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_zone_echauffement a
    WHERE id_site IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_zone_echauffement_sans_site OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_zone_echauffement_sans_site IS 
    '[ATD16] Vue de la couche geo_zone_echauffement sans lien avec la couche geo_site';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_zone_echauffement;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.geo_zone_echauffement
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_zone_echauffement IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_zone_echauffement;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.geo_zone_echauffement
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_zone_echauffement IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                         Mise à jour de la longueur du circuit (champ longueur_sig)                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER atd16_univerttrail.t_before_iu_maj_longueur_sig() ON atd16_univerttrail.geo_zone_echauffement;

CREATE TRIGGER t_before_iu_maj_longueur_sig
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_zone_echauffement 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_longueur_sig();
    
COMMENT ON TRIGGER t_before_iu_maj_longueur_sig ON atd16_univerttrail.geo_zone_echauffement IS 
    '[ATD16] Mise à jour de la longueur du circuit';

