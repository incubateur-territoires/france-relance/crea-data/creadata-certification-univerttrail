-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                    Table non géographique : Domaine de valeur des zones de départ                                          ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_zone_depart;

CREATE TABLE atd16_univerttrail.lst_zone_depart 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de la zone de départ
    libelle varchar(254), --[UVT] Libellé de la zone de départ
    CONSTRAINT pk_lst_zone_depart PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_zone_depart OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_zone_depart TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_zone_depart IS '[UVT] Domaine de valeur des zones de départ';

COMMENT ON COLUMN atd16_univerttrail.lst_zone_depart.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_zone_depart.code IS '[ATD16] Code de la zone de départ';
COMMENT ON COLUMN atd16_univerttrail.lst_zone_depart.libelle IS '[UVT] Libellé de la zone de départ';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_zone_depart 
    (code, libelle)
VALUES
    ('01', 'Porte principale'),
    ('02', 'Porte secondaire');

