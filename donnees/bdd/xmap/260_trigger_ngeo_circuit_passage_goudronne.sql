-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/05 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_longueur_goudronne() et du trigger associé t_after_iu_maj_longueur_goudronne
--                 . Ajout de la fonction f_maj_proportion_goudronne() et du trigger associé t_after_iu_maj_proportion_goudronne
-- 2021/08/06 : SL / Modification du nom du fichier en 260_trigger_ngeo_circuit_passage_goudronne.sql. S'applique à ngeo_circuit_passage_goudronne
--                 . Modification de la fonction f_maj_longueur_goudronne() et du trigger associé t_after_iud_maj_longueur_goudronne
--                 . Modification de la fonction f_maj_proportion_goudronne() et du trigger associé t_after_iud_maj_proportion_goudronne
-- 2021/10/26 : SL / Modification du nom de la fonction f_maj_proportion_goudronne_pago() et du trigger associé t_after_iud_maj_proportion_goudronne_pago

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                             Fonctions triggers et triggers spécifiques à la table ngeo_circuit_passage_goudronne                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                 Mise à jour de la longueur totale des passages goudronnés dans geo_circuit                                 ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_longueur_goudronne() et le trigger t_after_iud_maj_longueur_goudronne
-- 2 - La fonction-trigger f_maj_proportion_goudronne_pago() et le trigger t_after_iud_maj_proportion_goudronne_pago
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_longueur_goudronne();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_longueur_goudronne()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN -- Si il y a suppression de données dans la table ngeo_circuit_passage_goudronne alors
        UPDATE atd16_univerttrail.geo_circuit -- On met à jour la table geo_circuit
	    SET longueur_goudronne = --Le champ longueur_goudronne prend la valeur
	        (
                SELECT SUM(b.longueur) 
                FROM atd16_univerttrail.geo_passage_goudronne b 
                LEFT JOIN atd16_univerttrail.ngeo_circuit_passage_goudronne c ON b.gid = c.id_passage_goudronne
                WHERE c.id_circuit = OLD.id_circuit
            ) 
	        --de la somme des longueurs des différents passages goudronnés ayant un id_circuit identique à celui qui est supprimé
	    WHERE gid::text = OLD.id_circuit::text; -- Où l'ancien id_circuit est égal au gid de la table geo_circuit
    ELSE
        UPDATE atd16_univerttrail.geo_circuit -- On met à jour la table geo_circuit
	    SET longueur_goudronne = --Le champ longueur_goudronne prend la valeur
	        (
                SELECT SUM(b.longueur) 
                FROM atd16_univerttrail.geo_passage_goudronne b 
                LEFT JOIN atd16_univerttrail.ngeo_circuit_passage_goudronne c ON b.gid = c.id_passage_goudronne
                WHERE c.id_circuit = NEW.id_circuit
            ) 
	        --de la somme des longueurs des différents passages goudronnés ayant un id_circuit identique à celui qui est inséré ou maj
	    WHERE gid::text = NEW.id_circuit::text; -- Où le nouvel id_circuit est égal au gid de la table geo_circuit
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_longueur_goudronne() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_longueur_goudronne() IS 
    '[ATD16] Mise à jour de la longueur des portions goudronnées totale dans geo_circuit';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_longueur_goudronne 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_univerttrail.ngeo_circuit_passage_goudronne 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_longueur_goudronne();
    
COMMENT ON TRIGGER t_after_iud_maj_longueur_goudronne ON atd16_univerttrail.ngeo_circuit_passage_goudronne IS 
    '[ATD16] Mise à jour de la longueur des portions goudronnées totale dans geo_circuit';


-- ##################################################################################################################################################
-- ###                                   Mise à jour de la proportion des passages goudronnés dans geo_circuit                                    ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_longueur_goudronne() et le trigger t_after_iud_maj_longueur_goudronne
-- 2 - La fonction-trigger f_maj_proportion_goudronne_pago() et le trigger t_after_iud_maj_proportion_goudronne_pago
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_proportion_goudronne_pago();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_proportion_goudronne_pago()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN -- Si il y a suppression de données dans la table ngeo_circuit_passage_goudronne alors
        UPDATE atd16_univerttrail.geo_circuit -- On met à jour la table geo_circuit
        SET proportion_goudronne = --Le champ proportion_goudronne prend la valeur
        (
            SELECT SUM(b.longueur) 
            FROM atd16_univerttrail.geo_passage_goudronne b 
            LEFT JOIN atd16_univerttrail.ngeo_circuit_passage_goudronne c ON b.gid = c.id_passage_goudronne
            WHERE c.id_circuit = OLD.id_circuit
        ) 
        --de la somme des longueurs des différents passages goudronnés ayant un id_circuit identique à celui qui est supprimé
        / longueur_gps
        -- divisé par la longueur totale
        * 100
        -- multiplié par 100 pour obtenir un pourcentage
        WHERE gid::text = OLD.id_circuit::text; -- Où l'ancien id_circuit est égal au gid de la table geo_circuit
    ELSE
        UPDATE atd16_univerttrail.geo_circuit -- On met à jour la table geo_circuit
        SET proportion_goudronne = --Le champ proportion_goudronne prend la valeur
        (
            SELECT SUM(b.longueur) 
            FROM atd16_univerttrail.geo_passage_goudronne b 
            LEFT JOIN atd16_univerttrail.ngeo_circuit_passage_goudronne c ON b.gid = c.id_passage_goudronne
            WHERE c.id_circuit = NEW.id_circuit
        ) 
        --de la somme des longueurs des différents passages goudronnés ayant un id_circuit identique à celui qui est créé ou maj
        / longueur_gps
        -- divisé par la longueur totale
        * 100
        -- multiplié par 100 pour obtenir un pourcentage
        WHERE gid::text = NEW.id_circuit::text; -- Où le nouvel id_circuit est égal au gid de la table geo_circuit
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_proportion_goudronne_pago() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_proportion_goudronne_pago() IS 
    '[ATD16] Mise à jour de la proportion des portions goudronnées dans geo_circuit lors d''une modification d''un passage goudronné';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_proportion_goudronne_pago
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_univerttrail.ngeo_circuit_passage_goudronne 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_proportion_goudronne_pago();
    
COMMENT ON TRIGGER t_after_iud_maj_proportion_goudronne_pago ON atd16_univerttrail.ngeo_circuit_passage_goudronne IS 
    '[ATD16] Mise à jour de la proportion des portions goudronnées dans geo_circuit';

