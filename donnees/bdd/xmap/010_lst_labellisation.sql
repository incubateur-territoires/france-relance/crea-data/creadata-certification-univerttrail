-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                     Table non géographique : Domaine de valeur de la labellisation                                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_labellisation;

CREATE TABLE atd16_univerttrail.lst_labellisation 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de la labellisation
    libelle varchar(254), --[UVT] Libellé de la labellisation
    CONSTRAINT pk_lst_labellisation PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_labellisation OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_labellisation TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_labellisation IS '[UVT] Intitulé de la labellisation Uni''vert trail';

COMMENT ON COLUMN atd16_univerttrail.lst_labellisation.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_labellisation.code IS '[ATD16] Code de la labellisation';
COMMENT ON COLUMN atd16_univerttrail.lst_labellisation.libelle IS '[UVT] Libellé de la labellisation';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_labellisation 
    (code, libelle)
VALUES
    ('01', 'Uni''vert trail 1 Argent'),
    ('02', 'Uni''vert trail 2 Or'),
    ('03', 'Demande en cours');

