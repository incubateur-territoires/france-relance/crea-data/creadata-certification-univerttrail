-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/06 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                           Table non géographique : Lien circuit-passage privé                                              ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.ngeo_circuit_passage_prive;

CREATE TABLE atd16_univerttrail.ngeo_circuit_passage_prive
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    id_circuit integer, --[FK][ATD16] Identifiant du circuit
    id_passage_prive integer, --[FK][ATD16] Identifiant du passage privé
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    CONSTRAINT pk_ngeo_circuit_passage_prive PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.ngeo_circuit_passage_prive OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.ngeo_circuit_passage_prive TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.ngeo_circuit_passage_prive IS '[ATD16] Description de la fonctionnalité de la table';

COMMENT ON COLUMN atd16_univerttrail.ngeo_circuit_passage_prive.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.ngeo_circuit_passage_prive.id_circuit IS '[FK][ATD16] Identifiant du circuit';
COMMENT ON COLUMN atd16_univerttrail.ngeo_circuit_passage_prive.id_passage_prive IS '[FK][ATD16] Identifiant du passage privé';
COMMENT ON COLUMN atd16_univerttrail.ngeo_circuit_passage_prive.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.ngeo_circuit_passage_prive.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.ngeo_circuit_passage_prive;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.ngeo_circuit_passage_prive
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.ngeo_circuit_passage_prive IS 
    '[ATD16] Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.ngeo_circuit_passage_prive;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.ngeo_circuit_passage_prive
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.ngeo_circuit_passage_prive IS
    '[ATD16] Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

