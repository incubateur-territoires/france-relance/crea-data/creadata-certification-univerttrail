-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                          Table non géographique : Domaine de valeur de la catégorie de la longueur du circuit                              ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_categorie_longueur;

CREATE TABLE atd16_univerttrail.lst_categorie_longueur 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de la catégorie
    libelle varchar(254), --[UVT] Libellé de la catégorie
    note integer, --[UVT] Note de la catégorie
    CONSTRAINT pk_lst_categorie_longueur PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_categorie_longueur OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_categorie_longueur TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_categorie_longueur IS '[UVT] Domaine de valeur de la catégorie de la longueur du circuit';

COMMENT ON COLUMN atd16_univerttrail.lst_categorie_longueur.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_longueur.code IS '[ATD16] Code de la catégorie';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_longueur.libelle IS '[UVT] Libellé de la catégorie';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_longueur.note IS '[UVT] Note de la catégorie';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_categorie_longueur 
    (code, libelle, note)
VALUES
    ('01', '< 10 km', '2'),
    ('02', '10-15 km', '3'),
    ('03', '15-20 km', '4'),
    ('04', '20-25 km', '5'),
    ('05', '25-30 km', '6'),
    ('06', '30-45 km', '10'),
    ('07', '> 45 km', '14');

