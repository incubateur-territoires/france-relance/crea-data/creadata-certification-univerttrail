-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/07 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/08/04 : SL / Ajout du trigger t_before_iu_maj_longueur_sig
-- 2021/08/06 : SL / Suppression du champ id_circuit ; le lien se fait dans la table ngeo_circuit_passage_goudronne
-- 2021/10/28 : SL / Ajout de la vue v_geo_passage_goudronne_sans_site

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                    Table géographique : Passage goudronné                                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.geo_passage_goudronne;

CREATE TABLE atd16_univerttrail.geo_passage_goudronne
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    id_site integer, --[FK][UVT] Identifiant du site
    longueur_sig numeric(5,3), --[UVT] Longueur du tronçon (automatique)
    longueur numeric(5,3), --[UVT] Longueur du tronçon
    ident varchar(80), --[SIRAP] Identificaion de l'objet
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_passage_goudronne PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.geo_passage_goudronne OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.geo_passage_goudronne TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.geo_passage_goudronne IS '[ATD16] Table géographique contenant les tronçons goudronnées';

COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.id_site IS '[FK][UVT] Identifiant du site';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.longueur_sig IS '[UVT] Longueur du tronçon (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.longueur IS '[UVT] Longueur du tronçon';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.ident IS '[SIRAP] Identificaion de l''objet';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                v_geo_passage_goudronne_sans_site : vue de la couche geo_passage_goudronne sans lien avec la couche geo_site                ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_passage_goudronne_sans_site;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_passage_goudronne_sans_site 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_passage_goudronne a
    WHERE id_site IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_passage_goudronne_sans_site OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_passage_goudronne_sans_site IS 
    '[ATD16] Vue de la couche geo_passage_goudronne sans lien avec la couche geo_site';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_passage_goudronne;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.geo_passage_goudronne
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_passage_goudronne IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_passage_goudronne;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.geo_passage_goudronne
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_passage_goudronne IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                 Mise à jour de la longueur du passage goudronné (champ longueur_sig)                                       ###
-- ##################################################################################################################################################

-- DROP TRIGGER atd16_univerttrail.t_before_iu_maj_longueur_sig() ON atd16_univerttrail.geo_passage_goudronne;

CREATE TRIGGER t_before_iu_maj_longueur_sig
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_passage_goudronne 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_longueur_sig();
    
COMMENT ON TRIGGER t_before_iu_maj_longueur_sig ON atd16_univerttrail.geo_passage_goudronne IS 
    '[ATD16] Mise à jour de la longueur du passage goudronné';

