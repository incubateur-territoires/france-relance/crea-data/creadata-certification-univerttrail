-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_nb_circuit() et du trigger associé t_after_iud_maj_nb_circuit
--                 . Ajout de la fonction f_maj_longueur_iti_totale() et du trigger associé t_after_iud_maj_longueur_iti_totale
-- 2021/08/03 : SL / Ajout de la fonction f_maj_longueur_sig() et du trigger associé t_before_iu_maj_longueur_sig
--                 . Ajout de la fonction f_maj_categorie_longueur() et du trigger associé t_before_iu_maj_categorie_longueur
--                 . Ajout de la fonction f_maj_categorie_denivele_positif() et du trigger associé t_before_iu_maj_categorie_denivele_positif
--                 . Ajout de la fonction f_maj_categorie_denivele_continu_max() et du trigger associé t_before_iu_maj_categorie_denivele_continu_max
-- 2021/08/04 : SL / Ajout de la fonction f_maj_cotation() et du trigger associé t_before_iu_maj_cotation
--                 . La fonction f_maj_longueur_sig() et son trigger associé t_before_iu_maj_longueur_sig dispatchés dans les triggers génériques
-- 2021/08/06 : SL / Ajout de la fonction f_supp_lien_circuit_passage_prive() et du trigger associé t_after_d_circuit_supp_lien_circuit_passage_prive
--                 . Ajout de la fonction f_supp_lien_circuit_passage_goudronne() et du trigger associé t_after_d_circuit_supp_lien_circuit_passage_goudronne
-- 2021/10/21 : SL / Ajout de la fonction f_maj_1longueur_gps() et du trigger associé t_before_iu_maj_1longueur_gps
-- 2021/10/26 : SL / Ajout de la fonction f_maj_proportion_goudronne_circuit() et du trigger associé t_before_u_maj_proportion_goudronne_circuit

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                       Fonctions triggers et triggers spécifiques à la table geo_circuit                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                              Mise à jour du nombre de circuit dans geo_site                                                ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_nb_circuit();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_nb_circuit()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_univerttrail.geo_site -- On met à jour la table geo_site
	    SET nb_circuit = --Le champ nb_circuit prend la valeur
	    (SELECT COUNT(b.gid) FROM atd16_univerttrail.geo_circuit b WHERE b.id_site = OLD.id_site) 
	    --de la somme des circuits ayant un id_site identique à celui qui est supprimé
	    WHERE gid::text = OLD.id_site::text; -- Où l'ancien' id_site est égal au gid de la table geo_site
    ELSE
        UPDATE atd16_univerttrail.geo_site -- On met à jour la table geo_site
	    SET nb_circuit = --Le champ nb_circuit prend la valeur
	    (SELECT COUNT(b.gid) FROM atd16_univerttrail.geo_circuit b WHERE b.id_site = NEW.id_site) 
	    --de la somme des circuits ayant un id_site identique à celui qui est créé
	    WHERE gid::text = NEW.id_site::text; -- Où le nouvel id_site est égal au gid de la table geo_site
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_nb_circuit() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_nb_circuit() IS '[ATD16] Mise à jour du nombre de circuit dans geo_site';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_nb_circuit 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_nb_circuit();
    
COMMENT ON TRIGGER t_after_iud_maj_nb_circuit ON atd16_univerttrail.geo_circuit IS '[ATD16] Mise à jour du nombre de circuit dans geo_site';


-- ##################################################################################################################################################
-- ###                                        Mise à jour du champ longueur_gps lorsqu'il n'est pas rempli                                        ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_1longueur_gps() et le trigger t_before_iu_maj_1longueur_gps
-- 2 - La fonction-trigger f_maj_categorie_longueur() et le trigger t_before_iu_maj_categorie_longueur
-- 2 - La fonction-trigger f_maj_longueur_iti_totale() et le trigger t_after_iud_maj_longueur_iti_totale (AFTER donc pas besoin de changer le nom)
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_1longueur_gps();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_1longueur_gps()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF NEW.longueur_gps IS NULL THEN
        NEW.longueur_gps = ST_Length(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154))*0.001;
        -- Calcul de la longueur de la nouvelle géométrie (reprojection car en 3857 la longueur est à peu près 50% plus grande)
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_1longueur_gps() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_1longueur_gps() IS '[ATD16] Mise à jour du champ longueur_gps lorsque sa valeur est NULL';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_1longueur_gps
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_1longueur_gps();
    
COMMENT ON TRIGGER t_before_iu_maj_1longueur_gps ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Mise à jour du champ longueur_gps lorsque sa valeur est NULL';


-- ##################################################################################################################################################
-- ###                                        Mise à jour de la longueur totale des circuits dans geo_site                                        ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_1longueur_gps() et le trigger t_before_iu_maj_1longueur_gps
-- 2 - La fonction-trigger f_maj_categorie_longueur() et le trigger t_before_iu_maj_categorie_longueur
-- 2 - La fonction-trigger f_maj_longueur_iti_totale() et le trigger t_after_iud_maj_longueur_iti_totale (AFTER donc pas besoin de changer le nom)
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_1longueur_gps() et le trigger t_before_iu_maj_1longueur_gps
-- 2 - La fonction-trigger f_maj_proportion_goudronne_circuit() et le trigger t_before_u_maj_proportion_goudronne_circuit
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_longueur_iti_totale();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_longueur_iti_totale()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_univerttrail.geo_site -- On met à jour la table geo_site
	    SET longueur_iti_totale = --Le champ longueur_iti_totale prend la valeur
	    (SELECT SUM(b.longueur_gps) FROM atd16_univerttrail.geo_circuit b WHERE b.id_site = OLD.id_site) 
	    --de la somme des longueurs des différents circuits ayant un id_site identique à celui qui est supprimé
	    WHERE gid::text = OLD.id_site::text; -- Où l'ancien' id_site est égal au gid de la table geo_site
    ELSE
        UPDATE atd16_univerttrail.geo_site -- On met à jour la table geo_site
	    SET longueur_iti_totale = --Le champ longueur_iti_totale prend la valeur
	    (SELECT SUM(b.longueur_gps) FROM atd16_univerttrail.geo_circuit b WHERE b.id_site = NEW.id_site) 
	    --de la somme des longueurs des différents circuits ayant un id_site identique à celui qui est créé
	    WHERE gid::text = NEW.id_site::text; -- Où le nouvel id_site est égal au gid de la table geo_site
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_longueur_iti_totale() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_longueur_iti_totale() IS '[ATD16] Mise à jour de la longueur totale des circuits dans geo_site';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_longueur_iti_totale 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_longueur_iti_totale();
    
COMMENT ON TRIGGER t_after_iud_maj_longueur_iti_totale ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Mise à jour de la longueur totale des circuits dans geo_site';


-- ##################################################################################################################################################
-- ###                                                   Mise à jour du champ categorie_longueur                                                  ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_1longueur_gps() et le trigger t_before_iu_maj_1longueur_gps
-- 2 - La fonction-trigger f_maj_categorie_longueur() et le trigger t_before_iu_maj_categorie_longueur
-- 2 - La fonction-trigger f_maj_longueur_iti_totale() et le trigger t_after_iud_maj_longueur_iti_totale (AFTER donc pas besoin de changer le nom)
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_categorie_denivele_continu_max() et le trigger t_before_iu_maj_categorie_denivele_continu_max
-- 1 - La fonction-trigger f_maj_categorie_denivele_positif() et le trigger t_before_iu_maj_categorie_denivele_positif
-- 1 - La fonction-trigger f_maj_categorie_longueur() et le trigger t_before_iu_maj_categorie_longueur
-- 2 - La fonction-trigger f_maj_cotation() et le trigger t_before_iu_maj_cotation
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_categorie_longueur();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_categorie_longueur()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF NEW.longueur_gps < 10 THEN NEW.categorie_longueur = '01';
    ELSIF NEW.longueur_gps >= 10 AND NEW.longueur_gps < 15 THEN NEW.categorie_longueur = '02';
    ELSIF NEW.longueur_gps >= 15 AND NEW.longueur_gps < 20 THEN NEW.categorie_longueur = '03';
    ELSIF NEW.longueur_gps >= 20 AND NEW.longueur_gps < 25 THEN NEW.categorie_longueur = '04';
    ELSIF NEW.longueur_gps >= 25 AND NEW.longueur_gps < 30 THEN NEW.categorie_longueur = '05';
    ELSIF NEW.longueur_gps >= 30 AND NEW.longueur_gps < 45 THEN NEW.categorie_longueur = '06';
    ELSIF NEW.longueur_gps >= 45 THEN NEW.categorie_longueur = '07';
    ELSE NEW.categorie_longueur = '00';
	END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_categorie_longueur() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_categorie_longueur() IS '[ATD16] Mise à jour du champ categorie_longueur';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_categorie_longueur
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_categorie_longueur();
    
COMMENT ON TRIGGER t_before_iu_maj_categorie_longueur ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Mise à jour du champ categorie_longueur';


-- ##################################################################################################################################################
-- ###                                               Mise à jour du champ categorie_denivele_positif                                              ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_categorie_denivele_continu_max() et le trigger t_before_iu_maj_categorie_denivele_continu_max
-- 1 - La fonction-trigger f_maj_categorie_denivele_positif() et le trigger t_before_iu_maj_categorie_denivele_positif
-- 1 - La fonction-trigger f_maj_categorie_longueur() et le trigger t_before_iu_maj_categorie_longueur
-- 2 - La fonction-trigger f_maj_cotation() et le trigger t_before_iu_maj_cotation
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_categorie_denivele_positif();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_categorie_denivele_positif()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF NEW.denivele_positif < 500 THEN NEW.categorie_denivele_positif = '01';
    ELSIF NEW.denivele_positif >= 500 AND NEW.denivele_positif < 750 THEN NEW.categorie_denivele_positif = '02';
    ELSIF NEW.denivele_positif >= 750 AND NEW.denivele_positif < 1000 THEN NEW.categorie_denivele_positif = '03';
    ELSIF NEW.denivele_positif >= 1000 AND NEW.denivele_positif < 1500 THEN NEW.categorie_denivele_positif = '04';
    ELSIF NEW.denivele_positif >= 1500 AND NEW.denivele_positif < 2000 THEN NEW.categorie_denivele_positif = '05';
    ELSIF NEW.denivele_positif >= 2000 AND NEW.denivele_positif < 2500 THEN NEW.categorie_denivele_positif = '06';
    ELSIF NEW.denivele_positif >= 2500 THEN NEW.categorie_denivele_positif = '07';
    ELSE NEW.categorie_denivele_positif = '00';
	END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_categorie_denivele_positif() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_categorie_denivele_positif() IS '[ATD16] Mise à jour du champ categorie_denivele_positif';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_categorie_denivele_positif
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_categorie_denivele_positif();
    
COMMENT ON TRIGGER t_before_iu_maj_categorie_denivele_positif ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Mise à jour du champ categorie_denivele_positif';


-- ##################################################################################################################################################
-- ###                                             Mise à jour du champ categorie_denivele_continu_max                                            ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_categorie_denivele_continu_max() et le trigger t_before_iu_maj_categorie_denivele_continu_max
-- 1 - La fonction-trigger f_maj_categorie_denivele_positif() et le trigger t_before_iu_maj_categorie_denivele_positif
-- 1 - La fonction-trigger f_maj_categorie_longueur() et le trigger t_before_iu_maj_categorie_longueur
-- 2 - La fonction-trigger f_maj_cotation() et le trigger t_before_iu_maj_cotation
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_categorie_denivele_continu_max();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_categorie_denivele_continu_max()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF NEW.denivele_continu_max < 300 THEN NEW.categorie_denivele_continu_max = '01';
    ELSIF NEW.denivele_continu_max >= 300 AND NEW.denivele_continu_max < 600 THEN NEW.categorie_denivele_continu_max = '02';
    ELSIF NEW.denivele_continu_max >= 600 AND NEW.denivele_continu_max < 1000 THEN NEW.categorie_denivele_continu_max = '03';
    ELSIF NEW.denivele_continu_max >= 1000 THEN NEW.categorie_denivele_continu_max = '04';
    ELSE NEW.categorie_denivele_continu_max = '00';
	END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_categorie_denivele_continu_max() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_categorie_denivele_continu_max() IS '[ATD16] Mise à jour du champ categorie_denivele_continu_max';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_categorie_denivele_continu_max
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_categorie_denivele_continu_max();
    
COMMENT ON TRIGGER t_before_iu_maj_categorie_denivele_continu_max ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Mise à jour du champ categorie_denivele_continu_max';


-- ##################################################################################################################################################
-- ###                                             Mise à jour du champ cotation (addition des notes)                                             ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_categorie_denivele_continu_max() et le trigger t_before_iu_maj_categorie_denivele_continu_max
-- 1 - La fonction-trigger f_maj_categorie_denivele_positif() et le trigger t_before_iu_maj_categorie_denivele_positif
-- 1 - La fonction-trigger f_maj_categorie_longueur() et le trigger t_before_iu_maj_categorie_longueur
-- 2 - La fonction-trigger f_maj_cotation() et le trigger t_before_iu_maj_cotation
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_cotation();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_cotation()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    NEW.cotation = -- Le champ cotation du circuit créé ou modifié est égal à
    (SELECT b.note + c.note + d.note + e.note -- l'addition des notes correspondants aux 4 catégories
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON NEW.categorie_longueur::text = b.code::text -- jointures avec chacune des tables listes
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON NEW.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON NEW.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON NEW.technicite::text = e.code::text
    WHERE NEW.gid = a.gid); -- sélection de la ligne nouvellement créée
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_cotation() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_cotation() IS '[ATD16] Mise à jour du champ cotation (addition des notes)';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_cotation
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_cotation();
    
COMMENT ON TRIGGER t_before_iu_maj_cotation ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Mise à jour du champ cotation (addition des notes)';


-- ##################################################################################################################################################
-- ###                        Suppression des liens entre le circuit et les passages privés lorsque le circuit est supprimé                       ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_supp_lien_circuit_passage_prive();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_supp_lien_circuit_passage_prive()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    DELETE FROM atd16_univerttrail.ngeo_circuit_passage_prive
    WHERE id_circuit = OLD.gid;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_supp_lien_circuit_passage_prive() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_supp_lien_circuit_passage_prive() IS 
    '[ATD16] Suppression des liens entre le circuit et les passages privés (dans ngeo_circuit_passage_prive) lorsque le circuit est supprimé';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_circuit_supp_lien_circuit_passage_prive
    AFTER DELETE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_supp_lien_circuit_passage_prive();
    
COMMENT ON TRIGGER t_after_d_circuit_supp_lien_circuit_passage_prive ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Suppression des liens entre le circuit et les passages privés (dans ngeo_circuit_passage_prive) lorsque le circuit est supprimé';


-- ##################################################################################################################################################
-- ###                      Suppression des liens entre le circuit et les passages goudronnés lorsque le circuit est supprimé                     ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_supp_lien_circuit_passage_goudronne();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_supp_lien_circuit_passage_goudronne()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    DELETE FROM atd16_univerttrail.ngeo_circuit_passage_goudronne
    WHERE id_circuit = OLD.gid;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_supp_lien_circuit_passage_goudronne() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_supp_lien_circuit_passage_goudronne() IS 
    '[ATD16] Suppression des liens entre le circuit et les passages goudronnés (dans ngeo_circuit_passage_goudronne) lorsque le circuit est supprimé';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_circuit_supp_lien_circuit_passage_goudronne
    AFTER DELETE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_supp_lien_circuit_passage_goudronne();
    
COMMENT ON TRIGGER t_after_d_circuit_supp_lien_circuit_passage_goudronne ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Suppression des liens entre le circuit et les passages goudronnés (dans ngeo_circuit_passage_goudronne) lorsque le circuit est supprimé';


-- ##################################################################################################################################################
-- ###                                   Mise à jour de la proportion des passages goudronnés dans geo_circuit                                    ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_1longueur_gps() et le trigger t_before_iu_maj_1longueur_gps
-- 2 - La fonction-trigger f_maj_proportion_goudronne_circuit() et le trigger t_before_u_maj_proportion_goudronne_circuit
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_proportion_goudronne_circuit();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_proportion_goudronne_circuit()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    NEW.proportion_goudronne = NEW.longueur_goudronne / NEW.longueur_gps * 100;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_proportion_goudronne_circuit() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_proportion_goudronne_circuit() IS 
    '[ATD16] Mise à jour de la proportion des portions goudronnées dans geo_circuit lors d''une modification d''un circuit';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_u_maj_proportion_goudronne_circuit
    BEFORE UPDATE
    ON atd16_univerttrail.geo_circuit
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_proportion_goudronne_circuit();
    
COMMENT ON TRIGGER t_before_u_maj_proportion_goudronne_circuit ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Mise à jour de la proportion des portions goudronnées dans geo_circuit';

