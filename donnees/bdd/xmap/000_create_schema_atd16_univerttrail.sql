-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/06 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                          SCHEMA : Gestion des trails (certification UniVert'Trail)                                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP SCHEMA IF EXISTS atd16_univerttrail;

CREATE SCHEMA atd16_univerttrail;
ALTER SCHEMA atd16_univerttrail OWNER TO sditecgrp;
COMMENT ON SCHEMA atd16_univerttrail IS 'Certification UniVert''Trail';

