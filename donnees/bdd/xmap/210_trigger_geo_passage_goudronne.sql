-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/06 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_supp_lien_passage_goudronne_circuit() et du trigger associé t_after_d_passage_goudronne_supp_lien_passage_goudronne_circuit
-- 2021/10/21 : SL / Ajout de la fonction f_maj_longueur_passage_goudronne() et du trigger associé t_before_iu_maj_longueur_passage_goudronne

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                  Fonctions triggers et triggers spécifiques à la table geo_passage_goudronne                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                 Suppression des liens entre le circuit et les passages goudronnés lorsque le passage goudronné est supprimé                ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_supp_lien_passage_goudronne_circuit();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_supp_lien_passage_goudronne_circuit()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    DELETE FROM atd16_univerttrail.ngeo_circuit_passage_goudronne
    WHERE id_passage_goudronne = OLD.gid;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_supp_lien_passage_goudronne_circuit() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_supp_lien_passage_goudronne_circuit() IS 
    '[ATD16] Suppression des liens entre le circuit et les passages goudronnés (dans ngeo_circuit_passage_goudronne) lorsque le passage goudronné est supprimé';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_passage_goudronne_supp_lien_passage_goudronne_circuit
    AFTER DELETE
    ON atd16_univerttrail.geo_passage_goudronne 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_supp_lien_passage_goudronne_circuit();
    
COMMENT ON TRIGGER t_after_d_passage_goudronne_supp_lien_passage_goudronne_circuit ON atd16_univerttrail.geo_passage_goudronne IS 
    '[ATD16] Suppression des liens entre le circuit et les passages goudronnés (dans ngeo_circuit_passage_goudronne) lorsque le passage goudronné est supprimé';


-- ##################################################################################################################################################
-- ###                                          Mise à jour du champ longueur lorsqu'il n'est pas rempli                                          ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_longueur_passage_goudronne();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_longueur_passage_goudronne()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF NEW.longueur IS NULL THEN
        NEW.longueur = ST_Length(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154))*0.001;
        -- Calcul de la longueur de la nouvelle géométrie (reprojection car en 3857 la longueur est à peu près 50% plus grande)
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_longueur_passage_goudronne() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_longueur_passage_goudronne() IS '[ATD16] Mise à jour du champ longueur lorsque sa valeur est NULL';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_longueur_passage_goudronne
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_passage_goudronne 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_longueur_passage_goudronne();
    
COMMENT ON TRIGGER t_before_iu_maj_longueur_passage_goudronne ON atd16_univerttrail.geo_passage_goudronne IS 
    '[ATD16] Mise à jour du champ longueur lorsque sa valeur est NULL';

