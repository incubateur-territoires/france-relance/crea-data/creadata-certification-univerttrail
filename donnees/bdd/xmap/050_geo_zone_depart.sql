-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/07 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/10/27 : SL / Ajout de la vue v_geo_zone_depart_01_alert_incomplete
--                 . Ajout de la vue v_geo_zone_depart_02_alert_incomplete
-- 2021/10/28 : SL / Ajout de la vue v_geo_zone_depart_sans_site
--                 . Ajout de la vue v_geo_zone_depart_portes_principales_multiples
-- 2021/11/10 : SL / Refonte totale de la vue v_geo_zone_depart_01_alert_incomplete
--                 . Refonte totale de la vue v_geo_zone_depart_02_alert_incomplete

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                      Table géographique : Zone de départ                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.geo_zone_depart;

CREATE TABLE atd16_univerttrail.geo_zone_depart
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    id_site integer, --[FK][UVT] Identifiant du site
    type varchar(2), --[FK][UVT] Type de zone de départ
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Nom de la zone
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_zone_depart PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.geo_zone_depart OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.geo_zone_depart TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.geo_zone_depart IS '[ATD16] Table géographique contenant les zones de départ';

COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.id_site IS '[FK][UVT] Identifiant du site';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.type IS '[FK][UVT] Type de zone de départ';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.ident IS '[SIRAP] Identificaion de l''objet / Nom de la zone';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###      v_geo_zone_depart_01_alert_incomplete : vue de geo_zone_depart sans panneau d'info, de parking, de point d'eau et/ou de poubelles     ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_zone_depart_01_alert_incomplete;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_zone_depart_01_alert_incomplete 
    AS 
    WITH -- Permet la création de tables temporaires
        tb_nb_panneau_info AS ( -- Création de la table tb_nb_panneau_info qui calcule le nombre de panneau d'info pour chaque zone de départ p.
            (SELECT z.gid, COUNT(y.gid) AS nb_panneau_info -- On compte le nombre d'objet
            FROM atd16_univerttrail.geo_signaletique y, atd16_univerttrail.geo_zone_depart z -- de la table geo_signaletique
            WHERE ST_Intersects(y.the_geom, z.the_geom) AND y.type_signaletique = '01' AND z.type = '01'
            GROUP BY z.gid)
        ),
        tb_nb_parking AS (
            (SELECT z.gid, COUNT(x.gid) AS nb_parking
            FROM atd16_univerttrail.geo_parking x, atd16_univerttrail.geo_zone_depart z
            WHERE ST_Intersects(x.the_geom, z.the_geom) AND z.type = '01'
            GROUP BY z.gid)
        ),
        tb_nb_point_d_eau AS (
            (SELECT z.gid, COUNT(w.gid) AS nb_point_d_eau
            FROM atd16_univerttrail.geo_equipement w, atd16_univerttrail.geo_zone_depart z
            WHERE ST_Intersects(w.the_geom, z.the_geom) AND w.type_equipement = '05' AND z.type = '01'
            GROUP BY z.gid)
        ),
        tb_nb_poubelles AS (
            (SELECT z.gid, COUNT(v.gid) AS nb_poubelles
            FROM atd16_univerttrail.geo_equipement v, atd16_univerttrail.geo_zone_depart z
            WHERE ST_Intersects(v.the_geom, z.the_geom) AND v.type_equipement = '04' AND z.type = '01'
            GROUP BY z.gid)
        )
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        CASE WHEN npi.nb_panneau_info IS NULL THEN 0 ELSE npi.nb_panneau_info END,
        CASE WHEN npk.nb_parking IS NULL THEN 0 ELSE npk.nb_parking END,
        CASE WHEN npe.nb_point_d_eau IS NULL THEN 0 ELSE npe.nb_point_d_eau END,
        CASE WHEN npb.nb_poubelles IS NULL THEN 0 ELSE npb.nb_poubelles END,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_zone_depart a
    LEFT JOIN tb_nb_panneau_info npi ON a.gid = npi.gid
    LEFT JOIN tb_nb_parking npk ON a.gid = npk.gid
    LEFT JOIN tb_nb_point_d_eau npe ON a.gid = npe.gid
    LEFT JOIN tb_nb_poubelles npb ON a.gid = npb.gid
    WHERE 
    (
        (CASE WHEN npi.nb_panneau_info IS NULL THEN 0 ELSE npi.nb_panneau_info END) = 0
    OR
        (CASE WHEN npk.nb_parking IS NULL THEN 0 ELSE npk.nb_parking END) = 0
    OR
        (CASE WHEN npe.nb_point_d_eau IS NULL THEN 0 ELSE npe.nb_point_d_eau END) = 0
    OR
        (CASE WHEN npb.nb_poubelles IS NULL THEN 0 ELSE npb.nb_poubelles END) = 0
    )
    AND
        a.type = '01';

ALTER TABLE atd16_univerttrail.v_geo_zone_depart_01_alert_incomplete OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_zone_depart_01_alert_incomplete IS 
    '[ATD16] Vue de la couche geo_zone_depart sans panneau d''info, de parking, de point d''eau et/ou de poubelles';


-- ##################################################################################################################################################
-- ###               v_geo_zone_depart_02_alert_incomplete : vue de geo_zone_depart sans panneau d'info ou totem et/ou sans parking               ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_zone_depart_02_alert_incomplete;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_zone_depart_02_alert_incomplete 
    AS 
    WITH -- Permet la création de tables temporaires
        tb_nb_panneau_info AS ( -- Création de la table tb_nb_panneau_info qui calcule le nombre de panneau d'info pour chaque zone de départ p.
            (SELECT z.gid, COUNT(y.gid) AS nb_panneau_info -- On compte le nombre d'objet
            FROM atd16_univerttrail.geo_signaletique y, atd16_univerttrail.geo_zone_depart z -- de la table geo_signaletique
            WHERE ST_Intersects(y.the_geom, z.the_geom) AND y.type_signaletique = '01' AND z.type = '02'
            -- qui intersecte la zone de départ, dont le type de signalétique = '01' et dont le type de zone = '02'
            GROUP BY z.gid)
        ),
        tb_nb_totem AS (
            (SELECT z.gid, COUNT(w.gid) AS nb_totem
            FROM atd16_univerttrail.geo_signaletique w, atd16_univerttrail.geo_zone_depart z
            WHERE ST_Intersects(w.the_geom, z.the_geom) AND w.type_signaletique = '02' AND z.type = '02'
            GROUP BY z.gid)
        ),
        tb_nb_parking AS (
            (SELECT z.gid, COUNT(x.gid) AS nb_parking
            FROM atd16_univerttrail.geo_parking x, atd16_univerttrail.geo_zone_depart z
            WHERE ST_Intersects(x.the_geom, z.the_geom) AND z.type = '02'
            GROUP BY z.gid)
        )
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        (CASE WHEN npi.nb_panneau_info IS NULL THEN 0 ELSE npi.nb_panneau_info END
        +
        CASE WHEN nt.nb_totem IS NULL THEN 0 ELSE nt.nb_totem END) AS nb_panneau_et_totem,
        CASE WHEN npk.nb_parking IS NULL THEN 0 ELSE npk.nb_parking END,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_zone_depart a
    LEFT JOIN tb_nb_panneau_info npi ON a.gid = npi.gid
    LEFT JOIN tb_nb_totem nt ON a.gid = nt.gid
    LEFT JOIN tb_nb_parking npk ON a.gid = npk.gid
    WHERE 
    (
        (CASE WHEN npi.nb_panneau_info IS NULL THEN 0 ELSE npi.nb_panneau_info END
        +
        CASE WHEN nt.nb_totem IS NULL THEN 0 ELSE nt.nb_totem END) = 0
    OR
        (CASE WHEN npk.nb_parking IS NULL THEN 0 ELSE npk.nb_parking END) = 0
    )
    AND
        a.type = '02';

ALTER TABLE atd16_univerttrail.v_geo_zone_depart_02_alert_incomplete OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_zone_depart_02_alert_incomplete IS 
    '[ATD16] Vue de la couche geo_zone_depart sans panneau d''info ou totem et/ou sans parking';


-- ##################################################################################################################################################
-- ###                      v_geo_zone_depart_sans_site : vue de la couche geo_zone_depart sans lien avec la couche geo_site                      ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_zone_depart_sans_site;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_zone_depart_sans_site 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        a.type,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_zone_depart a
    WHERE id_site IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_zone_depart_sans_site OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_zone_depart_sans_site IS 
    '[ATD16] Vue de la couche geo_zone_depart sans lien avec la couche geo_site';


-- ##################################################################################################################################################
-- ###    v_geo_zone_depart_portes_principales_multiples : vue de geo_zone_depart qui comporte plusieurs portes principales pour un seul site     ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_zone_depart_portes_principales_multiples;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_zone_depart_portes_principales_multiples 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        a.type,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_zone_depart a
    WHERE EXISTS ( -- EXISTS vérifie si la sous-requête retourne un résultat ce qui permet à la requête de s'exécuter
        SELECT 
            b.gid,
            b.insee,
            b.id_site,
            b.ident,
            b.type,
            b.date_creation,
            b.date_maj,
            b.datesig,
            b.origdata,
            b.the_geom
        FROM atd16_univerttrail.geo_zone_depart b -- Même table que la requête
        WHERE a.gid <> b.gid -- Et on les compare entre elles (le gid doit être différent) 
        AND a.id_site = b.id_site -- Le champ id_site similaire
        AND a.type = b.type -- Le champ type similaire aussi
        AND type = '01' -- Et il doit avoir la valeur à 01
    );

ALTER TABLE atd16_univerttrail.v_geo_zone_depart_portes_principales_multiples OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_zone_depart_portes_principales_multiples IS 
    '[ATD16] Vue de la couche geo_zone_depart qui comporte plusieurs portes principales pour un seul site';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_zone_depart;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.geo_zone_depart
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_zone_depart IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_zone_depart;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.geo_zone_depart
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_zone_depart IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

