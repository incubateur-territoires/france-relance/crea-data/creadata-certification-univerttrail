-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                Table non géographique : Domaine de valeur de l'autorisation de passage                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_autorisation_passage;

CREATE TABLE atd16_univerttrail.lst_autorisation_passage 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    libelle varchar(254), --[UVT] Libellé de l'autorisation de passage
    CONSTRAINT pk_lst_autorisation_passage PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_autorisation_passage OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_autorisation_passage TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_autorisation_passage IS '[UVT] Domaine de valeur de l''autorisation de passage';

COMMENT ON COLUMN atd16_univerttrail.lst_autorisation_passage.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_autorisation_passage.libelle IS '[UVT] Libellé de l''autorisation de passage';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_autorisation_passage 
    (libelle)
VALUES
    ('Oui'),
    ('Non');

