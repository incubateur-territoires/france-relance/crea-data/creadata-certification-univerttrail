-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/06 : SL / Création du fichier sur Git
-- 2021/08/04 : SL / Ajout de la fonction f_maj_longueur_sig()

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Fonction(s) trigger(s) générique(s)                                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_univerttrail.f_date_creation();

CREATE FUNCTION atd16_univerttrail.f_date_creation()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_creation = now(); -- À la création d'un objet, le champ date_creation prend la valeur de la date du jour
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_univerttrail.f_date_creation() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_date_creation() IS 'Fonction trigger permettant l''initialisation du champ date_creation';


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_univerttrail.f_date_maj();

CREATE FUNCTION atd16_univerttrail.f_date_maj()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_maj = now(); -- À la modification d'un objet, le champ date_maj prend la valeur de la date du jour
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_univerttrail.f_date_maj() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_date_maj() IS 'Fonction trigger permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                               Mise à jour de la longueur (champ longueur_sig)                                              ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_longueur_sig();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_longueur_sig()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    NEW.longueur_sig := ST_Length(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154))*0.001; 
    -- Calcul de la longueur de la nouvelle géométrie (reprojection car en 3857 la longueur est à peu près 50% plus grande)
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_longueur_sig() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_longueur_sig() IS '[ATD16] Mise à jour de la longueur';

