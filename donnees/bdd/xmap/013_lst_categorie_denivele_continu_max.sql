-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Modification de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                         Table non géographique : Domaine de valeur de la catégorie du dénivelé continu maximum                             ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_categorie_denivele_continu_max;

CREATE TABLE atd16_univerttrail.lst_categorie_denivele_continu_max 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de la catégorie
    libelle varchar(254), --[UVT] Libellé de la catégorie
    note integer, --[UVT] Note de la catégorie
    CONSTRAINT pk_lst_categorie_denivele_continu_max PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_categorie_denivele_continu_max OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_categorie_denivele_continu_max TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_categorie_denivele_continu_max IS '[UVT] Domaine de valeur de la catégorie du dénivelé continu maximum';

COMMENT ON COLUMN atd16_univerttrail.lst_categorie_denivele_continu_max.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_denivele_continu_max.code IS '[ATD16] Code de la catégorie';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_denivele_continu_max.libelle IS '[UVT] Libellé de la catégorie';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_denivele_continu_max.note IS '[UVT] Note de la catégorie';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_categorie_denivele_continu_max 
    (code, libelle, note)
VALUES
    ('00', 'Pas de dénivelé continu max renseigné', NULL),
    ('01', '< 300 m', '1'),
    ('02', '300-600 m', '2'),
    ('03', '600-1000 m', '3'),
    ('04', '> 1000 m', '4');

