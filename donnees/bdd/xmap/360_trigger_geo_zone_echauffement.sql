-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/10/22 : SL / Création du fichier sur Git
-- 2021/10/22 : SL / Ajout de la fonction f_maj_longueur_zone_echauffement() et du trigger associé t_before_iu_maj_longueur_zone_echauffement

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                  Fonctions triggers et triggers spécifiques à la table geo_zone_echauffement                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                          Mise à jour du champ longueur lorsqu'il n'est pas rempli                                          ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_longueur_zone_echauffement();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_longueur_zone_echauffement()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF NEW.longueur IS NULL THEN
        NEW.longueur = ST_Length(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154))*0.001;
        -- Calcul de la longueur de la nouvelle géométrie (reprojection car en 3857 la longueur est à peu près 50% plus grande)
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_longueur_zone_echauffement() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_longueur_zone_echauffement() IS '[ATD16] Mise à jour du champ longueur lorsque sa valeur est NULL';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_longueur_zone_echauffement
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_zone_echauffement 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_longueur_zone_echauffement();
    
COMMENT ON TRIGGER t_before_iu_maj_longueur_zone_echauffement ON atd16_univerttrail.geo_zone_echauffement IS 
    '[ATD16] Mise à jour du champ longueur lorsque sa valeur est NULL';

