-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/06 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/07/08 : SL / Modification du nom du champ certification en labellisation
-- 2021/10/26 : SL / Ajout de la vue v_geo_site_alert_longueur_iti_totale
--                 . Ajout de la vue v_geo_site_alert_circuit_non_heterogene
--                 . Ajout de la vue v_geo_site_alert_sans_zone
-- 2021/10/27 : SL / Refonte totale de la vue v_geo_site_alert_circuit_non_heterogene
--                 . Refonte totale de la vue v_geo_site_alert_sans_zone
-- 2021/11/09 : SL / Refonte de la vue v_geo_site_alert_sans_zone : on enlève la table des zones d'accueil
--                 . Mise en commentaire de l'ancienne vue v_geo_site_alert_sans_zone_old

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Table géographique : Site                                                        ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.geo_site;

CREATE TABLE atd16_univerttrail.geo_site
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    nb_circuit integer, --[UVT] Nombre de circuit du site (automatique)
    longueur_iti_totale numeric(8,3), --[UVT] Longueur totale des circuits en km (automatique)
    labellisation varchar(2), --[FK][UVT] Labellisation UniVert'Trail (liste déroulante)
    url varchar(2054), --[ATD16] Adresse URL du site internet
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Nom du site
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_site PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.geo_site OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.geo_site TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.geo_site IS '[ATD16] Table géographique contenant les sites UniVert''Trail';

COMMENT ON COLUMN atd16_univerttrail.geo_site.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.geo_site.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_univerttrail.geo_site.nb_circuit IS '[UVT] Nombre de circuit du site (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_site.longueur_iti_totale IS '[UVT] Longueur totale des circuits en km (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_site.labellisation IS '[FK][UVT] Labellisation UniVert''Trail (liste déroulante)';
COMMENT ON COLUMN atd16_univerttrail.geo_site.url IS '[ATD16] Adresse URL du site internet';
COMMENT ON COLUMN atd16_univerttrail.geo_site.ident IS '[SIRAP] Identificaion de l''objet / Nom du site';
COMMENT ON COLUMN atd16_univerttrail.geo_site.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_site.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_site.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_site.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_site.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###          v_geo_site_alert_longueur_iti_totale : vue de la couche geo_site dont le champ longueur_iti_totale est inférieur à 40 km          ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_site_alert_longueur_iti_totale;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_site_alert_longueur_iti_totale 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.nb_circuit,
        a.longueur_iti_totale,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_site a
    WHERE longueur_iti_totale < 40 OR longueur_iti_totale IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_site_alert_longueur_iti_totale OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_site_alert_longueur_iti_totale IS 
    '[ATD16] Vue de la couche geo_site dont le champ longueur_iti_totale est inférieur à 40 km';


-- ##################################################################################################################################################
-- ###   v_geo_site_alert_sans_zone_old : vue de la couche geo_site sans zone de départ et/ou sans zone d'accueil (obsolete sans zone d'accueil)  ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_site_alert_sans_zone_old;
/*
CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_site_alert_sans_zone_old 
    AS 
    WITH -- Permet la création de tables temporaires
        nb_zone_accueil AS ( -- Création de la table nb_zone_accueil qui calcule le nombre de zone d'accueil pour chaque site
            (SELECT a.gid, COUNT(b.gid) AS nb_zone_accueil
            FROM atd16_univerttrail.geo_site a
            LEFT JOIN atd16_univerttrail.geo_zone_accueil b ON a.gid = b.id_site
            GROUP BY a.gid)
        ),
        nb_zone_depart AS (
            (SELECT a.gid, COUNT(c.gid) AS nb_zone_depart
            FROM atd16_univerttrail.geo_site a
            LEFT JOIN atd16_univerttrail.geo_zone_depart c ON a.gid = c.id_site
            GROUP BY a.gid)
        )
    SELECT
        a.gid,
        a.insee,
        a.ident,
        a.nb_circuit,
        a.longueur_iti_totale,
        za.nb_zone_accueil,
        zd.nb_zone_depart,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_site a
    LEFT JOIN nb_zone_accueil za ON a.gid = za.gid
    LEFT JOIN nb_zone_depart zd ON a.gid = zd.gid
    WHERE za.nb_zone_accueil = 0
        OR zd.nb_zone_depart = 0;

ALTER TABLE atd16_univerttrail.v_geo_site_alert_sans_zone_old OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_site_alert_sans_zone_old IS 
    '[ATD16] Vue de la couche geo_site sans zone de départ et/ou sans zone d''accueil';
*/

-- ##################################################################################################################################################
-- ###                            v_geo_site_alert_sans_zone : vue de la couche geo_site sans zone de départ principale                           ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_site_alert_sans_zone;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_site_alert_sans_zone 
    AS 
    WITH -- Permet la création de tables temporaires
        nb_zone_depart AS ( -- Création de la table nb_zone_depart qui calcule le nombre de zone de départ principale pour chaque site
            (SELECT a.gid, COUNT(c.gid) AS nb_zone_depart
            FROM atd16_univerttrail.geo_site a
            LEFT JOIN atd16_univerttrail.geo_zone_depart c ON a.gid = c.id_site
            WHERE c.type = '01'
            GROUP BY a.gid)
        )
    SELECT
        a.gid,
        a.insee,
        a.ident,
        a.nb_circuit,
        a.longueur_iti_totale,
        CASE WHEN zd.nb_zone_depart IS NULL THEN 0 ELSE zd.nb_zone_depart END,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_site a
    LEFT JOIN nb_zone_depart zd ON a.gid = zd.gid
    WHERE (CASE WHEN zd.nb_zone_depart IS NULL THEN 0 ELSE zd.nb_zone_depart END) = 0;

ALTER TABLE atd16_univerttrail.v_geo_site_alert_sans_zone OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_site_alert_sans_zone IS 
    '[ATD16] Vue de la couche geo_site sans zone de départ principale';


-- ##################################################################################################################################################
-- ###          v_geo_site_alert_circuit_non_heterogene : vue de la couche geo_site ayant moins de trois circuits de niveaux différents           ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_site_alert_circuit_non_heterogene;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_site_alert_circuit_non_heterogene
    AS
    WITH -- Permet de créer des tables temporaires
        table_01 AS ( -- Création de la table_01 des sites et de la présence de circuit de niveau 01 (facile)
            (SELECT a.gid, COUNT(DISTINCT b.niveau) AS presence_01
            -- Deux champs : le gid du site et le décompte du niveau 01 en distinct pour avoir une valeur max de 1 quand il y en a
            FROM atd16_univerttrail.geo_site a -- de la table geo_site
            LEFT JOIN atd16_univerttrail.geo_circuit b ON a.gid = b.id_site -- jointe à la table geo_circuit
            WHERE b.niveau='01' -- où le niveau = '01'
            GROUP BY a.gid) -- regroupé par le gid de geo_site pour avoir au max le nombre de ligne de geo_site.
        ),
        table_02 AS (
            (SELECT a.gid, COUNT(DISTINCT b.niveau) AS presence_02
            FROM atd16_univerttrail.geo_site a
            LEFT JOIN atd16_univerttrail.geo_circuit b ON a.gid = b.id_site
            WHERE b.niveau='02'
            GROUP BY a.gid)
        ),
        table_03 AS (
            (SELECT a.gid, COUNT(DISTINCT b.niveau) AS presence_03
            FROM atd16_univerttrail.geo_site a
            LEFT JOIN atd16_univerttrail.geo_circuit b ON a.gid = b.id_site
            WHERE b.niveau='03'
            GROUP BY a.gid)
        ),
        table_04 AS (
            (SELECT a.gid, COUNT(DISTINCT b.niveau) AS presence_04
            FROM atd16_univerttrail.geo_site a
            LEFT JOIN atd16_univerttrail.geo_circuit b ON a.gid = b.id_site
            WHERE b.niveau='04'
            GROUP BY a.gid)
        )
    SELECT 
        a.gid,
        a.insee,
        a.ident,
        a.nb_circuit,
        a.longueur_iti_totale,
        CASE WHEN z.presence_01 IS NULL THEN 0 ELSE z.presence_01 END,
        -- valeur de la présence de circuit de niveau 01 (facile) à 1 sauf lorsqu'elle est nulle ou elle prend 0 (nécessaire pour effectuer une somme)
        CASE WHEN y.presence_02 IS NULL THEN 0 ELSE y.presence_02 END,
        CASE WHEN x.presence_03 IS NULL THEN 0 ELSE x.presence_03 END,
        CASE WHEN w.presence_04 IS NULL THEN 0 ELSE w.presence_04 END,
        (CASE WHEN z.presence_01 IS NULL THEN 0 ELSE z.presence_01 END +
        CASE WHEN y.presence_02 IS NULL THEN 0 ELSE y.presence_02 END +
        CASE WHEN x.presence_03 IS NULL THEN 0 ELSE x.presence_03 END +
        CASE WHEN w.presence_04 IS NULL THEN 0 ELSE w.presence_04 END) AS presence_niveau,
        -- Somme de toutes les présences pour avoir une valeur comprise entre 0 et 4
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_site a
    LEFT JOIN table_01 z ON a.gid = z.gid -- Jointure avec la table temporaire table_01
    LEFT JOIN table_02 y ON a.gid = y.gid
    LEFT JOIN table_03 x ON a.gid = x.gid
    LEFT JOIN table_04 w ON a.gid = w.gid
    WHERE (CASE WHEN z.presence_01 IS NULL THEN 0 ELSE z.presence_01 END +
        CASE WHEN y.presence_02 IS NULL THEN 0 ELSE y.presence_02 END +
        CASE WHEN x.presence_03 IS NULL THEN 0 ELSE x.presence_03 END +
        CASE WHEN w.presence_04 IS NULL THEN 0 ELSE w.presence_04 END) < 3
        -- Où la somme de toutes les présences est inférieure à 3
    GROUP BY a.gid, z.presence_01, y.presence_02, x.presence_03, w.presence_04;

ALTER TABLE atd16_univerttrail.v_geo_site_alert_circuit_non_heterogene OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_site_alert_circuit_non_heterogene IS 
    '[ATD16] Vue de la couche geo_site ayant moins de trois circuits de niveaux différents';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_site;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.geo_site
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_site IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_site;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.geo_site
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_site IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

