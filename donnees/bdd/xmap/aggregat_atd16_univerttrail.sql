-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/06 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                          SCHEMA : Gestion des trails (certification UniVert'Trail)                                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP SCHEMA IF EXISTS atd16_univerttrail;

CREATE SCHEMA atd16_univerttrail;
ALTER SCHEMA atd16_univerttrail OWNER TO sditecgrp;
COMMENT ON SCHEMA atd16_univerttrail IS 'Certification UniVert''Trail';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/06 : SL / Création du fichier sur Git
-- 2021/08/04 : SL / Ajout de la fonction f_maj_longueur_sig()

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Fonction(s) trigger(s) générique(s)                                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_univerttrail.f_date_creation();

CREATE FUNCTION atd16_univerttrail.f_date_creation()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_creation = now(); -- À la création d'un objet, le champ date_creation prend la valeur de la date du jour
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_univerttrail.f_date_creation() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_date_creation() IS 'Fonction trigger permettant l''initialisation du champ date_creation';


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_univerttrail.f_date_maj();

CREATE FUNCTION atd16_univerttrail.f_date_maj()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_maj = now(); -- À la modification d'un objet, le champ date_maj prend la valeur de la date du jour
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_univerttrail.f_date_maj() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_date_maj() IS 'Fonction trigger permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                               Mise à jour de la longueur (champ longueur_sig)                                              ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_longueur_sig();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_longueur_sig()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    NEW.longueur_sig := ST_Length(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154))*0.001; 
    -- Calcul de la longueur de la nouvelle géométrie (reprojection car en 3857 la longueur est à peu près 50% plus grande)
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_longueur_sig() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_longueur_sig() IS '[ATD16] Mise à jour de la longueur';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                     Table non géographique : Domaine de valeur de la labellisation                                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_labellisation;

CREATE TABLE atd16_univerttrail.lst_labellisation 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de la labellisation
    libelle varchar(254), --[UVT] Libellé de la labellisation
    CONSTRAINT pk_lst_labellisation PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_labellisation OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_labellisation TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_labellisation IS '[UVT] Intitulé de la labellisation Uni''vert trail';

COMMENT ON COLUMN atd16_univerttrail.lst_labellisation.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_labellisation.code IS '[ATD16] Code de la labellisation';
COMMENT ON COLUMN atd16_univerttrail.lst_labellisation.libelle IS '[UVT] Libellé de la labellisation';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_labellisation 
    (code, libelle)
VALUES
    ('01', 'Uni''vert trail 1 Argent'),
    ('02', 'Uni''vert trail 2 Or'),
    ('03', 'Demande en cours');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                          Table non géographique : Domaine de valeur de la catégorie de la longueur du circuit                              ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_categorie_longueur;

CREATE TABLE atd16_univerttrail.lst_categorie_longueur 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de la catégorie
    libelle varchar(254), --[UVT] Libellé de la catégorie
    note integer, --[UVT] Note de la catégorie
    CONSTRAINT pk_lst_categorie_longueur PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_categorie_longueur OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_categorie_longueur TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_categorie_longueur IS '[UVT] Domaine de valeur de la catégorie de la longueur du circuit';

COMMENT ON COLUMN atd16_univerttrail.lst_categorie_longueur.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_longueur.code IS '[ATD16] Code de la catégorie';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_longueur.libelle IS '[UVT] Libellé de la catégorie';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_longueur.note IS '[UVT] Note de la catégorie';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_categorie_longueur 
    (code, libelle, note)
VALUES
    ('01', '< 10 km', '2'),
    ('02', '10-15 km', '3'),
    ('03', '15-20 km', '4'),
    ('04', '20-25 km', '5'),
    ('05', '25-30 km', '6'),
    ('06', '30-45 km', '10'),
    ('07', '> 45 km', '14');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Modification de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                        Table non géographique : Domaine de valeur de la catégorie du dénivelé positif du circuit                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_categorie_denivele_positif;

CREATE TABLE atd16_univerttrail.lst_categorie_denivele_positif 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de la catégorie
    libelle varchar(254), --[UVT] Libellé de la catégorie
    note integer, --[UVT] Note de la catégorie
    CONSTRAINT pk_lst_categorie_denivele_positif PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_categorie_denivele_positif OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_categorie_denivele_positif TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_categorie_denivele_positif IS '[UVT] Domaine de valeur de la catégorie du dénivelé positif du circuit ';

COMMENT ON COLUMN atd16_univerttrail.lst_categorie_denivele_positif.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_denivele_positif.code IS '[ATD16] Code de la catégorie';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_denivele_positif.libelle IS '[UVT] Libellé de la catégorie';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_denivele_positif.note IS '[UVT] Note de la longueur';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_categorie_denivele_positif 
    (code, libelle, note)
VALUES
    ('00', 'Pas de dénivelé positif renseigné', NULL),
    ('01', '< 500 m', '2'),
    ('02', '500-750 m', '3'),
    ('03', '750-1000 m', '4'),
    ('04', '1000-1500 m', '5'),
    ('05', '1500-2000 m', '6'),
    ('06', '2000-2500 m', '7'),
    ('07', '> 2500 m', '8');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Modification de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                         Table non géographique : Domaine de valeur de la catégorie du dénivelé continu maximum                             ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_categorie_denivele_continu_max;

CREATE TABLE atd16_univerttrail.lst_categorie_denivele_continu_max 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de la catégorie
    libelle varchar(254), --[UVT] Libellé de la catégorie
    note integer, --[UVT] Note de la catégorie
    CONSTRAINT pk_lst_categorie_denivele_continu_max PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_categorie_denivele_continu_max OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_categorie_denivele_continu_max TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_categorie_denivele_continu_max IS '[UVT] Domaine de valeur de la catégorie du dénivelé continu maximum';

COMMENT ON COLUMN atd16_univerttrail.lst_categorie_denivele_continu_max.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_denivele_continu_max.code IS '[ATD16] Code de la catégorie';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_denivele_continu_max.libelle IS '[UVT] Libellé de la catégorie';
COMMENT ON COLUMN atd16_univerttrail.lst_categorie_denivele_continu_max.note IS '[UVT] Note de la catégorie';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_categorie_denivele_continu_max 
    (code, libelle, note)
VALUES
    ('00', 'Pas de dénivelé continu max renseigné', NULL),
    ('01', '< 300 m', '1'),
    ('02', '300-600 m', '2'),
    ('03', '600-1000 m', '3'),
    ('04', '> 1000 m', '4');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                         Table non géographique : Domaine de valeur de la catégorie de la technicité du circuit                             ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_technicite;

CREATE TABLE atd16_univerttrail.lst_technicite 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de la catégorie
    libelle varchar(254), --[UVT] Libellé de la catégorie
    note integer, --[UVT] Note de la catégorie
    CONSTRAINT pk_lst_technicite PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_technicite OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_technicite TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_technicite IS '[UVT] Domaine de valeur de la catégorie de la technicité du circuit';

COMMENT ON COLUMN atd16_univerttrail.lst_technicite.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_technicite.code IS '[ATD16] Code de la catégorie';
COMMENT ON COLUMN atd16_univerttrail.lst_technicite.libelle IS '[UVT] Libellé de la catégorie';
COMMENT ON COLUMN atd16_univerttrail.lst_technicite.note IS '[UVT] Note de la catégorie';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_technicite 
    (code, libelle, note)
VALUES
    ('01', 'Itinéraire sans obstacle notable ou sans réelles difficultés', '1'),
    ('02', 'Itinéraire présentant des portions de sentier étroits ou des obstacles de faible dimension', '2'),
    ('03', 'Itinéraire présentant des difficultés techniques sur des portions relativement courtes et peu nombreuses', '3'),
    ('04', 'Itinéraire présentant des difficultés techniques sur des portions assez longues ou se répétant fréquemment', '4');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                              Table non géographique : Domaine de valeur du niveau de difficulté du circuit                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_niveau;

CREATE TABLE atd16_univerttrail.lst_niveau 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du niveau de difficulté
    libelle varchar(254), --[UVT] Libellé du niveau de difficulté
    cotation varchar(12), --[UVT] Cotation du niveau de difficulté
    CONSTRAINT pk_lst_niveau PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_niveau OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_niveau TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_niveau IS '[UVT] Domaine de valeur du niveau de difficulté du circuit';

COMMENT ON COLUMN atd16_univerttrail.lst_niveau.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_niveau.code IS '[ATD16] Code du niveau de difficulté';
COMMENT ON COLUMN atd16_univerttrail.lst_niveau.libelle IS '[UVT] Libellé du niveau de difficulté';
COMMENT ON COLUMN atd16_univerttrail.lst_niveau.cotation IS '[UVT] Cotation du niveau de difficulté';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_niveau 
    (code, libelle, cotation)
VALUES
    ('01', 'Niveau facile', '5 à 6'),
    ('02', 'Niveau moyen', '6 à 11'),
    ('03', 'Niveau difficile', '10 à 18'),
    ('04', 'Niveau très difficile', '17 à 26');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                               Table non géographique : Domaine de valeur du niveau de risque du circuit                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_risque;

CREATE TABLE atd16_univerttrail.lst_risque 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du niveau de risque
    libelle varchar(254), --[UVT] Libellé du niveau de risque
    CONSTRAINT pk_lst_risque PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_risque OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_risque TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_risque IS '[UVT] Domaine de valeur du niveau de risque du circuit';

COMMENT ON COLUMN atd16_univerttrail.lst_risque.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_risque.code IS '[ATD16] Code du niveau de risque';
COMMENT ON COLUMN atd16_univerttrail.lst_risque.libelle IS '[UVT] Libellé du niveau de risque';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_risque 
    (code, libelle)
VALUES
    ('01', 'Faible'),
    ('02', 'Assez faible'),
    ('03', 'Assez élevé'),
    ('04', 'Élevé');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                       Table non géographique : Domaine de valeur de la présence de passage privé sur le circuit                            ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_passage_prive;

CREATE TABLE atd16_univerttrail.lst_passage_prive 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de la présence de passage privé
    libelle varchar(254), --[UVT] Libellé de la présence de passage privé
    CONSTRAINT pk_lst_passage_prive PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_passage_prive OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_passage_prive TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_passage_prive IS '[UVT] Domaine de valeur de la présence de passage privé du circuit';

COMMENT ON COLUMN atd16_univerttrail.lst_passage_prive.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_passage_prive.code IS '[ATD16] Code de la présence de passage privé';
COMMENT ON COLUMN atd16_univerttrail.lst_passage_prive.libelle IS '[UVT] Libellé de la présence de passage privé';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_passage_prive 
    (code, libelle)
VALUES
    ('01', 'Oui'),
    ('02', 'Non');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                Table non géographique : Domaine de valeur de l'autorisation de passage                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_autorisation_passage;

CREATE TABLE atd16_univerttrail.lst_autorisation_passage 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    libelle varchar(254), --[UVT] Libellé de l'autorisation de passage
    CONSTRAINT pk_lst_autorisation_passage PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_autorisation_passage OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_autorisation_passage TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_autorisation_passage IS '[UVT] Domaine de valeur de l''autorisation de passage';

COMMENT ON COLUMN atd16_univerttrail.lst_autorisation_passage.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_autorisation_passage.libelle IS '[UVT] Libellé de l''autorisation de passage';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_autorisation_passage 
    (libelle)
VALUES
    ('Oui'),
    ('Non');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                    Table non géographique : Domaine de valeur des zones de départ                                          ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_zone_depart;

CREATE TABLE atd16_univerttrail.lst_zone_depart 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de la zone de départ
    libelle varchar(254), --[UVT] Libellé de la zone de départ
    CONSTRAINT pk_lst_zone_depart PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_zone_depart OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_zone_depart TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_zone_depart IS '[UVT] Domaine de valeur des zones de départ';

COMMENT ON COLUMN atd16_univerttrail.lst_zone_depart.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_zone_depart.code IS '[ATD16] Code de la zone de départ';
COMMENT ON COLUMN atd16_univerttrail.lst_zone_depart.libelle IS '[UVT] Libellé de la zone de départ';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_zone_depart 
    (code, libelle)
VALUES
    ('01', 'Porte principale'),
    ('02', 'Porte secondaire');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                   Table non géographique : Domaine de valeur du type de signalétique                                       ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_type_signaletique;

CREATE TABLE atd16_univerttrail.lst_type_signaletique 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du type de signalétique
    libelle varchar(254), --[UVT] Libellé du type de signalétique
    CONSTRAINT pk_lst_type_signaletique PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_type_signaletique OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_type_signaletique TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_type_signaletique IS '[UVT] Domaine de valeur du type de signalétique';

COMMENT ON COLUMN atd16_univerttrail.lst_type_signaletique.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_type_signaletique.code IS '[ATD16] Code du type de signalétique';
COMMENT ON COLUMN atd16_univerttrail.lst_type_signaletique.libelle IS '[UVT] Libellé du type de signalétique';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_type_signaletique 
    (code, libelle)
VALUES
    ('01', 'Panneau'),
    ('02', 'Totem'),
    ('03', 'Balise'),
    ('04', 'Passage dangereux'),
    ('99', 'Autre');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                    Table non géographique : Domaine de valeur du type d'équipement                                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_type_equipement;

CREATE TABLE atd16_univerttrail.lst_type_equipement 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du type d'équipement 
    libelle varchar(254), --[UVT] Libellé du type d'équipement 
    CONSTRAINT pk_lst_type_equipement PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_type_equipement OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_type_equipement TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_type_equipement IS '[UVT] Domaine de valeur du type d''équipement ';

COMMENT ON COLUMN atd16_univerttrail.lst_type_equipement.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_type_equipement.code IS '[ATD16] Code du type d''équipement ';
COMMENT ON COLUMN atd16_univerttrail.lst_type_equipement.libelle IS '[UVT] Libellé du type d''équipement ';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_type_equipement 
    (code, libelle)
VALUES
    ('01', 'Point d''accueil'),
    ('02', 'Vestiaire/Douche'),
    ('03', 'Restauration'),
    ('04', 'Poubelle'),
    ('05', 'Point d''eau'),
    ('99', 'Autre');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/06 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                           Table non géographique : Lien circuit-passage privé                                              ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.ngeo_circuit_passage_prive;

CREATE TABLE atd16_univerttrail.ngeo_circuit_passage_prive
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    id_circuit integer, --[FK][ATD16] Identifiant du circuit
    id_passage_prive integer, --[FK][ATD16] Identifiant du passage privé
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    CONSTRAINT pk_ngeo_circuit_passage_prive PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.ngeo_circuit_passage_prive OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.ngeo_circuit_passage_prive TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.ngeo_circuit_passage_prive IS '[ATD16] Description de la fonctionnalité de la table';

COMMENT ON COLUMN atd16_univerttrail.ngeo_circuit_passage_prive.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.ngeo_circuit_passage_prive.id_circuit IS '[FK][ATD16] Identifiant du circuit';
COMMENT ON COLUMN atd16_univerttrail.ngeo_circuit_passage_prive.id_passage_prive IS '[FK][ATD16] Identifiant du passage privé';
COMMENT ON COLUMN atd16_univerttrail.ngeo_circuit_passage_prive.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.ngeo_circuit_passage_prive.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.ngeo_circuit_passage_prive;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.ngeo_circuit_passage_prive
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.ngeo_circuit_passage_prive IS 
    '[ATD16] Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.ngeo_circuit_passage_prive;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.ngeo_circuit_passage_prive
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.ngeo_circuit_passage_prive IS
    '[ATD16] Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/06 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                         Table non géographique : Lien circuit-passage goudronné                                            ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.ngeo_circuit_passage_goudronne;

CREATE TABLE atd16_univerttrail.ngeo_circuit_passage_goudronne
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    id_circuit integer, --[FK][ATD16] Identifiant du circuit
    id_passage_goudronne integer, --[FK][ATD16] Identifiant du passage goudronné
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    CONSTRAINT pk_ngeo_circuit_passage_goudronne PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.ngeo_circuit_passage_goudronne OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.ngeo_circuit_passage_goudronne TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.ngeo_circuit_passage_goudronne IS '[ATD16] Description de la fonctionnalité de la table';

COMMENT ON COLUMN atd16_univerttrail.ngeo_circuit_passage_goudronne.gid IS 
    '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.ngeo_circuit_passage_goudronne.id_circuit IS '[FK][ATD16] Identifiant du circuit';
COMMENT ON COLUMN atd16_univerttrail.ngeo_circuit_passage_goudronne.id_passage_goudronne IS '[FK][ATD16] Identifiant du passage goudronné';
COMMENT ON COLUMN atd16_univerttrail.ngeo_circuit_passage_goudronne.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.ngeo_circuit_passage_goudronne.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.ngeo_circuit_passage_goudronne;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.ngeo_circuit_passage_goudronne
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.ngeo_circuit_passage_goudronne IS 
    '[ATD16] Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.ngeo_circuit_passage_goudronne;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.ngeo_circuit_passage_goudronne
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.ngeo_circuit_passage_goudronne IS
    '[ATD16] Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/06 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/07/08 : SL / Modification du nom du champ certification en labellisation
-- 2021/10/26 : SL / Ajout de la vue v_geo_site_alert_longueur_iti_totale
--                 . Ajout de la vue v_geo_site_alert_circuit_non_heterogene
--                 . Ajout de la vue v_geo_site_alert_sans_zone
-- 2021/10/27 : SL / Refonte totale de la vue v_geo_site_alert_circuit_non_heterogene
--                 . Refonte totale de la vue v_geo_site_alert_sans_zone
-- 2021/11/09 : SL / Refonte de la vue v_geo_site_alert_sans_zone : on enlève la table des zones d'accueil
--                 . Mise en commentaire de l'ancienne vue v_geo_site_alert_sans_zone_old

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Table géographique : Site                                                        ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.geo_site;

CREATE TABLE atd16_univerttrail.geo_site
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    nb_circuit integer, --[UVT] Nombre de circuit du site (automatique)
    longueur_iti_totale numeric(8,3), --[UVT] Longueur totale des circuits en km (automatique)
    labellisation varchar(2), --[FK][UVT] Labellisation UniVert'Trail (liste déroulante)
    url varchar(2054), --[ATD16] Adresse URL du site internet
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Nom du site
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_site PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.geo_site OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.geo_site TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.geo_site IS '[ATD16] Table géographique contenant les sites UniVert''Trail';

COMMENT ON COLUMN atd16_univerttrail.geo_site.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.geo_site.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_univerttrail.geo_site.nb_circuit IS '[UVT] Nombre de circuit du site (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_site.longueur_iti_totale IS '[UVT] Longueur totale des circuits en km (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_site.labellisation IS '[FK][UVT] Labellisation UniVert''Trail (liste déroulante)';
COMMENT ON COLUMN atd16_univerttrail.geo_site.url IS '[ATD16] Adresse URL du site internet';
COMMENT ON COLUMN atd16_univerttrail.geo_site.ident IS '[SIRAP] Identificaion de l''objet / Nom du site';
COMMENT ON COLUMN atd16_univerttrail.geo_site.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_site.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_site.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_site.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_site.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###          v_geo_site_alert_longueur_iti_totale : vue de la couche geo_site dont le champ longueur_iti_totale est inférieur à 40 km          ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_site_alert_longueur_iti_totale;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_site_alert_longueur_iti_totale 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.nb_circuit,
        a.longueur_iti_totale,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_site a
    WHERE longueur_iti_totale < 40 OR longueur_iti_totale IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_site_alert_longueur_iti_totale OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_site_alert_longueur_iti_totale IS 
    '[ATD16] Vue de la couche geo_site dont le champ longueur_iti_totale est inférieur à 40 km';


-- ##################################################################################################################################################
-- ###   v_geo_site_alert_sans_zone_old : vue de la couche geo_site sans zone de départ et/ou sans zone d'accueil (obsolete sans zone d'accueil)  ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_site_alert_sans_zone_old;
/*
CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_site_alert_sans_zone_old 
    AS 
    WITH -- Permet la création de tables temporaires
        nb_zone_accueil AS ( -- Création de la table nb_zone_accueil qui calcule le nombre de zone d'accueil pour chaque site
            (SELECT a.gid, COUNT(b.gid) AS nb_zone_accueil
            FROM atd16_univerttrail.geo_site a
            LEFT JOIN atd16_univerttrail.geo_zone_accueil b ON a.gid = b.id_site
            GROUP BY a.gid)
        ),
        nb_zone_depart AS (
            (SELECT a.gid, COUNT(c.gid) AS nb_zone_depart
            FROM atd16_univerttrail.geo_site a
            LEFT JOIN atd16_univerttrail.geo_zone_depart c ON a.gid = c.id_site
            GROUP BY a.gid)
        )
    SELECT
        a.gid,
        a.insee,
        a.ident,
        a.nb_circuit,
        a.longueur_iti_totale,
        za.nb_zone_accueil,
        zd.nb_zone_depart,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_site a
    LEFT JOIN nb_zone_accueil za ON a.gid = za.gid
    LEFT JOIN nb_zone_depart zd ON a.gid = zd.gid
    WHERE za.nb_zone_accueil = 0
        OR zd.nb_zone_depart = 0;

ALTER TABLE atd16_univerttrail.v_geo_site_alert_sans_zone_old OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_site_alert_sans_zone_old IS 
    '[ATD16] Vue de la couche geo_site sans zone de départ et/ou sans zone d''accueil';
*/

-- ##################################################################################################################################################
-- ###                            v_geo_site_alert_sans_zone : vue de la couche geo_site sans zone de départ principale                           ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_site_alert_sans_zone;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_site_alert_sans_zone 
    AS 
    WITH -- Permet la création de tables temporaires
        nb_zone_depart AS ( -- Création de la table nb_zone_depart qui calcule le nombre de zone de départ principale pour chaque site
            (SELECT a.gid, COUNT(c.gid) AS nb_zone_depart
            FROM atd16_univerttrail.geo_site a
            LEFT JOIN atd16_univerttrail.geo_zone_depart c ON a.gid = c.id_site
            WHERE c.type = '01'
            GROUP BY a.gid)
        )
    SELECT
        a.gid,
        a.insee,
        a.ident,
        a.nb_circuit,
        a.longueur_iti_totale,
        CASE WHEN zd.nb_zone_depart IS NULL THEN 0 ELSE zd.nb_zone_depart END,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_site a
    LEFT JOIN nb_zone_depart zd ON a.gid = zd.gid
    WHERE (CASE WHEN zd.nb_zone_depart IS NULL THEN 0 ELSE zd.nb_zone_depart END) = 0;

ALTER TABLE atd16_univerttrail.v_geo_site_alert_sans_zone OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_site_alert_sans_zone IS 
    '[ATD16] Vue de la couche geo_site sans zone de départ principale';


-- ##################################################################################################################################################
-- ###          v_geo_site_alert_circuit_non_heterogene : vue de la couche geo_site ayant moins de trois circuits de niveaux différents           ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_site_alert_circuit_non_heterogene;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_site_alert_circuit_non_heterogene
    AS
    WITH -- Permet de créer des tables temporaires
        table_01 AS ( -- Création de la table_01 des sites et de la présence de circuit de niveau 01 (facile)
            (SELECT a.gid, COUNT(DISTINCT b.niveau) AS presence_01
            -- Deux champs : le gid du site et le décompte du niveau 01 en distinct pour avoir une valeur max de 1 quand il y en a
            FROM atd16_univerttrail.geo_site a -- de la table geo_site
            LEFT JOIN atd16_univerttrail.geo_circuit b ON a.gid = b.id_site -- jointe à la table geo_circuit
            WHERE b.niveau='01' -- où le niveau = '01'
            GROUP BY a.gid) -- regroupé par le gid de geo_site pour avoir au max le nombre de ligne de geo_site.
        ),
        table_02 AS (
            (SELECT a.gid, COUNT(DISTINCT b.niveau) AS presence_02
            FROM atd16_univerttrail.geo_site a
            LEFT JOIN atd16_univerttrail.geo_circuit b ON a.gid = b.id_site
            WHERE b.niveau='02'
            GROUP BY a.gid)
        ),
        table_03 AS (
            (SELECT a.gid, COUNT(DISTINCT b.niveau) AS presence_03
            FROM atd16_univerttrail.geo_site a
            LEFT JOIN atd16_univerttrail.geo_circuit b ON a.gid = b.id_site
            WHERE b.niveau='03'
            GROUP BY a.gid)
        ),
        table_04 AS (
            (SELECT a.gid, COUNT(DISTINCT b.niveau) AS presence_04
            FROM atd16_univerttrail.geo_site a
            LEFT JOIN atd16_univerttrail.geo_circuit b ON a.gid = b.id_site
            WHERE b.niveau='04'
            GROUP BY a.gid)
        )
    SELECT 
        a.gid,
        a.insee,
        a.ident,
        a.nb_circuit,
        a.longueur_iti_totale,
        CASE WHEN z.presence_01 IS NULL THEN 0 ELSE z.presence_01 END,
        -- valeur de la présence de circuit de niveau 01 (facile) à 1 sauf lorsqu'elle est nulle ou elle prend 0 (nécessaire pour effectuer une somme)
        CASE WHEN y.presence_02 IS NULL THEN 0 ELSE y.presence_02 END,
        CASE WHEN x.presence_03 IS NULL THEN 0 ELSE x.presence_03 END,
        CASE WHEN w.presence_04 IS NULL THEN 0 ELSE w.presence_04 END,
        (CASE WHEN z.presence_01 IS NULL THEN 0 ELSE z.presence_01 END +
        CASE WHEN y.presence_02 IS NULL THEN 0 ELSE y.presence_02 END +
        CASE WHEN x.presence_03 IS NULL THEN 0 ELSE x.presence_03 END +
        CASE WHEN w.presence_04 IS NULL THEN 0 ELSE w.presence_04 END) AS presence_niveau,
        -- Somme de toutes les présences pour avoir une valeur comprise entre 0 et 4
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_site a
    LEFT JOIN table_01 z ON a.gid = z.gid -- Jointure avec la table temporaire table_01
    LEFT JOIN table_02 y ON a.gid = y.gid
    LEFT JOIN table_03 x ON a.gid = x.gid
    LEFT JOIN table_04 w ON a.gid = w.gid
    WHERE (CASE WHEN z.presence_01 IS NULL THEN 0 ELSE z.presence_01 END +
        CASE WHEN y.presence_02 IS NULL THEN 0 ELSE y.presence_02 END +
        CASE WHEN x.presence_03 IS NULL THEN 0 ELSE x.presence_03 END +
        CASE WHEN w.presence_04 IS NULL THEN 0 ELSE w.presence_04 END) < 3
        -- Où la somme de toutes les présences est inférieure à 3
    GROUP BY a.gid, z.presence_01, y.presence_02, x.presence_03, w.presence_04;

ALTER TABLE atd16_univerttrail.v_geo_site_alert_circuit_non_heterogene OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_site_alert_circuit_non_heterogene IS 
    '[ATD16] Vue de la couche geo_site ayant moins de trois circuits de niveaux différents';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_site;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.geo_site
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_site IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_site;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.geo_site
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_site IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/07 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/10/26 : SL / Ajout de la vue v_geo_zone_accueil_alert_incomplete
-- 2021/10/28 : SL / Ajout de la vue v_geo_zone_accueil_sans_site
-- 2021/11/09 : SL / Table obsolète (zone d'accueil = zone de départ principale) : mise en commentaire

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                      Table géographique : Zone d'accueil                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.geo_zone_accueil;
/*
CREATE TABLE atd16_univerttrail.geo_zone_accueil
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    id_site integer, --[FK][UVT] Identifiant du site
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Nom de la zone
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_zone_accueil PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.geo_zone_accueil OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.geo_zone_accueil TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.geo_zone_accueil IS '[ATD16] Table géographique contenant les zones d''accueil';

COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.id_site IS '[FK][UVT] Identifiant du site';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.ident IS '[SIRAP] Identificaion de l''objet / Nom de la zone';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ### v_geo_zone_accueil_alert_incomplete : vue de la couche geo_zone_accueil sans panneau d'info, de parking, de point d'eau et/ou de poubelles ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_zone_accueil_alert_incomplete;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_zone_accueil_alert_incomplete 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        (SELECT COUNT(y.gid) -- On compte le nombre d'objet
            FROM atd16_univerttrail.geo_signaletique y, atd16_univerttrail.geo_zone_accueil z -- de la table geo_signaletique
            WHERE ST_Intersects(y.the_geom, z.the_geom) AND y.type_signaletique = '01') AS nb_panneau_info,
            -- qui intersecte la zone d'accueil et dont le type = '01'
        (SELECT COUNT(x.gid)
            FROM atd16_univerttrail.geo_parking x, atd16_univerttrail.geo_zone_accueil z 
            WHERE ST_Intersects(x.the_geom, z.the_geom)) AS nb_parking,
        (SELECT COUNT(w.gid)
            FROM atd16_univerttrail.geo_equipement w, atd16_univerttrail.geo_zone_accueil z 
            WHERE ST_Intersects(w.the_geom, z.the_geom) AND w.type_equipement = '05') AS nb_point_d_eau,
        (SELECT COUNT(v.gid)
            FROM atd16_univerttrail.geo_equipement v, atd16_univerttrail.geo_zone_accueil z 
            WHERE ST_Intersects(v.the_geom, z.the_geom) AND v.type_equipement = '04') AS nb_poubelles,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_zone_accueil a
    WHERE 
        (SELECT COUNT(y.gid)
        FROM atd16_univerttrail.geo_signaletique y, atd16_univerttrail.geo_zone_accueil z
        WHERE ST_Intersects(y.the_geom, z.the_geom) AND y.type_signaletique = '01') = 0
    OR
        (SELECT COUNT(x.gid)
        FROM atd16_univerttrail.geo_parking x, atd16_univerttrail.geo_zone_accueil z 
        WHERE ST_Intersects(x.the_geom, z.the_geom)) = 0
    OR
        (SELECT COUNT(w.gid)
        FROM atd16_univerttrail.geo_equipement w, atd16_univerttrail.geo_zone_accueil z 
        WHERE ST_Intersects(w.the_geom, z.the_geom) AND w.type_equipement = '05') = 0
    OR
        (SELECT COUNT(v.gid)
        FROM atd16_univerttrail.geo_equipement v, atd16_univerttrail.geo_zone_accueil z 
        WHERE ST_Intersects(v.the_geom, z.the_geom) AND v.type_equipement = '04') = 0; 

ALTER TABLE atd16_univerttrail.v_geo_zone_accueil_alert_incomplete OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_zone_accueil_alert_incomplete IS 
    '[ATD16] Vue de la couche geo_zone_accueil sans panneau d''info, de parking, de point d''eau et/ou de poubelles';


-- ##################################################################################################################################################
-- ###                     v_geo_zone_accueil_sans_site : vue de la couche geo_zone_accueil sans lien avec la couche geo_site                     ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_zone_accueil_sans_site;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_zone_accueil_sans_site 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_zone_accueil a
    WHERE id_site IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_zone_accueil_sans_site OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_zone_accueil_sans_site IS 
    '[ATD16] Vue de la couche geo_zone_accueil sans lien avec la couche geo_site';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_zone_accueil;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.geo_zone_accueil
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_zone_accueil IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_zone_accueil;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.geo_zone_accueil
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_zone_accueil IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';
*/

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/07 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/10/27 : SL / Ajout de la vue v_geo_zone_depart_01_alert_incomplete
--                 . Ajout de la vue v_geo_zone_depart_02_alert_incomplete
-- 2021/10/28 : SL / Ajout de la vue v_geo_zone_depart_sans_site
--                 . Ajout de la vue v_geo_zone_depart_portes_principales_multiples
-- 2021/11/10 : SL / Refonte totale de la vue v_geo_zone_depart_01_alert_incomplete
--                 . Refonte totale de la vue v_geo_zone_depart_02_alert_incomplete

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                      Table géographique : Zone de départ                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.geo_zone_depart;

CREATE TABLE atd16_univerttrail.geo_zone_depart
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    id_site integer, --[FK][UVT] Identifiant du site
    type varchar(2), --[FK][UVT] Type de zone de départ
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Nom de la zone
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_zone_depart PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.geo_zone_depart OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.geo_zone_depart TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.geo_zone_depart IS '[ATD16] Table géographique contenant les zones de départ';

COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.id_site IS '[FK][UVT] Identifiant du site';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.type IS '[FK][UVT] Type de zone de départ';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.ident IS '[SIRAP] Identificaion de l''objet / Nom de la zone';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_depart.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###      v_geo_zone_depart_01_alert_incomplete : vue de geo_zone_depart sans panneau d'info, de parking, de point d'eau et/ou de poubelles     ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_zone_depart_01_alert_incomplete;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_zone_depart_01_alert_incomplete 
    AS 
    WITH -- Permet la création de tables temporaires
        tb_nb_panneau_info AS ( -- Création de la table tb_nb_panneau_info qui calcule le nombre de panneau d'info pour chaque zone de départ p.
            (SELECT z.gid, COUNT(y.gid) AS nb_panneau_info -- On compte le nombre d'objet
            FROM atd16_univerttrail.geo_signaletique y, atd16_univerttrail.geo_zone_depart z -- de la table geo_signaletique
            WHERE ST_Intersects(y.the_geom, z.the_geom) AND y.type_signaletique = '01' AND z.type = '01'
            GROUP BY z.gid)
        ),
        tb_nb_parking AS (
            (SELECT z.gid, COUNT(x.gid) AS nb_parking
            FROM atd16_univerttrail.geo_parking x, atd16_univerttrail.geo_zone_depart z
            WHERE ST_Intersects(x.the_geom, z.the_geom) AND z.type = '01'
            GROUP BY z.gid)
        ),
        tb_nb_point_d_eau AS (
            (SELECT z.gid, COUNT(w.gid) AS nb_point_d_eau
            FROM atd16_univerttrail.geo_equipement w, atd16_univerttrail.geo_zone_depart z
            WHERE ST_Intersects(w.the_geom, z.the_geom) AND w.type_equipement = '05' AND z.type = '01'
            GROUP BY z.gid)
        ),
        tb_nb_poubelles AS (
            (SELECT z.gid, COUNT(v.gid) AS nb_poubelles
            FROM atd16_univerttrail.geo_equipement v, atd16_univerttrail.geo_zone_depart z
            WHERE ST_Intersects(v.the_geom, z.the_geom) AND v.type_equipement = '04' AND z.type = '01'
            GROUP BY z.gid)
        )
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        CASE WHEN npi.nb_panneau_info IS NULL THEN 0 ELSE npi.nb_panneau_info END,
        CASE WHEN npk.nb_parking IS NULL THEN 0 ELSE npk.nb_parking END,
        CASE WHEN npe.nb_point_d_eau IS NULL THEN 0 ELSE npe.nb_point_d_eau END,
        CASE WHEN npb.nb_poubelles IS NULL THEN 0 ELSE npb.nb_poubelles END,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_zone_depart a
    LEFT JOIN tb_nb_panneau_info npi ON a.gid = npi.gid
    LEFT JOIN tb_nb_parking npk ON a.gid = npk.gid
    LEFT JOIN tb_nb_point_d_eau npe ON a.gid = npe.gid
    LEFT JOIN tb_nb_poubelles npb ON a.gid = npb.gid
    WHERE 
    (
        (CASE WHEN npi.nb_panneau_info IS NULL THEN 0 ELSE npi.nb_panneau_info END) = 0
    OR
        (CASE WHEN npk.nb_parking IS NULL THEN 0 ELSE npk.nb_parking END) = 0
    OR
        (CASE WHEN npe.nb_point_d_eau IS NULL THEN 0 ELSE npe.nb_point_d_eau END) = 0
    OR
        (CASE WHEN npb.nb_poubelles IS NULL THEN 0 ELSE npb.nb_poubelles END) = 0
    )
    AND
        a.type = '01';

ALTER TABLE atd16_univerttrail.v_geo_zone_depart_01_alert_incomplete OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_zone_depart_01_alert_incomplete IS 
    '[ATD16] Vue de la couche geo_zone_depart sans panneau d''info, de parking, de point d''eau et/ou de poubelles';


-- ##################################################################################################################################################
-- ###               v_geo_zone_depart_02_alert_incomplete : vue de geo_zone_depart sans panneau d'info ou totem et/ou sans parking               ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_zone_depart_02_alert_incomplete;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_zone_depart_02_alert_incomplete 
    AS 
    WITH -- Permet la création de tables temporaires
        tb_nb_panneau_info AS ( -- Création de la table tb_nb_panneau_info qui calcule le nombre de panneau d'info pour chaque zone de départ p.
            (SELECT z.gid, COUNT(y.gid) AS nb_panneau_info -- On compte le nombre d'objet
            FROM atd16_univerttrail.geo_signaletique y, atd16_univerttrail.geo_zone_depart z -- de la table geo_signaletique
            WHERE ST_Intersects(y.the_geom, z.the_geom) AND y.type_signaletique = '01' AND z.type = '02'
            -- qui intersecte la zone de départ, dont le type de signalétique = '01' et dont le type de zone = '02'
            GROUP BY z.gid)
        ),
        tb_nb_totem AS (
            (SELECT z.gid, COUNT(w.gid) AS nb_totem
            FROM atd16_univerttrail.geo_signaletique w, atd16_univerttrail.geo_zone_depart z
            WHERE ST_Intersects(w.the_geom, z.the_geom) AND w.type_signaletique = '02' AND z.type = '02'
            GROUP BY z.gid)
        ),
        tb_nb_parking AS (
            (SELECT z.gid, COUNT(x.gid) AS nb_parking
            FROM atd16_univerttrail.geo_parking x, atd16_univerttrail.geo_zone_depart z
            WHERE ST_Intersects(x.the_geom, z.the_geom) AND z.type = '02'
            GROUP BY z.gid)
        )
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        (CASE WHEN npi.nb_panneau_info IS NULL THEN 0 ELSE npi.nb_panneau_info END
        +
        CASE WHEN nt.nb_totem IS NULL THEN 0 ELSE nt.nb_totem END) AS nb_panneau_et_totem,
        CASE WHEN npk.nb_parking IS NULL THEN 0 ELSE npk.nb_parking END,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_zone_depart a
    LEFT JOIN tb_nb_panneau_info npi ON a.gid = npi.gid
    LEFT JOIN tb_nb_totem nt ON a.gid = nt.gid
    LEFT JOIN tb_nb_parking npk ON a.gid = npk.gid
    WHERE 
    (
        (CASE WHEN npi.nb_panneau_info IS NULL THEN 0 ELSE npi.nb_panneau_info END
        +
        CASE WHEN nt.nb_totem IS NULL THEN 0 ELSE nt.nb_totem END) = 0
    OR
        (CASE WHEN npk.nb_parking IS NULL THEN 0 ELSE npk.nb_parking END) = 0
    )
    AND
        a.type = '02';

ALTER TABLE atd16_univerttrail.v_geo_zone_depart_02_alert_incomplete OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_zone_depart_02_alert_incomplete IS 
    '[ATD16] Vue de la couche geo_zone_depart sans panneau d''info ou totem et/ou sans parking';


-- ##################################################################################################################################################
-- ###                      v_geo_zone_depart_sans_site : vue de la couche geo_zone_depart sans lien avec la couche geo_site                      ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_zone_depart_sans_site;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_zone_depart_sans_site 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        a.type,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_zone_depart a
    WHERE id_site IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_zone_depart_sans_site OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_zone_depart_sans_site IS 
    '[ATD16] Vue de la couche geo_zone_depart sans lien avec la couche geo_site';


-- ##################################################################################################################################################
-- ###    v_geo_zone_depart_portes_principales_multiples : vue de geo_zone_depart qui comporte plusieurs portes principales pour un seul site     ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_zone_depart_portes_principales_multiples;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_zone_depart_portes_principales_multiples 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        a.type,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_zone_depart a
    WHERE EXISTS ( -- EXISTS vérifie si la sous-requête retourne un résultat ce qui permet à la requête de s'exécuter
        SELECT 
            b.gid,
            b.insee,
            b.id_site,
            b.ident,
            b.type,
            b.date_creation,
            b.date_maj,
            b.datesig,
            b.origdata,
            b.the_geom
        FROM atd16_univerttrail.geo_zone_depart b -- Même table que la requête
        WHERE a.gid <> b.gid -- Et on les compare entre elles (le gid doit être différent) 
        AND a.id_site = b.id_site -- Le champ id_site similaire
        AND a.type = b.type -- Le champ type similaire aussi
        AND type = '01' -- Et il doit avoir la valeur à 01
    );

ALTER TABLE atd16_univerttrail.v_geo_zone_depart_portes_principales_multiples OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_zone_depart_portes_principales_multiples IS 
    '[ATD16] Vue de la couche geo_zone_depart qui comporte plusieurs portes principales pour un seul site';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_zone_depart;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.geo_zone_depart
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_zone_depart IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_zone_depart;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.geo_zone_depart
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_zone_depart IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/07 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/08/05 : SL / Ajout du trigger t_before_iu_maj_longueur_sig
-- 2021/10/26 : SL / Ajout de la vue v_geo_zone_echauffement_alert_longueur
-- 2021/10/28 : SL / Ajout de la vue v_geo_zone_echauffement_sans_site

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                   Table géographique : Zone d'échauffement                                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.geo_zone_echauffement;

CREATE TABLE atd16_univerttrail.geo_zone_echauffement
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    id_site integer, --[FK][UVT] Identifiant du site
    longueur_sig numeric(5,3), --[UVT] Longueur du tronçon (automatique)
    longueur numeric(5,3), --[UVT] Longueur du tronçon
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Nom de la zone
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_zone_echauffement PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.geo_zone_echauffement OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.geo_zone_echauffement TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.geo_zone_echauffement IS '[ATD16] Table géographique contenant les zones d''échauffement';

COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.id_site IS '[FK][UVT] Identifiant du site';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.longueur_sig IS '[UVT] Longueur du tronçon (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.longueur IS '[UVT] Longueur du tronçon';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.ident IS '[SIRAP] Identificaion de l''objet / Nom de la zone';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_echauffement.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###   v_geo_zone_echauffement_alert_longueur : vue de la couche geo_zone_echauffement dont la longueur n'est pas comprise entre 400 et 600 m   ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_zone_echauffement_alert_longueur;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_zone_echauffement_alert_longueur 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.longueur,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_zone_echauffement a
    WHERE longueur > 0.6 OR longueur < 0.4;

ALTER TABLE atd16_univerttrail.v_geo_zone_echauffement_alert_longueur OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_zone_echauffement_alert_longueur IS 
    '[ATD16] Vue de la couche geo_zone_echauffement dont la longueur n''est pas comprise entre 400 et 600 m';


-- ##################################################################################################################################################
-- ###                v_geo_zone_echauffement_sans_site : vue de la couche geo_zone_echauffement sans lien avec la couche geo_site                ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_zone_echauffement_sans_site;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_zone_echauffement_sans_site 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_zone_echauffement a
    WHERE id_site IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_zone_echauffement_sans_site OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_zone_echauffement_sans_site IS 
    '[ATD16] Vue de la couche geo_zone_echauffement sans lien avec la couche geo_site';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_zone_echauffement;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.geo_zone_echauffement
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_zone_echauffement IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_zone_echauffement;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.geo_zone_echauffement
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_zone_echauffement IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                         Mise à jour de la longueur du circuit (champ longueur_sig)                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER atd16_univerttrail.t_before_iu_maj_longueur_sig() ON atd16_univerttrail.geo_zone_echauffement;

CREATE TRIGGER t_before_iu_maj_longueur_sig
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_zone_echauffement 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_longueur_sig();
    
COMMENT ON TRIGGER t_before_iu_maj_longueur_sig ON atd16_univerttrail.geo_zone_echauffement IS 
    '[ATD16] Mise à jour de la longueur du circuit';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/07 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/10/28 : SL / Ajout de la vue v_geo_signaletique_sans_site

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                       Table géographique : Signalétique                                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.geo_signaletique;

CREATE TABLE atd16_univerttrail.geo_signaletique
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    id_site integer, --[FK][UVT] Identifiant du site
    type_signaletique varchar(2), --[FK][UVT] Type de signalétique (liste)
    ident varchar(80), --[SIRAP] Identificaion de l'objet
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_signaletique PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.geo_signaletique OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.geo_signaletique TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.geo_signaletique IS '[ATD16] Table géographique contenant les ponctuels de signalétique';

COMMENT ON COLUMN atd16_univerttrail.geo_signaletique.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.geo_signaletique.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_univerttrail.geo_signaletique.id_site IS '[FK][UVT] Identifiant du site';
COMMENT ON COLUMN atd16_univerttrail.geo_signaletique.type_signaletique IS '[FK][UVT] Type de signalétique (liste)';
COMMENT ON COLUMN atd16_univerttrail.geo_signaletique.ident IS '[SIRAP] Identificaion de l''objet';
COMMENT ON COLUMN atd16_univerttrail.geo_signaletique.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_signaletique.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_signaletique.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_signaletique.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_signaletique.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                     v_geo_signaletique_sans_site : vue de la couche geo_signaletique sans lien avec la couche geo_site                     ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_signaletique_sans_site;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_signaletique_sans_site 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        a.type_signaletique,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_signaletique a
    WHERE id_site IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_signaletique_sans_site OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_signaletique_sans_site IS 
    '[ATD16] Vue de la couche geo_signaletique sans lien avec la couche geo_site';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_signaletique;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.geo_signaletique
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_signaletique IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_signaletique;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.geo_signaletique
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_signaletique IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/07 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/10/28 : SL / Ajout de la vue v_geo_equipement_sans_site

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                        Table géographique : Équipement                                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.geo_equipement;

CREATE TABLE atd16_univerttrail.geo_equipement
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    id_site integer, --[FK][UVT] Identifiant du site
    type_equipement varchar(2), --[FK][UVT] Type d'équipement
    ident varchar(80), --[SIRAP] Identificaion de l'objet
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_equipement PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.geo_equipement OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.geo_equipement TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.geo_equipement IS '[ATD16] Table géographique contenant les équipements';

COMMENT ON COLUMN atd16_univerttrail.geo_equipement.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.geo_equipement.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_univerttrail.geo_equipement.id_site IS '[FK][UVT] Identifiant du site';
COMMENT ON COLUMN atd16_univerttrail.geo_equipement.type_equipement IS '[FK][UVT] Type d''équipement';
COMMENT ON COLUMN atd16_univerttrail.geo_equipement.ident IS '[SIRAP] Identificaion de l''objet';
COMMENT ON COLUMN atd16_univerttrail.geo_equipement.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_equipement.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_equipement.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_equipement.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_equipement.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                       v_geo_equipement_sans_site : vue de la couche geo_equipement sans lien avec la couche geo_site                       ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_equipement_sans_site;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_equipement_sans_site 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        a.type_equipement,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_equipement a
    WHERE id_site IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_equipement_sans_site OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_equipement_sans_site IS 
    '[ATD16] Vue de la couche geo_equipement sans lien avec la couche geo_site';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_equipement;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.geo_equipement
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_equipement IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_equipement;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.geo_equipement
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_equipement IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/07 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/10/28 : SL / Ajout de la vue v_geo_parking_sans_site

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                         Table géographique : Parking                                                       ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.geo_parking;

CREATE TABLE atd16_univerttrail.geo_parking
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    id_site integer, --[FK][UVT] Identifiant du site
    nb_places integer, --[UVT] Nombre de places
    ident varchar(80), --[SIRAP] Identificaion de l'objet
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_parking PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.geo_parking OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.geo_parking TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.geo_parking IS '[ATD16] Table géographique contenant les parking';

COMMENT ON COLUMN atd16_univerttrail.geo_parking.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.geo_parking.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_univerttrail.geo_parking.id_site IS '[FK][UVT] Identifiant du site';
COMMENT ON COLUMN atd16_univerttrail.geo_parking.nb_places IS '[UVT] Nombre de places';
COMMENT ON COLUMN atd16_univerttrail.geo_parking.ident IS '[SIRAP] Identificaion de l''objet';
COMMENT ON COLUMN atd16_univerttrail.geo_parking.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_parking.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_parking.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_parking.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_parking.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                          v_geo_parking_sans_site : vue de la couche geo_parking sans lien avec la couche geo_site                          ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_parking_sans_site;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_parking_sans_site 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_parking a
    WHERE id_site IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_parking_sans_site OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_parking_sans_site IS 
    '[ATD16] Vue de la couche geo_parking sans lien avec la couche geo_site';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_parking;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.geo_parking
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_parking IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_parking;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.geo_parking
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_parking IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/07 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
--                 . Ajout de vue v_geo_circuit
-- 2021/07/08 : SL / Ajout de vue v_geo_circuit_1
--                 . Ajout de vue v_geo_circuit_2
--                 . Ajout de vue v_geo_circuit_3
--                 . Ajout de vue v_geo_circuit_4
--                 . Ajout de vue v_geo_circuit_5
-- 2021/08/04 : SL / Ajout du trigger t_before_iu_maj_longueur_sig
-- 2021/10/26 : SL / Ajout de vue v_geo_circuit_alert_passage_goudronne
-- 2021/10/26 : SL / Ajout de vue v_geo_circuit_sans_site
-- 2021/11/12 : SL / Ajout de vue v_geo_circuit_sans_niveau
--                 . Ajout de vue v_geo_circuit_avec_niveau_incoherent

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                              Table géographique : Circuit de trail (itinéraire)                                            ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.geo_circuit;

CREATE TABLE atd16_univerttrail.geo_circuit
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    id_site integer, --[FK][UVT] Identifiant du site
    numero varchar(20), --[ATD16] Numéro du circuit
    longueur_sig numeric(6,3), --[UVT] Longueur du circuit en km (automatique)
    longueur_gps numeric(6,3), --[UVT] Longueur du circuit en km
    categorie_longueur varchar(2), --[FK][UVT] Catégorie associée à la longueur (liste) (automatique)
    denivele_positif integer, --[UVT] Dénivelé postif en m
    categorie_denivele_positif varchar(2), --[FK][UVT] Catégorie associée au dénivelé positif (liste) (automatique)
    denivele_continu_max integer, --[UVT] Dénivelé continu max en m (positif ou négatif)
    categorie_denivele_continu_max varchar(2), --[FK][UVT] Catégorie associée au dénivelé continu max (liste) (automatique)
    technicite varchar(2), --[FK][UVT] Technicité du circuit (liste)
    cotation integer, --[UVT] Cotation du circuit : addition des notes de longueur du circuit, de dénivelé positif, de dénivelé continu max et de technicité (automatique)
    niveau varchar(2), --[FK][UVT] Niveau de difficulté (liste)
    risque varchar(2), --[FK][UVT] Niveau de risque (liste)
    longueur_goudronne numeric(5,3), --[UVT] Longueur totale des portions goudronnées en km (automatique)
    proportion_goudronne numeric(5,2), --[UVT] Proportion de portions goudronnées sur la longueur totale du circuit (automatique)
    passage_prive varchar(2), --[FK][UVT] Présence de passage privé sur le tracé du circuit (liste)
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Nom du circuit
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_circuit PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.geo_circuit OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.geo_circuit TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.geo_circuit IS '[ATD16] Table géographique contenant les circuits trail pour la certification UniVert''Trail';

COMMENT ON COLUMN atd16_univerttrail.geo_circuit.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.id_site IS '[FK][UVT] Identifiant du site';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.numero IS '[ATD16] Numéro du circuit';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.longueur_sig IS '[UVT] Longueur du circuit en km (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.longueur_gps IS '[UVT] Longueur du circuit en km';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.categorie_longueur IS '[FK][UVT] Catégorie associée à la longueur (liste) (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.denivele_positif IS '[UVT] Dénivelé postif en m';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.categorie_denivele_positif IS 
    '[FK][UVT] Catégorie associée au dénivelé positif (liste) (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.denivele_continu_max IS '[UVT] Dénivelé continu max en m (positif ou négatif)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.categorie_denivele_continu_max IS 
    '[FK][UVT] Catégorie associée au dénivelé continu max (liste) (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.technicite IS '[FK][UVT] Technicité du circuit (liste)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.cotation IS 
    '[UVT] Cotation du circuit : addition des notes de longueur du circuit, de dénivelé positif, de dénivelé continu max et de technicité (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.niveau IS '[FK][UVT] Niveau de difficulté (liste)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.risque IS '[FK][UVT] Niveau de risque (liste)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.longueur_goudronne IS '[UVT] Longueur totale des portions goudronnées en km (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.proportion_goudronne IS 
    '[UVT] Proportion de portions goudronnées sur la longueur totale du circuit (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.passage_prive IS '[FK][UVT] Présence de passage privé sur le tracé du circuit (liste)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.ident IS '[SIRAP] Identificaion de l''objet / Nom du circuit';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###           v_geo_circuit : vue de la couche geo_circuit affichant en plus les champs notes (en production à la place de la table)           ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text;

ALTER TABLE atd16_univerttrail.v_geo_circuit OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit IS 
    '[ATD16] Vue de la couche geo_circuit affichant en plus les champs notes (en production à la place de la table)';


-- ##################################################################################################################################################
-- ###                             v_geo_circuit_1 : vue de la couche geo_circuit affichant uniquement le circuit n°1                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_1;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_1 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '1';

ALTER TABLE atd16_univerttrail.v_geo_circuit_1 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_1 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°1';


-- ##################################################################################################################################################
-- ###                             v_geo_circuit_2 : vue de la couche geo_circuit affichant uniquement le circuit n°2                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_2;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_2 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '2';

ALTER TABLE atd16_univerttrail.v_geo_circuit_2 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_2 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°2';


-- ##################################################################################################################################################
-- ###                             v_geo_circuit_3 : vue de la couche geo_circuit affichant uniquement le circuit n°3                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_3;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_3 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '3';

ALTER TABLE atd16_univerttrail.v_geo_circuit_3 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_3 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°3';


-- ##################################################################################################################################################
-- ###                             v_geo_circuit_4 : vue de la couche geo_circuit affichant uniquement le circuit n°4                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_4;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_4 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '4';

ALTER TABLE atd16_univerttrail.v_geo_circuit_4 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_4 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°4';


-- ##################################################################################################################################################
-- ###                             v_geo_circuit_5 : vue de la couche geo_circuit affichant uniquement le circuit n°5                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_5;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_5 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '5';

ALTER TABLE atd16_univerttrail.v_geo_circuit_5 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_5 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°5';


-- ##################################################################################################################################################
-- ###  v_geo_circuit_alert_passage_goudronne : vue de la couche geo_circuit affichant uniquement ceux qui ont plus de 20% de passages goudronnés ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_alert_passage_goudronne;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_alert_passage_goudronne 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_gps,
        a.denivele_positif,
        a.denivele_continu_max,
        a.technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    WHERE proportion_goudronne > 20;

ALTER TABLE atd16_univerttrail.v_geo_circuit_alert_passage_goudronne OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_alert_passage_goudronne IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement ceux qui ont plus de 20% de passages goudronnés';


-- ##################################################################################################################################################
-- ###                          v_geo_circuit_sans_site : vue de la couche geo_circuit sans lien avec la couche geo_site                          ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_sans_site;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_sans_site 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    WHERE id_site IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_circuit_sans_site OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_sans_site IS 
    '[ATD16] Vue de la couche geo_circuit sans lien avec la couche geo_site';


-- ##################################################################################################################################################
-- ###                         v_geo_circuit_sans_niveau : vue de la couche geo_circuit avec le champ niveau non renseigné                        ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_sans_niveau;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_sans_niveau 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.ident,
        a.cotation,
        a.niveau,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    WHERE niveau IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_circuit_sans_niveau OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_sans_niveau IS 
    '[ATD16] Vue de la couche geo_circuit avec le champ niveau non renseigné';


-- ##################################################################################################################################################
-- ###        v_geo_circuit_avec_niveau_incoherent : vue de la couche geo_circuit avec le champ niveau qui ne correspond pas à la cotation        ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_avec_niveau_incoherent;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_avec_niveau_incoherent 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.ident,
        a.cotation,
        a.niveau,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    WHERE 
        (
            niveau = '01'
            AND
            (cotation < '5' OR cotation > '6')
        )
        OR
        (
            niveau = '02'
            AND
            (cotation < '6' OR cotation > '11')
        )
        OR
        (
            niveau = '03'
            AND
            (cotation < '10' OR cotation > '18')
        )
        OR
        (
            niveau = '04'
            AND
            (cotation < '17' OR cotation > '26')
        );

ALTER TABLE atd16_univerttrail.v_geo_circuit_avec_niveau_incoherent OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_avec_niveau_incoherent IS 
    '[ATD16] Vue de la couche geo_circuit avec le champ niveau qui ne correspond pas à la cotation';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_circuit;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.geo_circuit
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_circuit;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.geo_circuit
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                         Mise à jour de la longueur du circuit (champ longueur_sig)                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER atd16_univerttrail.t_before_iu_maj_longueur_sig() ON atd16_univerttrail.geo_circuit;

CREATE TRIGGER t_before_iu_maj_longueur_sig
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_longueur_sig();
    
COMMENT ON TRIGGER t_before_iu_maj_longueur_sig ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Mise à jour de la longueur du circuit';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/07 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/08/04 : SL / Ajout du trigger t_before_iu_maj_longueur_sig
-- 2021/08/06 : SL / Suppression du champ id_circuit ; le lien se fait dans la table ngeo_circuit_passage_goudronne
-- 2021/10/28 : SL / Ajout de la vue v_geo_passage_goudronne_sans_site

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                    Table géographique : Passage goudronné                                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.geo_passage_goudronne;

CREATE TABLE atd16_univerttrail.geo_passage_goudronne
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    id_site integer, --[FK][UVT] Identifiant du site
    longueur_sig numeric(5,3), --[UVT] Longueur du tronçon (automatique)
    longueur numeric(5,3), --[UVT] Longueur du tronçon
    ident varchar(80), --[SIRAP] Identificaion de l'objet
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_passage_goudronne PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.geo_passage_goudronne OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.geo_passage_goudronne TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.geo_passage_goudronne IS '[ATD16] Table géographique contenant les tronçons goudronnées';

COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.id_site IS '[FK][UVT] Identifiant du site';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.longueur_sig IS '[UVT] Longueur du tronçon (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.longueur IS '[UVT] Longueur du tronçon';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.ident IS '[SIRAP] Identificaion de l''objet';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_goudronne.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                v_geo_passage_goudronne_sans_site : vue de la couche geo_passage_goudronne sans lien avec la couche geo_site                ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_passage_goudronne_sans_site;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_passage_goudronne_sans_site 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_passage_goudronne a
    WHERE id_site IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_passage_goudronne_sans_site OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_passage_goudronne_sans_site IS 
    '[ATD16] Vue de la couche geo_passage_goudronne sans lien avec la couche geo_site';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_passage_goudronne;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.geo_passage_goudronne
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_passage_goudronne IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_passage_goudronne;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.geo_passage_goudronne
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_passage_goudronne IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                 Mise à jour de la longueur du passage goudronné (champ longueur_sig)                                       ###
-- ##################################################################################################################################################

-- DROP TRIGGER atd16_univerttrail.t_before_iu_maj_longueur_sig() ON atd16_univerttrail.geo_passage_goudronne;

CREATE TRIGGER t_before_iu_maj_longueur_sig
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_passage_goudronne 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_longueur_sig();
    
COMMENT ON TRIGGER t_before_iu_maj_longueur_sig ON atd16_univerttrail.geo_passage_goudronne IS 
    '[ATD16] Mise à jour de la longueur du passage goudronné';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/07 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/08/06 : SL / Suppression du champ id_circuit ; le lien se fait dans la table ngeo_circuit_passage_prive
--                 . Suppression du champ document ; sw_custom_fiche s'en occupe
-- 2021/10/25 : SL / Ajout de la vue v_geo_passage_prive_sans_autorisation
-- 2021/10/28 : SL / Ajout de la vue v_geo_passage_prive_sans_site

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                      Table géographique : Passage privé                                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.geo_passage_prive;

CREATE TABLE atd16_univerttrail.geo_passage_prive
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    id_site integer, --[FK][UVT] Identifiant du site
    autorisation_passage integer, --[FK][UVT] Autorisation de passage (liste)
    ident varchar(80), --[SIRAP] Identificaion de l'objet
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_passage_prive PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.geo_passage_prive OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.geo_passage_prive TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.geo_passage_prive IS '[ATD16] Table géographique contenant les tronçons empruntant un passage privé';

COMMENT ON COLUMN atd16_univerttrail.geo_passage_prive.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_prive.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_prive.id_site IS '[FK][UVT] Identifiant du site';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_prive.autorisation_passage IS '[FK][UVT] Autorisation de passage (liste)';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_prive.ident IS '[SIRAP] Identificaion de l''objet';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_prive.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_prive.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_prive.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_prive.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_passage_prive.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                    v_geo_passage_prive_sans_site : vue de la couche geo_passage_prive sans lien avec la couche geo_site                    ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_passage_prive_sans_site;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_passage_prive_sans_site 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_passage_prive a
    WHERE id_site IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_passage_prive_sans_site OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_passage_prive_sans_site IS 
    '[ATD16] Vue de la couche geo_passage_prive sans lien avec la couche geo_site';


-- ##################################################################################################################################################
-- ###                   v_geo_passage_prive_sans_autorisation : vue de la couche geo_passage_prive sans autorisation de passage                  ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_passage_prive_sans_autorisation;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_passage_prive_sans_autorisation 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.autorisation_passage,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_passage_prive a
    WHERE a.autorisation_passage = '2' OR a.autorisation_passage IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_passage_prive_sans_autorisation OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_passage_prive_sans_autorisation IS 
    '[ATD16] Vue de la couche geo_passage_prive sans autorisation de passage';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_passage_prive;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.geo_passage_prive
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_passage_prive IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_passage_prive;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.geo_passage_prive
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_passage_prive IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
--                 . Ajout de vue v_geo_parcelle_x_circuit

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                            Vue(s) inter-schémas                                                            ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###               v_geo_parcelle_x_circuit : vue de la couche pci.geo_parcelle affichant les parcelles traversées par un circuit               ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_parcelle_x_circuit;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_parcelle_x_circuit 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.ident,
        b.gid AS id_circuit,
        b.ident AS nom_circuit,
        a.the_geom
    FROM pci.geo_parcelle a, atd16_univerttrail.geo_circuit b
    WHERE st_intersects(ST_SetSRID(a.the_geom,3857), ST_SetSRID(b.the_geom,3857));

ALTER TABLE atd16_univerttrail.v_geo_parcelle_x_circuit OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_parcelle_x_circuit IS 
    '[ATD16] Vue de la couche pci.geo_parcelle affichant les parcelles traversées par un circuit';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/10/28 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_supp_valeur_id_site() et du trigger associé t_after_d_site_supp_id_site

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                         Fonctions triggers et triggers spécifiques à la table geo_site                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                   Suppression de la valeur du champ id_site lorsque le site est supprimé                                   ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_supp_valeur_id_site();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_supp_valeur_id_site()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    UPDATE atd16_univerttrail.geo_circuit SET id_site = NULL WHERE id_site = OLD.gid;
	UPDATE atd16_univerttrail.geo_equipement SET id_site = NULL WHERE id_site = OLD.gid;
	UPDATE atd16_univerttrail.geo_parking SET id_site = NULL WHERE id_site = OLD.gid;
	UPDATE atd16_univerttrail.geo_passage_goudronne SET id_site = NULL WHERE id_site = OLD.gid;
	UPDATE atd16_univerttrail.geo_passage_prive SET id_site = NULL WHERE id_site = OLD.gid;
	UPDATE atd16_univerttrail.geo_signaletique SET id_site = NULL WHERE id_site = OLD.gid;
	UPDATE atd16_univerttrail.geo_zone_accueil SET id_site = NULL WHERE id_site = OLD.gid;
	UPDATE atd16_univerttrail.geo_zone_depart SET id_site = NULL WHERE id_site = OLD.gid;
	UPDATE atd16_univerttrail.geo_zone_echauffement SET id_site = NULL WHERE id_site = OLD.gid;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_supp_valeur_id_site() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_supp_valeur_id_site() IS 
    '[ATD16] Suppression de la valeur du champ id_site lorsque le site est supprimé';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_site_supp_id_site
    AFTER DELETE
    ON atd16_univerttrail.geo_site 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_supp_valeur_id_site();
    
COMMENT ON TRIGGER t_after_d_site_supp_id_site ON atd16_univerttrail.geo_site IS 
    '[ATD16] Suppression de la valeur du champ id_site lorsque le site est supprimé';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_nb_circuit() et du trigger associé t_after_iud_maj_nb_circuit
--                 . Ajout de la fonction f_maj_longueur_iti_totale() et du trigger associé t_after_iud_maj_longueur_iti_totale
-- 2021/08/03 : SL / Ajout de la fonction f_maj_longueur_sig() et du trigger associé t_before_iu_maj_longueur_sig
--                 . Ajout de la fonction f_maj_categorie_longueur() et du trigger associé t_before_iu_maj_categorie_longueur
--                 . Ajout de la fonction f_maj_categorie_denivele_positif() et du trigger associé t_before_iu_maj_categorie_denivele_positif
--                 . Ajout de la fonction f_maj_categorie_denivele_continu_max() et du trigger associé t_before_iu_maj_categorie_denivele_continu_max
-- 2021/08/04 : SL / Ajout de la fonction f_maj_cotation() et du trigger associé t_before_iu_maj_cotation
--                 . La fonction f_maj_longueur_sig() et son trigger associé t_before_iu_maj_longueur_sig dispatchés dans les triggers génériques
-- 2021/08/06 : SL / Ajout de la fonction f_supp_lien_circuit_passage_prive() et du trigger associé t_after_d_circuit_supp_lien_circuit_passage_prive
--                 . Ajout de la fonction f_supp_lien_circuit_passage_goudronne() et du trigger associé t_after_d_circuit_supp_lien_circuit_passage_goudronne
-- 2021/10/21 : SL / Ajout de la fonction f_maj_1longueur_gps() et du trigger associé t_before_iu_maj_1longueur_gps
-- 2021/10/26 : SL / Ajout de la fonction f_maj_proportion_goudronne_circuit() et du trigger associé t_before_u_maj_proportion_goudronne_circuit

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                       Fonctions triggers et triggers spécifiques à la table geo_circuit                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                              Mise à jour du nombre de circuit dans geo_site                                                ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_nb_circuit();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_nb_circuit()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_univerttrail.geo_site -- On met à jour la table geo_site
	    SET nb_circuit = --Le champ nb_circuit prend la valeur
	    (SELECT COUNT(b.gid) FROM atd16_univerttrail.geo_circuit b WHERE b.id_site = OLD.id_site) 
	    --de la somme des circuits ayant un id_site identique à celui qui est supprimé
	    WHERE gid::text = OLD.id_site::text; -- Où l'ancien' id_site est égal au gid de la table geo_site
    ELSE
        UPDATE atd16_univerttrail.geo_site -- On met à jour la table geo_site
	    SET nb_circuit = --Le champ nb_circuit prend la valeur
	    (SELECT COUNT(b.gid) FROM atd16_univerttrail.geo_circuit b WHERE b.id_site = NEW.id_site) 
	    --de la somme des circuits ayant un id_site identique à celui qui est créé
	    WHERE gid::text = NEW.id_site::text; -- Où le nouvel id_site est égal au gid de la table geo_site
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_nb_circuit() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_nb_circuit() IS '[ATD16] Mise à jour du nombre de circuit dans geo_site';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_nb_circuit 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_nb_circuit();
    
COMMENT ON TRIGGER t_after_iud_maj_nb_circuit ON atd16_univerttrail.geo_circuit IS '[ATD16] Mise à jour du nombre de circuit dans geo_site';


-- ##################################################################################################################################################
-- ###                                        Mise à jour du champ longueur_gps lorsqu'il n'est pas rempli                                        ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_1longueur_gps() et le trigger t_before_iu_maj_1longueur_gps
-- 2 - La fonction-trigger f_maj_categorie_longueur() et le trigger t_before_iu_maj_categorie_longueur
-- 2 - La fonction-trigger f_maj_longueur_iti_totale() et le trigger t_after_iud_maj_longueur_iti_totale (AFTER donc pas besoin de changer le nom)
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_1longueur_gps();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_1longueur_gps()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF NEW.longueur_gps IS NULL THEN
        NEW.longueur_gps = ST_Length(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154))*0.001;
        -- Calcul de la longueur de la nouvelle géométrie (reprojection car en 3857 la longueur est à peu près 50% plus grande)
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_1longueur_gps() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_1longueur_gps() IS '[ATD16] Mise à jour du champ longueur_gps lorsque sa valeur est NULL';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_1longueur_gps
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_1longueur_gps();
    
COMMENT ON TRIGGER t_before_iu_maj_1longueur_gps ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Mise à jour du champ longueur_gps lorsque sa valeur est NULL';


-- ##################################################################################################################################################
-- ###                                        Mise à jour de la longueur totale des circuits dans geo_site                                        ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_1longueur_gps() et le trigger t_before_iu_maj_1longueur_gps
-- 2 - La fonction-trigger f_maj_categorie_longueur() et le trigger t_before_iu_maj_categorie_longueur
-- 2 - La fonction-trigger f_maj_longueur_iti_totale() et le trigger t_after_iud_maj_longueur_iti_totale (AFTER donc pas besoin de changer le nom)
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_1longueur_gps() et le trigger t_before_iu_maj_1longueur_gps
-- 2 - La fonction-trigger f_maj_proportion_goudronne_circuit() et le trigger t_before_u_maj_proportion_goudronne_circuit
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_longueur_iti_totale();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_longueur_iti_totale()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_univerttrail.geo_site -- On met à jour la table geo_site
	    SET longueur_iti_totale = --Le champ longueur_iti_totale prend la valeur
	    (SELECT SUM(b.longueur_gps) FROM atd16_univerttrail.geo_circuit b WHERE b.id_site = OLD.id_site) 
	    --de la somme des longueurs des différents circuits ayant un id_site identique à celui qui est supprimé
	    WHERE gid::text = OLD.id_site::text; -- Où l'ancien' id_site est égal au gid de la table geo_site
    ELSE
        UPDATE atd16_univerttrail.geo_site -- On met à jour la table geo_site
	    SET longueur_iti_totale = --Le champ longueur_iti_totale prend la valeur
	    (SELECT SUM(b.longueur_gps) FROM atd16_univerttrail.geo_circuit b WHERE b.id_site = NEW.id_site) 
	    --de la somme des longueurs des différents circuits ayant un id_site identique à celui qui est créé
	    WHERE gid::text = NEW.id_site::text; -- Où le nouvel id_site est égal au gid de la table geo_site
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_longueur_iti_totale() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_longueur_iti_totale() IS '[ATD16] Mise à jour de la longueur totale des circuits dans geo_site';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_longueur_iti_totale 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_longueur_iti_totale();
    
COMMENT ON TRIGGER t_after_iud_maj_longueur_iti_totale ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Mise à jour de la longueur totale des circuits dans geo_site';


-- ##################################################################################################################################################
-- ###                                                   Mise à jour du champ categorie_longueur                                                  ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_1longueur_gps() et le trigger t_before_iu_maj_1longueur_gps
-- 2 - La fonction-trigger f_maj_categorie_longueur() et le trigger t_before_iu_maj_categorie_longueur
-- 2 - La fonction-trigger f_maj_longueur_iti_totale() et le trigger t_after_iud_maj_longueur_iti_totale (AFTER donc pas besoin de changer le nom)
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_categorie_denivele_continu_max() et le trigger t_before_iu_maj_categorie_denivele_continu_max
-- 1 - La fonction-trigger f_maj_categorie_denivele_positif() et le trigger t_before_iu_maj_categorie_denivele_positif
-- 1 - La fonction-trigger f_maj_categorie_longueur() et le trigger t_before_iu_maj_categorie_longueur
-- 2 - La fonction-trigger f_maj_cotation() et le trigger t_before_iu_maj_cotation
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_categorie_longueur();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_categorie_longueur()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF NEW.longueur_gps < 10 THEN NEW.categorie_longueur = '01';
    ELSIF NEW.longueur_gps >= 10 AND NEW.longueur_gps < 15 THEN NEW.categorie_longueur = '02';
    ELSIF NEW.longueur_gps >= 15 AND NEW.longueur_gps < 20 THEN NEW.categorie_longueur = '03';
    ELSIF NEW.longueur_gps >= 20 AND NEW.longueur_gps < 25 THEN NEW.categorie_longueur = '04';
    ELSIF NEW.longueur_gps >= 25 AND NEW.longueur_gps < 30 THEN NEW.categorie_longueur = '05';
    ELSIF NEW.longueur_gps >= 30 AND NEW.longueur_gps < 45 THEN NEW.categorie_longueur = '06';
    ELSIF NEW.longueur_gps >= 45 THEN NEW.categorie_longueur = '07';
    ELSE NEW.categorie_longueur = '00';
	END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_categorie_longueur() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_categorie_longueur() IS '[ATD16] Mise à jour du champ categorie_longueur';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_categorie_longueur
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_categorie_longueur();
    
COMMENT ON TRIGGER t_before_iu_maj_categorie_longueur ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Mise à jour du champ categorie_longueur';


-- ##################################################################################################################################################
-- ###                                               Mise à jour du champ categorie_denivele_positif                                              ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_categorie_denivele_continu_max() et le trigger t_before_iu_maj_categorie_denivele_continu_max
-- 1 - La fonction-trigger f_maj_categorie_denivele_positif() et le trigger t_before_iu_maj_categorie_denivele_positif
-- 1 - La fonction-trigger f_maj_categorie_longueur() et le trigger t_before_iu_maj_categorie_longueur
-- 2 - La fonction-trigger f_maj_cotation() et le trigger t_before_iu_maj_cotation
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_categorie_denivele_positif();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_categorie_denivele_positif()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF NEW.denivele_positif < 500 THEN NEW.categorie_denivele_positif = '01';
    ELSIF NEW.denivele_positif >= 500 AND NEW.denivele_positif < 750 THEN NEW.categorie_denivele_positif = '02';
    ELSIF NEW.denivele_positif >= 750 AND NEW.denivele_positif < 1000 THEN NEW.categorie_denivele_positif = '03';
    ELSIF NEW.denivele_positif >= 1000 AND NEW.denivele_positif < 1500 THEN NEW.categorie_denivele_positif = '04';
    ELSIF NEW.denivele_positif >= 1500 AND NEW.denivele_positif < 2000 THEN NEW.categorie_denivele_positif = '05';
    ELSIF NEW.denivele_positif >= 2000 AND NEW.denivele_positif < 2500 THEN NEW.categorie_denivele_positif = '06';
    ELSIF NEW.denivele_positif >= 2500 THEN NEW.categorie_denivele_positif = '07';
    ELSE NEW.categorie_denivele_positif = '00';
	END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_categorie_denivele_positif() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_categorie_denivele_positif() IS '[ATD16] Mise à jour du champ categorie_denivele_positif';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_categorie_denivele_positif
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_categorie_denivele_positif();
    
COMMENT ON TRIGGER t_before_iu_maj_categorie_denivele_positif ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Mise à jour du champ categorie_denivele_positif';


-- ##################################################################################################################################################
-- ###                                             Mise à jour du champ categorie_denivele_continu_max                                            ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_categorie_denivele_continu_max() et le trigger t_before_iu_maj_categorie_denivele_continu_max
-- 1 - La fonction-trigger f_maj_categorie_denivele_positif() et le trigger t_before_iu_maj_categorie_denivele_positif
-- 1 - La fonction-trigger f_maj_categorie_longueur() et le trigger t_before_iu_maj_categorie_longueur
-- 2 - La fonction-trigger f_maj_cotation() et le trigger t_before_iu_maj_cotation
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_categorie_denivele_continu_max();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_categorie_denivele_continu_max()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF NEW.denivele_continu_max < 300 THEN NEW.categorie_denivele_continu_max = '01';
    ELSIF NEW.denivele_continu_max >= 300 AND NEW.denivele_continu_max < 600 THEN NEW.categorie_denivele_continu_max = '02';
    ELSIF NEW.denivele_continu_max >= 600 AND NEW.denivele_continu_max < 1000 THEN NEW.categorie_denivele_continu_max = '03';
    ELSIF NEW.denivele_continu_max >= 1000 THEN NEW.categorie_denivele_continu_max = '04';
    ELSE NEW.categorie_denivele_continu_max = '00';
	END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_categorie_denivele_continu_max() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_categorie_denivele_continu_max() IS '[ATD16] Mise à jour du champ categorie_denivele_continu_max';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_categorie_denivele_continu_max
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_categorie_denivele_continu_max();
    
COMMENT ON TRIGGER t_before_iu_maj_categorie_denivele_continu_max ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Mise à jour du champ categorie_denivele_continu_max';


-- ##################################################################################################################################################
-- ###                                             Mise à jour du champ cotation (addition des notes)                                             ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_categorie_denivele_continu_max() et le trigger t_before_iu_maj_categorie_denivele_continu_max
-- 1 - La fonction-trigger f_maj_categorie_denivele_positif() et le trigger t_before_iu_maj_categorie_denivele_positif
-- 1 - La fonction-trigger f_maj_categorie_longueur() et le trigger t_before_iu_maj_categorie_longueur
-- 2 - La fonction-trigger f_maj_cotation() et le trigger t_before_iu_maj_cotation
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_cotation();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_cotation()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    NEW.cotation = -- Le champ cotation du circuit créé ou modifié est égal à
    (SELECT b.note + c.note + d.note + e.note -- l'addition des notes correspondants aux 4 catégories
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON NEW.categorie_longueur::text = b.code::text -- jointures avec chacune des tables listes
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON NEW.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON NEW.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON NEW.technicite::text = e.code::text
    WHERE NEW.gid = a.gid); -- sélection de la ligne nouvellement créée
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_cotation() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_cotation() IS '[ATD16] Mise à jour du champ cotation (addition des notes)';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_cotation
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_cotation();
    
COMMENT ON TRIGGER t_before_iu_maj_cotation ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Mise à jour du champ cotation (addition des notes)';


-- ##################################################################################################################################################
-- ###                        Suppression des liens entre le circuit et les passages privés lorsque le circuit est supprimé                       ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_supp_lien_circuit_passage_prive();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_supp_lien_circuit_passage_prive()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    DELETE FROM atd16_univerttrail.ngeo_circuit_passage_prive
    WHERE id_circuit = OLD.gid;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_supp_lien_circuit_passage_prive() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_supp_lien_circuit_passage_prive() IS 
    '[ATD16] Suppression des liens entre le circuit et les passages privés (dans ngeo_circuit_passage_prive) lorsque le circuit est supprimé';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_circuit_supp_lien_circuit_passage_prive
    AFTER DELETE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_supp_lien_circuit_passage_prive();
    
COMMENT ON TRIGGER t_after_d_circuit_supp_lien_circuit_passage_prive ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Suppression des liens entre le circuit et les passages privés (dans ngeo_circuit_passage_prive) lorsque le circuit est supprimé';


-- ##################################################################################################################################################
-- ###                      Suppression des liens entre le circuit et les passages goudronnés lorsque le circuit est supprimé                     ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_supp_lien_circuit_passage_goudronne();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_supp_lien_circuit_passage_goudronne()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    DELETE FROM atd16_univerttrail.ngeo_circuit_passage_goudronne
    WHERE id_circuit = OLD.gid;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_supp_lien_circuit_passage_goudronne() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_supp_lien_circuit_passage_goudronne() IS 
    '[ATD16] Suppression des liens entre le circuit et les passages goudronnés (dans ngeo_circuit_passage_goudronne) lorsque le circuit est supprimé';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_circuit_supp_lien_circuit_passage_goudronne
    AFTER DELETE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_supp_lien_circuit_passage_goudronne();
    
COMMENT ON TRIGGER t_after_d_circuit_supp_lien_circuit_passage_goudronne ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Suppression des liens entre le circuit et les passages goudronnés (dans ngeo_circuit_passage_goudronne) lorsque le circuit est supprimé';


-- ##################################################################################################################################################
-- ###                                   Mise à jour de la proportion des passages goudronnés dans geo_circuit                                    ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_1longueur_gps() et le trigger t_before_iu_maj_1longueur_gps
-- 2 - La fonction-trigger f_maj_proportion_goudronne_circuit() et le trigger t_before_u_maj_proportion_goudronne_circuit
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_proportion_goudronne_circuit();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_proportion_goudronne_circuit()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    NEW.proportion_goudronne = NEW.longueur_goudronne / NEW.longueur_gps * 100;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_proportion_goudronne_circuit() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_proportion_goudronne_circuit() IS 
    '[ATD16] Mise à jour de la proportion des portions goudronnées dans geo_circuit lors d''une modification d''un circuit';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_u_maj_proportion_goudronne_circuit
    BEFORE UPDATE
    ON atd16_univerttrail.geo_circuit
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_proportion_goudronne_circuit();
    
COMMENT ON TRIGGER t_before_u_maj_proportion_goudronne_circuit ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Mise à jour de la proportion des portions goudronnées dans geo_circuit';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/06 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_supp_lien_passage_goudronne_circuit() et du trigger associé t_after_d_passage_goudronne_supp_lien_passage_goudronne_circuit
-- 2021/10/21 : SL / Ajout de la fonction f_maj_longueur_passage_goudronne() et du trigger associé t_before_iu_maj_longueur_passage_goudronne

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                  Fonctions triggers et triggers spécifiques à la table geo_passage_goudronne                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                 Suppression des liens entre le circuit et les passages goudronnés lorsque le passage goudronné est supprimé                ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_supp_lien_passage_goudronne_circuit();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_supp_lien_passage_goudronne_circuit()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    DELETE FROM atd16_univerttrail.ngeo_circuit_passage_goudronne
    WHERE id_passage_goudronne = OLD.gid;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_supp_lien_passage_goudronne_circuit() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_supp_lien_passage_goudronne_circuit() IS 
    '[ATD16] Suppression des liens entre le circuit et les passages goudronnés (dans ngeo_circuit_passage_goudronne) lorsque le passage goudronné est supprimé';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_passage_goudronne_supp_lien_passage_goudronne_circuit
    AFTER DELETE
    ON atd16_univerttrail.geo_passage_goudronne 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_supp_lien_passage_goudronne_circuit();
    
COMMENT ON TRIGGER t_after_d_passage_goudronne_supp_lien_passage_goudronne_circuit ON atd16_univerttrail.geo_passage_goudronne IS 
    '[ATD16] Suppression des liens entre le circuit et les passages goudronnés (dans ngeo_circuit_passage_goudronne) lorsque le passage goudronné est supprimé';


-- ##################################################################################################################################################
-- ###                                          Mise à jour du champ longueur lorsqu'il n'est pas rempli                                          ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_longueur_passage_goudronne();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_longueur_passage_goudronne()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF NEW.longueur IS NULL THEN
        NEW.longueur = ST_Length(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154))*0.001;
        -- Calcul de la longueur de la nouvelle géométrie (reprojection car en 3857 la longueur est à peu près 50% plus grande)
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_longueur_passage_goudronne() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_longueur_passage_goudronne() IS '[ATD16] Mise à jour du champ longueur lorsque sa valeur est NULL';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_longueur_passage_goudronne
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_passage_goudronne 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_longueur_passage_goudronne();
    
COMMENT ON TRIGGER t_before_iu_maj_longueur_passage_goudronne ON atd16_univerttrail.geo_passage_goudronne IS 
    '[ATD16] Mise à jour du champ longueur lorsque sa valeur est NULL';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/06 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_supp_lien_passage_prive_circuit() et du trigger associé t_after_d_passage_prive_supp_lien_passage_prive_circuit

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                    Fonctions triggers et triggers spécifiques à la table geo_passage_prive                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                     Suppression des liens entre le circuit et les passages privés lorsque le passage privé est supprimé                    ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_supp_lien_passage_prive_circuit();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_supp_lien_passage_prive_circuit()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    DELETE FROM atd16_univerttrail.ngeo_circuit_passage_prive
    WHERE id_passage_prive = OLD.gid;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_supp_lien_passage_prive_circuit() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_supp_lien_passage_prive_circuit() IS 
    '[ATD16] Suppression des liens entre le circuit et les passages privés (dans ngeo_circuit_passage_prive) lorsque le passage privé est supprimé';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_passage_prive_supp_lien_passage_prive_circuit
    AFTER DELETE
    ON atd16_univerttrail.geo_passage_prive 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_supp_lien_passage_prive_circuit();
    
COMMENT ON TRIGGER t_after_d_passage_prive_supp_lien_passage_prive_circuit ON atd16_univerttrail.geo_passage_prive IS 
    '[ATD16] Suppression des liens entre le circuit et les passages privés (dans ngeo_circuit_passage_prive) lorsque le passage privé est supprimé';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/05 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_presence_passage_prive() et du trigger associé t_after_iud_maj_presence_passage_prive
-- 2021/08/06 : SL / Modification du nom en 250_trigger_ngeo_circuit_passage_prive.sql et modification de la table sur laquelle cela s'applique

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                               Fonctions triggers et triggers spécifiques à la table ngeo_circuit_passage_prive                             ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                       Mise à jour de la présence de passage privé dans geo_circuit                                         ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_presence_passage_prive();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_presence_passage_prive()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        IF 
            COUNT(id_circuit) > 0
            FROM atd16_univerttrail.ngeo_circuit_passage_prive
            WHERE OLD.id_circuit = id_circuit -- Si la somme des passages privés ayant un id_circuit correspondant à celui qui est créé est supérieur à 0 
        THEN -- alors
            UPDATE atd16_univerttrail.geo_circuit -- On met à jour la table geo_circuit
	        SET passage_prive = '01' -- Le champ passage_prive prend la valeur 01 (Oui)
	        WHERE gid::text = OLD.id_circuit::text; -- Où le nouvel id_circuit est égal au gid de la table geo_circuit
	    ELSE -- sinon
	        UPDATE atd16_univerttrail.geo_circuit -- On met à jour la table geo_circuit
	        SET passage_prive = '02' -- Le champ passage_prive prend la valeur 02 (Non)
	        WHERE gid::text = OLD.id_circuit::text; -- Où le nouvel id_circuit est égal au gid de la table geo_circuit
	    END IF;
	ELSE
        IF 
            COUNT(id_circuit) > 0
            FROM atd16_univerttrail.ngeo_circuit_passage_prive
            WHERE NEW.id_circuit = id_circuit -- Si la somme des passages privés ayant un id_circuit correspondant à celui qui est créé est supérieur à 0 
        THEN -- alors
            UPDATE atd16_univerttrail.geo_circuit -- On met à jour la table geo_circuit
	        SET passage_prive = '01' -- Le champ passage_prive prend la valeur 01 (Oui)
	        WHERE gid::text = NEW.id_circuit::text; -- Où le nouvel id_circuit est égal au gid de la table geo_circuit
	    ELSE -- sinon
	        UPDATE atd16_univerttrail.geo_circuit -- On met à jour la table geo_circuit
	        SET passage_prive = '02' -- Le champ passage_prive prend la valeur 02 (Non)
	        WHERE gid::text = NEW.id_circuit::text; -- Où le nouvel id_circuit est égal au gid de la table geo_circuit
	    END IF;
	END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_presence_passage_prive() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_presence_passage_prive() IS '[ATD16] Mise à jour de la présence de passage privé dans geo_circuit';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_presence_passage_prive 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_univerttrail.ngeo_circuit_passage_prive 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_presence_passage_prive();
    
COMMENT ON TRIGGER t_after_iud_maj_presence_passage_prive ON atd16_univerttrail.ngeo_circuit_passage_prive IS 
    '[ATD16] Mise à jour de la présence de passage privé dans geo_circuit';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/05 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_longueur_goudronne() et du trigger associé t_after_iu_maj_longueur_goudronne
--                 . Ajout de la fonction f_maj_proportion_goudronne() et du trigger associé t_after_iu_maj_proportion_goudronne
-- 2021/08/06 : SL / Modification du nom du fichier en 260_trigger_ngeo_circuit_passage_goudronne.sql. S'applique à ngeo_circuit_passage_goudronne
--                 . Modification de la fonction f_maj_longueur_goudronne() et du trigger associé t_after_iud_maj_longueur_goudronne
--                 . Modification de la fonction f_maj_proportion_goudronne() et du trigger associé t_after_iud_maj_proportion_goudronne
-- 2021/10/26 : SL / Modification du nom de la fonction f_maj_proportion_goudronne_pago() et du trigger associé t_after_iud_maj_proportion_goudronne_pago

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                             Fonctions triggers et triggers spécifiques à la table ngeo_circuit_passage_goudronne                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                 Mise à jour de la longueur totale des passages goudronnés dans geo_circuit                                 ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_longueur_goudronne() et le trigger t_after_iud_maj_longueur_goudronne
-- 2 - La fonction-trigger f_maj_proportion_goudronne_pago() et le trigger t_after_iud_maj_proportion_goudronne_pago
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_longueur_goudronne();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_longueur_goudronne()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN -- Si il y a suppression de données dans la table ngeo_circuit_passage_goudronne alors
        UPDATE atd16_univerttrail.geo_circuit -- On met à jour la table geo_circuit
	    SET longueur_goudronne = --Le champ longueur_goudronne prend la valeur
	        (
                SELECT SUM(b.longueur) 
                FROM atd16_univerttrail.geo_passage_goudronne b 
                LEFT JOIN atd16_univerttrail.ngeo_circuit_passage_goudronne c ON b.gid = c.id_passage_goudronne
                WHERE c.id_circuit = OLD.id_circuit
            ) 
	        --de la somme des longueurs des différents passages goudronnés ayant un id_circuit identique à celui qui est supprimé
	    WHERE gid::text = OLD.id_circuit::text; -- Où l'ancien id_circuit est égal au gid de la table geo_circuit
    ELSE
        UPDATE atd16_univerttrail.geo_circuit -- On met à jour la table geo_circuit
	    SET longueur_goudronne = --Le champ longueur_goudronne prend la valeur
	        (
                SELECT SUM(b.longueur) 
                FROM atd16_univerttrail.geo_passage_goudronne b 
                LEFT JOIN atd16_univerttrail.ngeo_circuit_passage_goudronne c ON b.gid = c.id_passage_goudronne
                WHERE c.id_circuit = NEW.id_circuit
            ) 
	        --de la somme des longueurs des différents passages goudronnés ayant un id_circuit identique à celui qui est inséré ou maj
	    WHERE gid::text = NEW.id_circuit::text; -- Où le nouvel id_circuit est égal au gid de la table geo_circuit
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_longueur_goudronne() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_longueur_goudronne() IS 
    '[ATD16] Mise à jour de la longueur des portions goudronnées totale dans geo_circuit';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_longueur_goudronne 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_univerttrail.ngeo_circuit_passage_goudronne 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_longueur_goudronne();
    
COMMENT ON TRIGGER t_after_iud_maj_longueur_goudronne ON atd16_univerttrail.ngeo_circuit_passage_goudronne IS 
    '[ATD16] Mise à jour de la longueur des portions goudronnées totale dans geo_circuit';


-- ##################################################################################################################################################
-- ###                                   Mise à jour de la proportion des passages goudronnés dans geo_circuit                                    ###
-- ##################################################################################################################################################

-- L'ordre des triggers est important :
-- 1 - La fonction-trigger f_maj_longueur_goudronne() et le trigger t_after_iud_maj_longueur_goudronne
-- 2 - La fonction-trigger f_maj_proportion_goudronne_pago() et le trigger t_after_iud_maj_proportion_goudronne_pago
-- PostgreSQL™ utilise l'ordre alphabétique de leur nom pour ordonner leur lancement.

-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_proportion_goudronne_pago();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_proportion_goudronne_pago()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN -- Si il y a suppression de données dans la table ngeo_circuit_passage_goudronne alors
        UPDATE atd16_univerttrail.geo_circuit -- On met à jour la table geo_circuit
        SET proportion_goudronne = --Le champ proportion_goudronne prend la valeur
        (
            SELECT SUM(b.longueur) 
            FROM atd16_univerttrail.geo_passage_goudronne b 
            LEFT JOIN atd16_univerttrail.ngeo_circuit_passage_goudronne c ON b.gid = c.id_passage_goudronne
            WHERE c.id_circuit = OLD.id_circuit
        ) 
        --de la somme des longueurs des différents passages goudronnés ayant un id_circuit identique à celui qui est supprimé
        / longueur_gps
        -- divisé par la longueur totale
        * 100
        -- multiplié par 100 pour obtenir un pourcentage
        WHERE gid::text = OLD.id_circuit::text; -- Où l'ancien id_circuit est égal au gid de la table geo_circuit
    ELSE
        UPDATE atd16_univerttrail.geo_circuit -- On met à jour la table geo_circuit
        SET proportion_goudronne = --Le champ proportion_goudronne prend la valeur
        (
            SELECT SUM(b.longueur) 
            FROM atd16_univerttrail.geo_passage_goudronne b 
            LEFT JOIN atd16_univerttrail.ngeo_circuit_passage_goudronne c ON b.gid = c.id_passage_goudronne
            WHERE c.id_circuit = NEW.id_circuit
        ) 
        --de la somme des longueurs des différents passages goudronnés ayant un id_circuit identique à celui qui est créé ou maj
        / longueur_gps
        -- divisé par la longueur totale
        * 100
        -- multiplié par 100 pour obtenir un pourcentage
        WHERE gid::text = NEW.id_circuit::text; -- Où le nouvel id_circuit est égal au gid de la table geo_circuit
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_proportion_goudronne_pago() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_proportion_goudronne_pago() IS 
    '[ATD16] Mise à jour de la proportion des portions goudronnées dans geo_circuit lors d''une modification d''un passage goudronné';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_proportion_goudronne_pago
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_univerttrail.ngeo_circuit_passage_goudronne 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_proportion_goudronne_pago();
    
COMMENT ON TRIGGER t_after_iud_maj_proportion_goudronne_pago ON atd16_univerttrail.ngeo_circuit_passage_goudronne IS 
    '[ATD16] Mise à jour de la proportion des portions goudronnées dans geo_circuit';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/10/22 : SL / Création du fichier sur Git
-- 2021/10/22 : SL / Ajout de la fonction f_maj_longueur_zone_echauffement() et du trigger associé t_before_iu_maj_longueur_zone_echauffement

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                  Fonctions triggers et triggers spécifiques à la table geo_zone_echauffement                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                          Mise à jour du champ longueur lorsqu'il n'est pas rempli                                          ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_maj_longueur_zone_echauffement();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_maj_longueur_zone_echauffement()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF NEW.longueur IS NULL THEN
        NEW.longueur = ST_Length(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154))*0.001;
        -- Calcul de la longueur de la nouvelle géométrie (reprojection car en 3857 la longueur est à peu près 50% plus grande)
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_maj_longueur_zone_echauffement() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_maj_longueur_zone_echauffement() IS '[ATD16] Mise à jour du champ longueur lorsque sa valeur est NULL';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_longueur_zone_echauffement
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_zone_echauffement 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_longueur_zone_echauffement();
    
COMMENT ON TRIGGER t_before_iu_maj_longueur_zone_echauffement ON atd16_univerttrail.geo_zone_echauffement IS 
    '[ATD16] Mise à jour du champ longueur lorsque sa valeur est NULL';

