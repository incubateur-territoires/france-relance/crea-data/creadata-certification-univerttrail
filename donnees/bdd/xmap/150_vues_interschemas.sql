-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
--                 . Ajout de vue v_geo_parcelle_x_circuit

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                            Vue(s) inter-schémas                                                            ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###               v_geo_parcelle_x_circuit : vue de la couche pci.geo_parcelle affichant les parcelles traversées par un circuit               ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_parcelle_x_circuit;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_parcelle_x_circuit 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.ident,
        b.gid AS id_circuit,
        b.ident AS nom_circuit,
        a.the_geom
    FROM pci.geo_parcelle a, atd16_univerttrail.geo_circuit b
    WHERE st_intersects(ST_SetSRID(a.the_geom,3857), ST_SetSRID(b.the_geom,3857));

ALTER TABLE atd16_univerttrail.v_geo_parcelle_x_circuit OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_parcelle_x_circuit IS 
    '[ATD16] Vue de la couche pci.geo_parcelle affichant les parcelles traversées par un circuit';

