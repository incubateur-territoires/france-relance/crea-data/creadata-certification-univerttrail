-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/07 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
--                 . Ajout de vue v_geo_circuit
-- 2021/07/08 : SL / Ajout de vue v_geo_circuit_1
--                 . Ajout de vue v_geo_circuit_2
--                 . Ajout de vue v_geo_circuit_3
--                 . Ajout de vue v_geo_circuit_4
--                 . Ajout de vue v_geo_circuit_5
-- 2021/08/04 : SL / Ajout du trigger t_before_iu_maj_longueur_sig
-- 2021/10/26 : SL / Ajout de vue v_geo_circuit_alert_passage_goudronne
-- 2021/10/26 : SL / Ajout de vue v_geo_circuit_sans_site
-- 2021/11/12 : SL / Ajout de vue v_geo_circuit_sans_niveau
--                 . Ajout de vue v_geo_circuit_avec_niveau_incoherent
-- 2022/11/25 : SL / Ajout de vue v_geo_circuit_6
--                 . Ajout de vue v_geo_circuit_7
--                 . Ajout de vue v_geo_circuit_8
--                 . Ajout de vue v_geo_circuit_9
--                 . Ajout de vue v_geo_circuit_11
--                 . Ajout de vue v_geo_circuit_16
--                 . Ajout de vue v_geo_circuit_21
--                 . Ajout de vue v_geo_circuit_22
--                 . Ajout de vue v_geo_circuit_23
--                 . Ajout de vue v_geo_circuit_24
--                 . Ajout de vue v_geo_circuit_25

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                              Table géographique : Circuit de trail (itinéraire)                                            ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.geo_circuit;

CREATE TABLE atd16_univerttrail.geo_circuit
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    id_site integer, --[FK][UVT] Identifiant du site
    numero varchar(20), --[ATD16] Numéro du circuit
    longueur_sig numeric(6,3), --[UVT] Longueur du circuit en km (automatique)
    longueur_gps numeric(6,3), --[UVT] Longueur du circuit en km
    categorie_longueur varchar(2), --[FK][UVT] Catégorie associée à la longueur (liste) (automatique)
    denivele_positif integer, --[UVT] Dénivelé postif en m
    categorie_denivele_positif varchar(2), --[FK][UVT] Catégorie associée au dénivelé positif (liste) (automatique)
    denivele_continu_max integer, --[UVT] Dénivelé continu max en m (positif ou négatif)
    categorie_denivele_continu_max varchar(2), --[FK][UVT] Catégorie associée au dénivelé continu max (liste) (automatique)
    technicite varchar(2), --[FK][UVT] Technicité du circuit (liste)
    cotation integer, --[UVT] Cotation du circuit : addition des notes de longueur du circuit, de dénivelé positif, de dénivelé continu max et de technicité (automatique)
    niveau varchar(2), --[FK][UVT] Niveau de difficulté (liste)
    risque varchar(2), --[FK][UVT] Niveau de risque (liste)
    longueur_goudronne numeric(5,3), --[UVT] Longueur totale des portions goudronnées en km (automatique)
    proportion_goudronne numeric(5,2), --[UVT] Proportion de portions goudronnées sur la longueur totale du circuit (automatique)
    passage_prive varchar(2), --[FK][UVT] Présence de passage privé sur le tracé du circuit (liste)
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Nom du circuit
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_circuit PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.geo_circuit OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.geo_circuit TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.geo_circuit IS '[ATD16] Table géographique contenant les circuits trail pour la certification UniVert''Trail';

COMMENT ON COLUMN atd16_univerttrail.geo_circuit.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.id_site IS '[FK][UVT] Identifiant du site';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.numero IS '[ATD16] Numéro du circuit';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.longueur_sig IS '[UVT] Longueur du circuit en km (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.longueur_gps IS '[UVT] Longueur du circuit en km';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.categorie_longueur IS '[FK][UVT] Catégorie associée à la longueur (liste) (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.denivele_positif IS '[UVT] Dénivelé postif en m';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.categorie_denivele_positif IS 
    '[FK][UVT] Catégorie associée au dénivelé positif (liste) (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.denivele_continu_max IS '[UVT] Dénivelé continu max en m (positif ou négatif)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.categorie_denivele_continu_max IS 
    '[FK][UVT] Catégorie associée au dénivelé continu max (liste) (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.technicite IS '[FK][UVT] Technicité du circuit (liste)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.cotation IS 
    '[UVT] Cotation du circuit : addition des notes de longueur du circuit, de dénivelé positif, de dénivelé continu max et de technicité (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.niveau IS '[FK][UVT] Niveau de difficulté (liste)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.risque IS '[FK][UVT] Niveau de risque (liste)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.longueur_goudronne IS '[UVT] Longueur totale des portions goudronnées en km (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.proportion_goudronne IS 
    '[UVT] Proportion de portions goudronnées sur la longueur totale du circuit (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.passage_prive IS '[FK][UVT] Présence de passage privé sur le tracé du circuit (liste)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.ident IS '[SIRAP] Identificaion de l''objet / Nom du circuit';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_circuit.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###           v_geo_circuit : vue de la couche geo_circuit affichant en plus les champs notes (en production à la place de la table)           ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text;

ALTER TABLE atd16_univerttrail.v_geo_circuit OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit IS 
    '[ATD16] Vue de la couche geo_circuit affichant en plus les champs notes (en production à la place de la table)';


-- ##################################################################################################################################################
-- ###                             v_geo_circuit_1 : vue de la couche geo_circuit affichant uniquement le circuit n°1                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_1;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_1 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '1';

ALTER TABLE atd16_univerttrail.v_geo_circuit_1 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_1 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°1';


-- ##################################################################################################################################################
-- ###                             v_geo_circuit_2 : vue de la couche geo_circuit affichant uniquement le circuit n°2                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_2;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_2 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '2';

ALTER TABLE atd16_univerttrail.v_geo_circuit_2 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_2 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°2';


-- ##################################################################################################################################################
-- ###                             v_geo_circuit_3 : vue de la couche geo_circuit affichant uniquement le circuit n°3                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_3;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_3 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '3';

ALTER TABLE atd16_univerttrail.v_geo_circuit_3 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_3 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°3';


-- ##################################################################################################################################################
-- ###                             v_geo_circuit_4 : vue de la couche geo_circuit affichant uniquement le circuit n°4                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_4;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_4 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '4';

ALTER TABLE atd16_univerttrail.v_geo_circuit_4 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_4 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°4';


-- ##################################################################################################################################################
-- ###                             v_geo_circuit_5 : vue de la couche geo_circuit affichant uniquement le circuit n°5                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_5;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_5 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '5';

ALTER TABLE atd16_univerttrail.v_geo_circuit_5 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_5 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°5';


-- ##################################################################################################################################################
-- ###                             v_geo_circuit_6 : vue de la couche geo_circuit affichant uniquement le circuit n°6                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_6;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_6 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '6';

ALTER TABLE atd16_univerttrail.v_geo_circuit_6 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_6 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°6';


-- ##################################################################################################################################################
-- ###                             v_geo_circuit_7 : vue de la couche geo_circuit affichant uniquement le circuit n°7                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_7;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_7 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '7';

ALTER TABLE atd16_univerttrail.v_geo_circuit_7 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_7 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°7';


-- ##################################################################################################################################################
-- ###                             v_geo_circuit_8 : vue de la couche geo_circuit affichant uniquement le circuit n°8                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_8;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_8 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '8';

ALTER TABLE atd16_univerttrail.v_geo_circuit_8 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_8 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°8';


-- ##################################################################################################################################################
-- ###                             v_geo_circuit_9 : vue de la couche geo_circuit affichant uniquement le circuit n°9                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_9;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_9 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '9';

ALTER TABLE atd16_univerttrail.v_geo_circuit_9 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_9 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°9';


-- ##################################################################################################################################################
-- ###                            v_geo_circuit_11 : vue de la couche geo_circuit affichant uniquement le circuit n°11                            ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_11;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_11 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '11';

ALTER TABLE atd16_univerttrail.v_geo_circuit_11 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_11 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°11';


-- ##################################################################################################################################################
-- ###                            v_geo_circuit_16 : vue de la couche geo_circuit affichant uniquement le circuit n°16                            ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_16;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_16 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '16';

ALTER TABLE atd16_univerttrail.v_geo_circuit_16 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_16 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°16';


-- ##################################################################################################################################################
-- ###                            v_geo_circuit_21 : vue de la couche geo_circuit affichant uniquement le circuit n°21                            ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_21;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_21 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '21';

ALTER TABLE atd16_univerttrail.v_geo_circuit_21 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_21 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°21';


-- ##################################################################################################################################################
-- ###                            v_geo_circuit_22 : vue de la couche geo_circuit affichant uniquement le circuit n°22                            ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_22;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_22 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '22';

ALTER TABLE atd16_univerttrail.v_geo_circuit_22 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_22 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°22';


-- ##################################################################################################################################################
-- ###                            v_geo_circuit_23 : vue de la couche geo_circuit affichant uniquement le circuit n°23                            ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_23;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_23 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '23';

ALTER TABLE atd16_univerttrail.v_geo_circuit_23 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_23 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°23';


-- ##################################################################################################################################################
-- ###                            v_geo_circuit_24 : vue de la couche geo_circuit affichant uniquement le circuit n°24                            ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_24;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_24 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '24';

ALTER TABLE atd16_univerttrail.v_geo_circuit_24 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_24 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°24';


-- ##################################################################################################################################################
-- ###                            v_geo_circuit_25 : vue de la couche geo_circuit affichant uniquement le circuit n°25                            ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_25;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_25 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_sig,
        a.longueur_gps,
        a.categorie_longueur,
        b.note AS note_longueur,
        a.denivele_positif,
        a.categorie_denivele_positif,
        c.note AS note_denivele_positif,
        a.denivele_continu_max,
        a.categorie_denivele_continu_max,
        d.note AS note_denivele_continu_max,
        a.technicite,
        e.note AS note_technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.passage_prive,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    LEFT JOIN atd16_univerttrail.lst_categorie_longueur b ON a.categorie_longueur::text = b.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_positif c ON a.categorie_denivele_positif::text = c.code::text
    LEFT JOIN atd16_univerttrail.lst_categorie_denivele_continu_max d ON a.categorie_denivele_continu_max::text = d.code::text
    LEFT JOIN atd16_univerttrail.lst_technicite e ON a.technicite::text = e.code::text
    WHERE numero = '25';

ALTER TABLE atd16_univerttrail.v_geo_circuit_25 OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_25 IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement le circuit n°25';


-- ##################################################################################################################################################
-- ###  v_geo_circuit_alert_passage_goudronne : vue de la couche geo_circuit affichant uniquement ceux qui ont plus de 20% de passages goudronnés ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_alert_passage_goudronne;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_alert_passage_goudronne 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.longueur_gps,
        a.denivele_positif,
        a.denivele_continu_max,
        a.technicite,
        a.cotation,
        a.niveau,
        a.risque,
        a.longueur_goudronne,
        a.proportion_goudronne,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    WHERE proportion_goudronne > 20;

ALTER TABLE atd16_univerttrail.v_geo_circuit_alert_passage_goudronne OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_alert_passage_goudronne IS 
    '[ATD16] Vue de la couche geo_circuit affichant uniquement ceux qui ont plus de 20% de passages goudronnés';


-- ##################################################################################################################################################
-- ###                          v_geo_circuit_sans_site : vue de la couche geo_circuit sans lien avec la couche geo_site                          ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_sans_site;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_sans_site 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    WHERE id_site IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_circuit_sans_site OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_sans_site IS 
    '[ATD16] Vue de la couche geo_circuit sans lien avec la couche geo_site';


-- ##################################################################################################################################################
-- ###                         v_geo_circuit_sans_niveau : vue de la couche geo_circuit avec le champ niveau non renseigné                        ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_sans_niveau;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_sans_niveau 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.ident,
        a.cotation,
        a.niveau,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    WHERE niveau IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_circuit_sans_niveau OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_sans_niveau IS 
    '[ATD16] Vue de la couche geo_circuit avec le champ niveau non renseigné';


-- ##################################################################################################################################################
-- ###        v_geo_circuit_avec_niveau_incoherent : vue de la couche geo_circuit avec le champ niveau qui ne correspond pas à la cotation        ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_circuit_avec_niveau_incoherent;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_circuit_avec_niveau_incoherent 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.numero,
        a.ident,
        a.cotation,
        a.niveau,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_circuit a
    WHERE 
        (
            niveau = '01'
            AND
            (cotation < '5' OR cotation > '6')
        )
        OR
        (
            niveau = '02'
            AND
            (cotation < '6' OR cotation > '11')
        )
        OR
        (
            niveau = '03'
            AND
            (cotation < '10' OR cotation > '18')
        )
        OR
        (
            niveau = '04'
            AND
            (cotation < '17' OR cotation > '26')
        );

ALTER TABLE atd16_univerttrail.v_geo_circuit_avec_niveau_incoherent OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_circuit_avec_niveau_incoherent IS 
    '[ATD16] Vue de la couche geo_circuit avec le champ niveau qui ne correspond pas à la cotation';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_circuit;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.geo_circuit
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_circuit;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.geo_circuit
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                         Mise à jour de la longueur du circuit (champ longueur_sig)                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER atd16_univerttrail.t_before_iu_maj_longueur_sig() ON atd16_univerttrail.geo_circuit;

CREATE TRIGGER t_before_iu_maj_longueur_sig
    BEFORE INSERT OR UPDATE
    ON atd16_univerttrail.geo_circuit 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_maj_longueur_sig();
    
COMMENT ON TRIGGER t_before_iu_maj_longueur_sig ON atd16_univerttrail.geo_circuit IS 
    '[ATD16] Mise à jour de la longueur du circuit';

