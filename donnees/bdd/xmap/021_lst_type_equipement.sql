-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                    Table non géographique : Domaine de valeur du type d'équipement                                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_type_equipement;

CREATE TABLE atd16_univerttrail.lst_type_equipement 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du type d'équipement 
    libelle varchar(254), --[UVT] Libellé du type d'équipement 
    CONSTRAINT pk_lst_type_equipement PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_type_equipement OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_type_equipement TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_type_equipement IS '[UVT] Domaine de valeur du type d''équipement ';

COMMENT ON COLUMN atd16_univerttrail.lst_type_equipement.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_type_equipement.code IS '[ATD16] Code du type d''équipement ';
COMMENT ON COLUMN atd16_univerttrail.lst_type_equipement.libelle IS '[UVT] Libellé du type d''équipement ';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_type_equipement 
    (code, libelle)
VALUES
    ('01', 'Point d''accueil'),
    ('02', 'Vestiaire/Douche'),
    ('03', 'Restauration'),
    ('04', 'Poubelle'),
    ('05', 'Point d''eau'),
    ('99', 'Autre');

