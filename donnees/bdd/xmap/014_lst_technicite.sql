-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                         Table non géographique : Domaine de valeur de la catégorie de la technicité du circuit                             ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_technicite;

CREATE TABLE atd16_univerttrail.lst_technicite 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de la catégorie
    libelle varchar(254), --[UVT] Libellé de la catégorie
    note integer, --[UVT] Note de la catégorie
    CONSTRAINT pk_lst_technicite PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_technicite OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_technicite TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_technicite IS '[UVT] Domaine de valeur de la catégorie de la technicité du circuit';

COMMENT ON COLUMN atd16_univerttrail.lst_technicite.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_technicite.code IS '[ATD16] Code de la catégorie';
COMMENT ON COLUMN atd16_univerttrail.lst_technicite.libelle IS '[UVT] Libellé de la catégorie';
COMMENT ON COLUMN atd16_univerttrail.lst_technicite.note IS '[UVT] Note de la catégorie';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_technicite 
    (code, libelle, note)
VALUES
    ('01', 'Itinéraire sans obstacle notable ou sans réelles difficultés', '1'),
    ('02', 'Itinéraire présentant des portions de sentier étroits ou des obstacles de faible dimension', '2'),
    ('03', 'Itinéraire présentant des difficultés techniques sur des portions relativement courtes et peu nombreuses', '3'),
    ('04', 'Itinéraire présentant des difficultés techniques sur des portions assez longues ou se répétant fréquemment', '4');

