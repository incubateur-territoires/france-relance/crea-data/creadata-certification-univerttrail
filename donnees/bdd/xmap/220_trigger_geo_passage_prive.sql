-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/06 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_supp_lien_passage_prive_circuit() et du trigger associé t_after_d_passage_prive_supp_lien_passage_prive_circuit

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                    Fonctions triggers et triggers spécifiques à la table geo_passage_prive                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                     Suppression des liens entre le circuit et les passages privés lorsque le passage privé est supprimé                    ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_supp_lien_passage_prive_circuit();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_supp_lien_passage_prive_circuit()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    DELETE FROM atd16_univerttrail.ngeo_circuit_passage_prive
    WHERE id_passage_prive = OLD.gid;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_supp_lien_passage_prive_circuit() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_supp_lien_passage_prive_circuit() IS 
    '[ATD16] Suppression des liens entre le circuit et les passages privés (dans ngeo_circuit_passage_prive) lorsque le passage privé est supprimé';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_passage_prive_supp_lien_passage_prive_circuit
    AFTER DELETE
    ON atd16_univerttrail.geo_passage_prive 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_supp_lien_passage_prive_circuit();
    
COMMENT ON TRIGGER t_after_d_passage_prive_supp_lien_passage_prive_circuit ON atd16_univerttrail.geo_passage_prive IS 
    '[ATD16] Suppression des liens entre le circuit et les passages privés (dans ngeo_circuit_passage_prive) lorsque le passage privé est supprimé';

