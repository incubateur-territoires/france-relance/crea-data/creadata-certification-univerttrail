-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                               Table non géographique : Domaine de valeur du niveau de risque du circuit                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_risque;

CREATE TABLE atd16_univerttrail.lst_risque 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du niveau de risque
    libelle varchar(254), --[UVT] Libellé du niveau de risque
    CONSTRAINT pk_lst_risque PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_risque OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_risque TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_risque IS '[UVT] Domaine de valeur du niveau de risque du circuit';

COMMENT ON COLUMN atd16_univerttrail.lst_risque.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_risque.code IS '[ATD16] Code du niveau de risque';
COMMENT ON COLUMN atd16_univerttrail.lst_risque.libelle IS '[UVT] Libellé du niveau de risque';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_risque 
    (code, libelle)
VALUES
    ('01', 'Faible'),
    ('02', 'Assez faible'),
    ('03', 'Assez élevé'),
    ('04', 'Élevé');

