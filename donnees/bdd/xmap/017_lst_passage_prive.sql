-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                       Table non géographique : Domaine de valeur de la présence de passage privé sur le circuit                            ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_passage_prive;

CREATE TABLE atd16_univerttrail.lst_passage_prive 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de la présence de passage privé
    libelle varchar(254), --[UVT] Libellé de la présence de passage privé
    CONSTRAINT pk_lst_passage_prive PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_passage_prive OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_passage_prive TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_passage_prive IS '[UVT] Domaine de valeur de la présence de passage privé du circuit';

COMMENT ON COLUMN atd16_univerttrail.lst_passage_prive.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_passage_prive.code IS '[ATD16] Code de la présence de passage privé';
COMMENT ON COLUMN atd16_univerttrail.lst_passage_prive.libelle IS '[UVT] Libellé de la présence de passage privé';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_passage_prive 
    (code, libelle)
VALUES
    ('01', 'Oui'),
    ('02', 'Non');

