-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/07 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/10/26 : SL / Ajout de la vue v_geo_zone_accueil_alert_incomplete
-- 2021/10/28 : SL / Ajout de la vue v_geo_zone_accueil_sans_site
-- 2021/11/09 : SL / Table obsolète (zone d'accueil = zone de départ principale) : mise en commentaire

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                      Table géographique : Zone d'accueil                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.geo_zone_accueil;
/*
CREATE TABLE atd16_univerttrail.geo_zone_accueil
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    id_site integer, --[FK][UVT] Identifiant du site
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Nom de la zone
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_zone_accueil PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.geo_zone_accueil OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.geo_zone_accueil TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.geo_zone_accueil IS '[ATD16] Table géographique contenant les zones d''accueil';

COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.id_site IS '[FK][UVT] Identifiant du site';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.ident IS '[SIRAP] Identificaion de l''objet / Nom de la zone';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_univerttrail.geo_zone_accueil.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ### v_geo_zone_accueil_alert_incomplete : vue de la couche geo_zone_accueil sans panneau d'info, de parking, de point d'eau et/ou de poubelles ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_zone_accueil_alert_incomplete;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_zone_accueil_alert_incomplete 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        (SELECT COUNT(y.gid) -- On compte le nombre d'objet
            FROM atd16_univerttrail.geo_signaletique y, atd16_univerttrail.geo_zone_accueil z -- de la table geo_signaletique
            WHERE ST_Intersects(y.the_geom, z.the_geom) AND y.type_signaletique = '01') AS nb_panneau_info,
            -- qui intersecte la zone d'accueil et dont le type = '01'
        (SELECT COUNT(x.gid)
            FROM atd16_univerttrail.geo_parking x, atd16_univerttrail.geo_zone_accueil z 
            WHERE ST_Intersects(x.the_geom, z.the_geom)) AS nb_parking,
        (SELECT COUNT(w.gid)
            FROM atd16_univerttrail.geo_equipement w, atd16_univerttrail.geo_zone_accueil z 
            WHERE ST_Intersects(w.the_geom, z.the_geom) AND w.type_equipement = '05') AS nb_point_d_eau,
        (SELECT COUNT(v.gid)
            FROM atd16_univerttrail.geo_equipement v, atd16_univerttrail.geo_zone_accueil z 
            WHERE ST_Intersects(v.the_geom, z.the_geom) AND v.type_equipement = '04') AS nb_poubelles,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_zone_accueil a
    WHERE 
        (SELECT COUNT(y.gid)
        FROM atd16_univerttrail.geo_signaletique y, atd16_univerttrail.geo_zone_accueil z
        WHERE ST_Intersects(y.the_geom, z.the_geom) AND y.type_signaletique = '01') = 0
    OR
        (SELECT COUNT(x.gid)
        FROM atd16_univerttrail.geo_parking x, atd16_univerttrail.geo_zone_accueil z 
        WHERE ST_Intersects(x.the_geom, z.the_geom)) = 0
    OR
        (SELECT COUNT(w.gid)
        FROM atd16_univerttrail.geo_equipement w, atd16_univerttrail.geo_zone_accueil z 
        WHERE ST_Intersects(w.the_geom, z.the_geom) AND w.type_equipement = '05') = 0
    OR
        (SELECT COUNT(v.gid)
        FROM atd16_univerttrail.geo_equipement v, atd16_univerttrail.geo_zone_accueil z 
        WHERE ST_Intersects(v.the_geom, z.the_geom) AND v.type_equipement = '04') = 0; 

ALTER TABLE atd16_univerttrail.v_geo_zone_accueil_alert_incomplete OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_zone_accueil_alert_incomplete IS 
    '[ATD16] Vue de la couche geo_zone_accueil sans panneau d''info, de parking, de point d''eau et/ou de poubelles';


-- ##################################################################################################################################################
-- ###                     v_geo_zone_accueil_sans_site : vue de la couche geo_zone_accueil sans lien avec la couche geo_site                     ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_univerttrail.v_geo_zone_accueil_sans_site;

CREATE OR REPLACE VIEW atd16_univerttrail.v_geo_zone_accueil_sans_site 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.id_site,
        a.ident,
        a.date_creation,
        a.date_maj,
        a.datesig,
        a.origdata,
        a.the_geom
    FROM atd16_univerttrail.geo_zone_accueil a
    WHERE id_site IS NULL;

ALTER TABLE atd16_univerttrail.v_geo_zone_accueil_sans_site OWNER TO sditecgrp;

COMMENT ON VIEW atd16_univerttrail.v_geo_zone_accueil_sans_site IS 
    '[ATD16] Vue de la couche geo_zone_accueil sans lien avec la couche geo_site';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_zone_accueil;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_univerttrail.geo_zone_accueil
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_univerttrail.geo_zone_accueil IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_zone_accueil;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_univerttrail.geo_zone_accueil
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_univerttrail.geo_zone_accueil IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';
*/

