-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/08 : SL / Création du fichier sur Git
-- 2021/10/21 : SL / Suppression de la valeur 00

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                   Table non géographique : Domaine de valeur du type de signalétique                                       ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_univerttrail.lst_type_signaletique;

CREATE TABLE atd16_univerttrail.lst_type_signaletique 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du type de signalétique
    libelle varchar(254), --[UVT] Libellé du type de signalétique
    CONSTRAINT pk_lst_type_signaletique PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_univerttrail.lst_type_signaletique OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_univerttrail.lst_type_signaletique TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_univerttrail.lst_type_signaletique IS '[UVT] Domaine de valeur du type de signalétique';

COMMENT ON COLUMN atd16_univerttrail.lst_type_signaletique.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_univerttrail.lst_type_signaletique.code IS '[ATD16] Code du type de signalétique';
COMMENT ON COLUMN atd16_univerttrail.lst_type_signaletique.libelle IS '[UVT] Libellé du type de signalétique';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_univerttrail.lst_type_signaletique 
    (code, libelle)
VALUES
    ('01', 'Panneau'),
    ('02', 'Totem'),
    ('03', 'Balise'),
    ('04', 'Passage dangereux'),
    ('99', 'Autre');

