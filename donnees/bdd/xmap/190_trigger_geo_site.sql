-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/10/28 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_supp_valeur_id_site() et du trigger associé t_after_d_site_supp_id_site

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                         Fonctions triggers et triggers spécifiques à la table geo_site                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                   Suppression de la valeur du champ id_site lorsque le site est supprimé                                   ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_univerttrail.f_supp_valeur_id_site();

CREATE OR REPLACE FUNCTION atd16_univerttrail.f_supp_valeur_id_site()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    UPDATE atd16_univerttrail.geo_circuit SET id_site = NULL WHERE id_site = OLD.gid;
	UPDATE atd16_univerttrail.geo_equipement SET id_site = NULL WHERE id_site = OLD.gid;
	UPDATE atd16_univerttrail.geo_parking SET id_site = NULL WHERE id_site = OLD.gid;
	UPDATE atd16_univerttrail.geo_passage_goudronne SET id_site = NULL WHERE id_site = OLD.gid;
	UPDATE atd16_univerttrail.geo_passage_prive SET id_site = NULL WHERE id_site = OLD.gid;
	UPDATE atd16_univerttrail.geo_signaletique SET id_site = NULL WHERE id_site = OLD.gid;
	UPDATE atd16_univerttrail.geo_zone_accueil SET id_site = NULL WHERE id_site = OLD.gid;
	UPDATE atd16_univerttrail.geo_zone_depart SET id_site = NULL WHERE id_site = OLD.gid;
	UPDATE atd16_univerttrail.geo_zone_echauffement SET id_site = NULL WHERE id_site = OLD.gid;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_univerttrail.f_supp_valeur_id_site() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_univerttrail.f_supp_valeur_id_site() IS 
    '[ATD16] Suppression de la valeur du champ id_site lorsque le site est supprimé';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_site_supp_id_site
    AFTER DELETE
    ON atd16_univerttrail.geo_site 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_univerttrail.f_supp_valeur_id_site();
    
COMMENT ON TRIGGER t_after_d_site_supp_id_site ON atd16_univerttrail.geo_site IS 
    '[ATD16] Suppression de la valeur du champ id_site lorsque le site est supprimé';

