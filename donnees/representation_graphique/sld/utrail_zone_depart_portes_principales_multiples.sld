<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation des zones de départ lorsqu'il existe plusieurs portes principales dans le même site
    appliquée à la couche univert_trail:v_geo_zone_depart_portes_principales_multiples (Geoserver) ; Randonnées\Uni'vert trail\Alertes\Portes principales multiples (zone de départ) (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>portes_principales_multiples</se:Name>
        <UserStyle>
            <se:Name>portes_principales_multiples</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>portes_principales_multiples</se:Name>
                    <se:Description>
                        <se:Title>Portes principales multiples</se:Title>
                    </se:Description>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#4D1FF9</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.6</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#4D1FF9</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
