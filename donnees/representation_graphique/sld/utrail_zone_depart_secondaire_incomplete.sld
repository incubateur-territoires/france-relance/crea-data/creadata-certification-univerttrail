<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation des zones de départ secondaire incomplètes
	appliquée à la couche univert_trail:v_geo_zone_depart_02_alert_incomplete (Geoserver) ; Randonnées\Uni'vert trail\Alertes\Zone de départ secondaire incomplète (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>zone_depart_incomplete</se:Name>
        <UserStyle>
            <se:Name>zone_depart_incomplete</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>zone_depart_incomplete</se:Name>
                    <se:Description>
                        <se:Title>Absence de panneau d'info ou de totem et de parking</se:Title>
                    </se:Description>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#F9FF4B</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.6</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#F9FF4B</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
