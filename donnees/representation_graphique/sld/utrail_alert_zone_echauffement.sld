<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des zones d'échauffement ayant une longueur non conforme
    appliquée à la couche univert_trail:v_geo_zone_echauffement_alert_longueur (Geoserver) ; Randonnées\Uni'vert trail\Alertes\Zone d'échauffement : longueur non conforme (doit être entre 400 et 600 m) (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>zone_echauf_longueur</se:Name>
        <UserStyle>
            <se:Name>zone_echauf_longueur</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>zone_echauf_longueur</se:Name>
                    <se:Description>
                        <se:Title>Zone d'échauffement : longueur non conforme (doit être entre 400 et 600 m)</se:Title>
                    </se:Description>
                    <se:LineSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#5B0D0D</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">4</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:LineSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
