<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation des zones d'accueil ou départ principales incomplètes
	appliquée à la couche univert_trail:v_geo_zone_depart_01_alert_incomplete (Geoserver) ; Randonnées\Uni'vert trail\Alertes\Zone d'accueil ou de départ principale incomplète (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>zone_accueil_incomplete</se:Name>
        <UserStyle>
            <se:Name>zone_accueil_incomplete</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>zone_accueil_incomplete</se:Name>
                    <se:Description>
                        <se:Title>Absence de panneau d'info, de parking, de point d'eau et/ou de poubelle</se:Title>
                    </se:Description>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#F7FF00</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.6</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#F7FF00</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
