<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des circuits avec un niveau de difficulté incohérent
    appliquée à la couche univert_trail:v_geo_circuit_avec_niveau_incoherent (Geoserver) ; Randonnées\Uni'vert trail\Alertes\Circuit avec un niveau de difficulté incohérent par rapport à la cotation (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>niveau_incoherent</se:Name>
        <UserStyle>
            <se:Name>niveau_incoherent</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>niveau_incoherent</se:Name>
                    <se:Description>
                        <se:Title>Circuit avec un niveau de difficulté incohérent par rapport à la cotation</se:Title>
                    </se:Description>
                    <se:LineSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#FFB000</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">7</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:LineSymbolizer>
                    <se:LineSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#000000</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">4</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                          	<se:SvgParameter name="stroke-dasharray">10 20</se:SvgParameter>
                        </se:Stroke>
                    </se:LineSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
