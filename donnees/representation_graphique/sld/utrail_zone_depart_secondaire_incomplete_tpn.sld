<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation des labels des zones de départ secondaire incomplètes
	appliquée à la couche univert_trail:v_geo_zone_depart_02_alert_incomplete_tpn (Geoserver) ; Randonnées\Uni'vert trail\Alertes\Labels\Zone de départ secondaire incomplète (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>zone_depart_incomplete</se:Name>
        <UserStyle>
            <se:Name>zone_depart_incomplete</se:Name>
            <se:FeatureTypeStyle>
        		<se:Rule>
          			<se:TextSymbolizer>
						<se:Label>
                          	<ogc:PropertyName>nb_panneau_et_totem</ogc:PropertyName> panneau(x) ou totem<![CDATA[
]]>
                          	<ogc:PropertyName>nb_parking</ogc:PropertyName> parking
						</se:Label>
            			<se:Font>
           					<se:SvgParameter name="font-size">10</se:SvgParameter>
         				</se:Font>
                      	<se:LabelPlacement>
                        	<se:PointPlacement>
                      			<se:AnchorPoint>
  									<se:AnchorPointX>0</se:AnchorPointX>
  									<se:AnchorPointY>0</se:AnchorPointY>
								</se:AnchorPoint>
              					<se:Displacement>
                					<se:DisplacementX>-40</se:DisplacementX>
                					<se:DisplacementY>-65</se:DisplacementY>
              					</se:Displacement>
                          	</se:PointPlacement>
                        </se:LabelPlacement>
            			<se:Halo>
           					<se:Radius><ogc:Literal>1.5</ogc:Literal></se:Radius>
           					<se:Fill>
             					<se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
           					</se:Fill>
         				</se:Halo>
           				<se:Fill>
             				<se:SvgParameter name="fill">#FF0000</se:SvgParameter>
           				</se:Fill>
                      	<se:Graphic>
                          	<se:Mark>
                              	<se:WellKnownName>square</se:WellKnownName>
             					<se:Fill>
               						<se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.6</se:SvgParameter>
             					</se:Fill>
             					<se:Stroke>
               						<se:SvgParameter name="stroke">#FF0000</se:SvgParameter>
               						<se:SvgParameter name="stroke-width">1</se:SvgParameter>
             					</se:Stroke>
                          	</se:Mark>
                      	</se:Graphic>
            			<se:VendorOption name="graphic-resize">stretch</se:VendorOption>
            			<se:VendorOption name="graphic-margin">5</se:VendorOption>
		   			</se:TextSymbolizer>
        		</se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
