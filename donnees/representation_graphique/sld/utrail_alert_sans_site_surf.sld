<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation des objets sans site
    appliquée à la couche univert_trail:v_geo_parking_sans_site (Geoserver) ; Randonnées\Uni'vert trail\Alertes\Objets sans site\Parking (X'MAP)
    appliquée à la couche univert_trail:v_geo_zone_accueil_sans_site (Geoserver) ; Randonnées\Uni'vert trail\Alertes\Objets sans site\Zone d'accueil (X'MAP)
    appliquée à la couche univert_trail:v_geo_zone_depart_sans_site (Geoserver) ; Randonnées\Uni'vert trail\Alertes\Objets sans site\Zone de départ (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>sans_site</se:Name>
        <UserStyle>
            <se:Name>sans_site</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>sans_site</se:Name>
                    <se:Description>
                        <se:Title>Objet sans site</se:Title>
                    </se:Description>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#000000</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">1</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#FF0000</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1.5</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
