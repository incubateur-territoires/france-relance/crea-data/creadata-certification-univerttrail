<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des labels des zones de départ
    appliquée à la couche univert_trail:geo_zone_depart_tpn (Geoserver) ; Randonnées\Uni'Vert trail\label\Zone de départ (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
	<NamedLayer>
    	<se:Name>geo_zone_depart_tpn</se:Name>
    	<UserStyle>
      		<se:Name>geo_zone_depart_tpn</se:Name>
      		<se:FeatureTypeStyle>
        		<se:Rule>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>type</ogc:PropertyName>
              				<ogc:Literal>01</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
          			<se:TextSymbolizer>
						<se:Label>
							Porte principale
						</se:Label>
            			<se:Font>
           					<se:SvgParameter name="font-size">10</se:SvgParameter>
         				</se:Font>
          				<se:LabelPlacement>
            				<se:PointPlacement>
              					<se:AnchorPoint>
                					<se:AnchorPointX>0.5</se:AnchorPointX>
                					<se:AnchorPointY>0.5</se:AnchorPointY>
              					</se:AnchorPoint>
              					<se:Displacement>
                					<se:DisplacementX>0</se:DisplacementX>
                					<se:DisplacementY>0</se:DisplacementY>
              					</se:Displacement>
            				</se:PointPlacement>
          				</se:LabelPlacement>
            			<se:Halo>
           					<se:Radius><ogc:Literal>1.5</ogc:Literal></se:Radius>
           					<se:Fill>
             					<se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
           					</se:Fill>
         				</se:Halo>
           				<se:Fill>
             				<se:SvgParameter name="fill">#0D6834</se:SvgParameter>
           				</se:Fill>
		   			</se:TextSymbolizer>
        		</se:Rule>
        		<se:Rule>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>type</ogc:PropertyName>
              				<ogc:Literal>02</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
          			<se:TextSymbolizer>
						<se:Label>
							Porte secondaire
						</se:Label>
            			<se:Font>
           					<se:SvgParameter name="font-size">10</se:SvgParameter>
         				</se:Font>
          				<se:LabelPlacement>
            				<se:PointPlacement>
              					<se:AnchorPoint>
                					<se:AnchorPointX>0.5</se:AnchorPointX>
                					<se:AnchorPointY>0.5</se:AnchorPointY>
              					</se:AnchorPoint>
              					<se:Displacement>
                					<se:DisplacementX>0</se:DisplacementX>
                					<se:DisplacementY>0</se:DisplacementY>
              					</se:Displacement>
            				</se:PointPlacement>
          				</se:LabelPlacement>
            			<se:Halo>
           					<se:Radius><ogc:Literal>1.5</ogc:Literal></se:Radius>
           					<se:Fill>
             					<se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
           					</se:Fill>
         				</se:Halo>
           				<se:Fill>
             				<se:SvgParameter name="fill">#0D6834</se:SvgParameter>
           				</se:Fill>
		   			</se:TextSymbolizer>
        		</se:Rule>
      		</se:FeatureTypeStyle>
    	</UserStyle>
  	</NamedLayer>
</StyledLayerDescriptor>
