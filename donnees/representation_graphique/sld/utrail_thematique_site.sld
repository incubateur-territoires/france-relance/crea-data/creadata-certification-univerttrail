<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des sites
    appliquée à la couche univert_trail:geo_site_thematique (Geoserver) ; Randonnées\Uni'vert trail\Thématiques\Site (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>site</se:Name>
        <UserStyle>
            <se:Name>site</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>site</se:Name>
                    <se:Description>
                        <se:Title>Certification Uni'Vert trail Or</se:Title>
                    </se:Description>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>labellisation</ogc:PropertyName>
              				<ogc:Literal>02</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#ffd700</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.4</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#ffd700</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-opacity">0.4</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
          			<se:PointSymbolizer>
                      	<se:Geometry>
                          	<ogc:Function name="centroid">
            					<ogc:PropertyName>the_geom</ogc:PropertyName>
         					</ogc:Function>
                      	</se:Geometry>
                      	<se:Graphic>
                          	<se:Mark>
                              	<se:WellKnownName>star</se:WellKnownName>
             					<se:Fill>
               						<se:SvgParameter name="fill">#ffd700</se:SvgParameter>
               						<se:SvgParameter name="fill-opacity">1</se:SvgParameter>
             					</se:Fill>
             					<se:Stroke>
               						<se:SvgParameter name="stroke">#ffd700</se:SvgParameter>
               						<se:SvgParameter name="stroke-width">1</se:SvgParameter>
             					</se:Stroke>
                          	</se:Mark>
                      		<se:Size>60</se:Size>
                      	</se:Graphic>
		   			</se:PointSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>site</se:Name>
                    <se:Description>
                        <se:Title>Certification Uni'Vert trail Argent</se:Title>
                    </se:Description>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>labellisation</ogc:PropertyName>
              				<ogc:Literal>01</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#c0c0c0</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.4</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#c0c0c0</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-opacity">0.4</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
          			<se:PointSymbolizer>
                      	<se:Geometry>
                          	<ogc:Function name="centroid">
            					<ogc:PropertyName>the_geom</ogc:PropertyName>
         					</ogc:Function>
                      	</se:Geometry>
                      	<se:Graphic>
                          	<se:Mark>
                              	<se:WellKnownName>star</se:WellKnownName>
             					<se:Fill>
               						<se:SvgParameter name="fill">#c0c0c0</se:SvgParameter>
               						<se:SvgParameter name="fill-opacity">1</se:SvgParameter>
             					</se:Fill>
             					<se:Stroke>
               						<se:SvgParameter name="stroke">#c0c0c0</se:SvgParameter>
               						<se:SvgParameter name="stroke-width">1</se:SvgParameter>
             					</se:Stroke>
                          	</se:Mark>
                      		<se:Size>60</se:Size>
                      	</se:Graphic>
		   			</se:PointSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>site</se:Name>
                    <se:Description>
                        <se:Title>Pas de certification</se:Title>
                    </se:Description>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>labellisation</ogc:PropertyName>
              				<ogc:Literal>03</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#F794F9</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.4</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#F794F9</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-opacity">0.4</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
