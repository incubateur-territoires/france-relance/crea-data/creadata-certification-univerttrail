<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation des sites ayant une longueur totale d'itinéraire inféreirue à 40 km
    appliquée à la couche univert_trail:v_geo_site_alert_longueur_iti_totale (Geoserver) ; Randonnées\Uni'vert trail\Alertes\Site ayant une longueur totale des circuits inf. à 40 km (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>site</se:Name>
        <UserStyle>
            <se:Name>site</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>site</se:Name>
                    <se:Description>
                        <se:Title>Longueur totale des circuits inf. à 40 km</se:Title>
                    </se:Description>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#FF0000</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.6</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#FF0000</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
