<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des parcelles intersectant des circuits
    appliquée à la couche univert_trail:v_geo_parcelle_x_circuit (Geoserver) ; Randonnées\Uni'vert trail\Alertes\Parcelle intersectée par un circuit (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>parcelle_x_circuit</se:Name>
        <UserStyle>
            <se:Name>parcelle_x_circuit</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>parcelle_x_circuit</se:Name>
                    <se:Description>
                        <se:Title>Parcelle intersectée par un circuit</se:Title>
                    </se:Description>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#C4AB84</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.5</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#C4AB84</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-opacity">0.5</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
