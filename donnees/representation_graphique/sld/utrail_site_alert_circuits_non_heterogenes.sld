<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation des sites n'ayant pas trois circuits de niveaux différents
    appliquée à la couche univert_trail:v_geo_site_alert_circuit_non_heterogene (Geoserver) ; Randonnées\Uni'vert trail\Alertes\Site n'ayant pas trois circuits de niveaux différents (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>site</se:Name>
        <UserStyle>
            <se:Name>site</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>site</se:Name>
                    <se:Description>
                        <se:Title>Moins de trois niveaux différents pour l'ensemble des circuits</se:Title>
                    </se:Description>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#FF9700</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.6</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#FF9700</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
