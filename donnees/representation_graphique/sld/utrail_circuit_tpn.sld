<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des labels basiques des circuits
    appliquée à la couche univert_trail:geo_circuit_tpn (Geoserver) ; Randonnées\Uni'Vert trail\label\Circuit (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
	<NamedLayer>
    	<se:Name>geo_circuit_tpn</se:Name>
    	<UserStyle>
      		<se:Name>geo_circuit_tpn</se:Name>
      		<se:FeatureTypeStyle>
        		<se:Rule>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>niveau</ogc:PropertyName>
              				<ogc:Literal>01</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
          			<se:TextSymbolizer>
						<se:Label>
							<ogc:PropertyName>numero</ogc:PropertyName>
						</se:Label>
            			<se:Font>
           					<se:SvgParameter name="font-size">15</se:SvgParameter>
         				</se:Font>
            			<se:Halo>
           					<se:Radius><ogc:Literal>1.5</ogc:Literal></se:Radius>
           					<se:Fill>
             					<se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
           					</se:Fill>
         				</se:Halo>
           				<se:Fill>
             				<se:SvgParameter name="fill">#259C17</se:SvgParameter>
           				</se:Fill>
                      	<se:Graphic>
                          	<se:Mark>
                              	<se:WellKnownName>circle</se:WellKnownName>
             					<se:Fill>
               						<se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
               						<se:SvgParameter name="fill-opacity">1</se:SvgParameter>
             					</se:Fill>
             					<se:Stroke>
               						<se:SvgParameter name="stroke">#259C17</se:SvgParameter>
               						<se:SvgParameter name="stroke-width">1</se:SvgParameter>
             					</se:Stroke>
                          	</se:Mark>
                      	</se:Graphic>
                      	<se:VendorOption name="repeat">666</se:VendorOption>
            			<se:VendorOption name="graphic-resize">stretch</se:VendorOption>
            			<se:VendorOption name="graphic-margin">5</se:VendorOption>
		   			</se:TextSymbolizer>
        		</se:Rule>
        		<se:Rule>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>niveau</ogc:PropertyName>
              				<ogc:Literal>02</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
          			<se:TextSymbolizer>
						<se:Label>
							<ogc:PropertyName>numero</ogc:PropertyName>
						</se:Label>
            			<se:Font>
           					<se:SvgParameter name="font-size">15</se:SvgParameter>
         				</se:Font>
            			<se:Halo>
           					<se:Radius><ogc:Literal>1.5</ogc:Literal></se:Radius>
           					<se:Fill>
             					<se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
           					</se:Fill>
         				</se:Halo>
           				<se:Fill>
             				<se:SvgParameter name="fill">#4F71FC</se:SvgParameter>
           				</se:Fill>
                      	<se:Graphic>
                          	<se:Mark>
                              	<se:WellKnownName>circle</se:WellKnownName>
             					<se:Fill>
               						<se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
               						<se:SvgParameter name="fill-opacity">1</se:SvgParameter>
             					</se:Fill>
             					<se:Stroke>
               						<se:SvgParameter name="stroke">#4F71FC</se:SvgParameter>
               						<se:SvgParameter name="stroke-width">1</se:SvgParameter>
             					</se:Stroke>
                          	</se:Mark>
                      	</se:Graphic>
                      	<se:VendorOption name="repeat">666</se:VendorOption>
            			<se:VendorOption name="graphic-resize">stretch</se:VendorOption>
            			<se:VendorOption name="graphic-margin">5</se:VendorOption>
		   			</se:TextSymbolizer>
        		</se:Rule>
        		<se:Rule>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>niveau</ogc:PropertyName>
              				<ogc:Literal>03</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
          			<se:TextSymbolizer>
						<se:Label>
							<ogc:PropertyName>numero</ogc:PropertyName>
						</se:Label>
            			<se:Font>
           					<se:SvgParameter name="font-size">15</se:SvgParameter>
         				</se:Font>
            			<se:Halo>
           					<se:Radius><ogc:Literal>1.5</ogc:Literal></se:Radius>
           					<se:Fill>
             					<se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
           					</se:Fill>
         				</se:Halo>
           				<se:Fill>
             				<se:SvgParameter name="fill">#FF0000</se:SvgParameter>
           				</se:Fill>
                      	<se:Graphic>
                          	<se:Mark>
                              	<se:WellKnownName>circle</se:WellKnownName>
             					<se:Fill>
               						<se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
               						<se:SvgParameter name="fill-opacity">1</se:SvgParameter>
             					</se:Fill>
             					<se:Stroke>
               						<se:SvgParameter name="stroke">#FF0000</se:SvgParameter>
               						<se:SvgParameter name="stroke-width">1</se:SvgParameter>
             					</se:Stroke>
                          	</se:Mark>
                      	</se:Graphic>
                      	<se:VendorOption name="repeat">666</se:VendorOption>
            			<se:VendorOption name="graphic-resize">stretch</se:VendorOption>
            			<se:VendorOption name="graphic-margin">5</se:VendorOption>
		   			</se:TextSymbolizer>
        		</se:Rule>
        		<se:Rule>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>niveau</ogc:PropertyName>
              				<ogc:Literal>04</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
          			<se:TextSymbolizer>
						<se:Label>
							<ogc:PropertyName>numero</ogc:PropertyName>
						</se:Label>
            			<se:Font>
           					<se:SvgParameter name="font-size">15</se:SvgParameter>
         				</se:Font>
            			<se:Halo>
           					<se:Radius><ogc:Literal>1.5</ogc:Literal></se:Radius>
           					<se:Fill>
             					<se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
           					</se:Fill>
         				</se:Halo>
           				<se:Fill>
             				<se:SvgParameter name="fill">#000000</se:SvgParameter>
           				</se:Fill>
                      	<se:Graphic>
                          	<se:Mark>
                              	<se:WellKnownName>circle</se:WellKnownName>
             					<se:Fill>
               						<se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
               						<se:SvgParameter name="fill-opacity">1</se:SvgParameter>
             					</se:Fill>
             					<se:Stroke>
               						<se:SvgParameter name="stroke">#000000</se:SvgParameter>
               						<se:SvgParameter name="stroke-width">1</se:SvgParameter>
             					</se:Stroke>
                          	</se:Mark>
                      	</se:Graphic>
                      	<se:VendorOption name="repeat">666</se:VendorOption>
            			<se:VendorOption name="graphic-resize">stretch</se:VendorOption>
            			<se:VendorOption name="graphic-margin">5</se:VendorOption>
		   			</se:TextSymbolizer>
        		</se:Rule>
        		<se:Rule>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsNull>
              				<ogc:PropertyName>niveau</ogc:PropertyName>
            			</ogc:PropertyIsNull>
          			</ogc:Filter>
          			<se:TextSymbolizer>
						<se:Label>
							<ogc:PropertyName>numero</ogc:PropertyName>
						</se:Label>
            			<se:Font>
           					<se:SvgParameter name="font-size">15</se:SvgParameter>
         				</se:Font>
            			<se:Halo>
           					<se:Radius><ogc:Literal>1.5</ogc:Literal></se:Radius>
           					<se:Fill>
             					<se:SvgParameter name="fill">#D3D3D3</se:SvgParameter>
           					</se:Fill>
         				</se:Halo>
           				<se:Fill>
             				<se:SvgParameter name="fill">#757575</se:SvgParameter>
           				</se:Fill>
                      	<se:Graphic>
                          	<se:Mark>
                              	<se:WellKnownName>circle</se:WellKnownName>
             					<se:Fill>
               						<se:SvgParameter name="fill">#D3D3D3</se:SvgParameter>
               						<se:SvgParameter name="fill-opacity">1</se:SvgParameter>
             					</se:Fill>
             					<se:Stroke>
               						<se:SvgParameter name="stroke">#757575</se:SvgParameter>
               						<se:SvgParameter name="stroke-width">1</se:SvgParameter>
             					</se:Stroke>
                          	</se:Mark>
                      	</se:Graphic>
                      	<se:VendorOption name="repeat">666</se:VendorOption>
            			<se:VendorOption name="graphic-resize">stretch</se:VendorOption>
            			<se:VendorOption name="graphic-margin">5</se:VendorOption>
		   			</se:TextSymbolizer>
        		</se:Rule>
      		</se:FeatureTypeStyle>
    	</UserStyle>
  	</NamedLayer>
</StyledLayerDescriptor>
