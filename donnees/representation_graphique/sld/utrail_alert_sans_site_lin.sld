<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des linéaires sans sites
    appliquée à la couche univert_trail:v_geo_circuit_sans_site (Geoserver) ; Randonnées\Uni'vert trail\Alertes\Objets sans site\Circuit (X'MAP)
	appliquée à la couche univert_trail:v_geo_passage_goudronne_sans_site (Geoserver) ; Randonnées\Uni'vert trail\Alertes\Objets sans site\Passage goudronné (X'MAP)
	appliquée à la couche univert_trail:v_geo_passage_prive_sans_site (Geoserver) ; Randonnées\Uni'vert trail\Alertes\Objets sans site\Passage privé (X'MAP)
	appliquée à la couche univert_trail:v_geo_zone_echauffement_sans_site (Geoserver) ; Randonnées\Uni'vert trail\Alertes\Objets sans site\Zone d'échauffement (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>sans_site</se:Name>
        <UserStyle>
            <se:Name>sans_site</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>sans_site</se:Name>
                    <se:Description>
                        <se:Title>Objet sans site</se:Title>
                    </se:Description>
                    <se:LineSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#FF0000</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">7</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:LineSymbolizer>
                    <se:LineSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#000000</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">4</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                          	<se:SvgParameter name="stroke-dasharray">10 20</se:SvgParameter>
                        </se:Stroke>
                    </se:LineSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
